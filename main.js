(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");








const routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '', component: _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"] },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'test', component: _test_test_component__WEBPACK_IMPORTED_MODULE_3__["TestComponent"] },
    { path: 'cpn', loadChildren: () => __webpack_require__.e(/*! import() | cpn-cpn-module */ "cpn-cpn-module").then(__webpack_require__.bind(null, /*! ./cpn/cpn.module */ "./src/app/cpn/cpn.module.ts")).then(m => m.CpnModule) },
    { path: '**', component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_5__["NotFoundComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class AppComponent {
    constructor(router) {
        this.title = 'frontCrm';
        this.loading = false;
        router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                this.loading = true;
            }
            else if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                this.loading = false;
            }
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: [".loader[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    width: 40px;\r\n    height: 40px;\r\n    position: absolute;\r\n    left: 0;\r\n    right: 0;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    top: calc(50% - 50px);\r\n    transform: translateY(-50%);\r\n  }\r\n  \r\n  .loader[_ngcontent-%COMP%]:after {\r\n    content: ' ';\r\n    display: block;\r\n    width: 30px;\r\n    height: 30px;\r\n    border-radius: 50%;\r\n    border: 5px solid #fff;\r\n    border-color: #fff transparent #fff transparent;\r\n    animation: loader 1.2s linear infinite;\r\n  }\r\n  \r\n  @keyframes loader {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(360deg);\r\n    }\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQiwyQkFBMkI7RUFDN0I7O0VBRUE7SUFDRSxZQUFZO0lBQ1osY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QiwrQ0FBK0M7SUFDL0Msc0NBQXNDO0VBQ3hDOztFQUVBO0lBQ0U7TUFDRSx1QkFBdUI7SUFDekI7SUFDQTtNQUNFLHlCQUF5QjtJQUMzQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVyIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB0b3A6IGNhbGMoNTAlIC0gNTBweCk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgfVxyXG4gIFxyXG4gIC5sb2FkZXI6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyAnO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XHJcbiAgICBib3JkZXItY29sb3I6ICNmZmYgdHJhbnNwYXJlbnQgI2ZmZiB0cmFuc3BhcmVudDtcclxuICAgIGFuaW1hdGlvbjogbG9hZGVyIDEuMnMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGxvYWRlciB7XHJcbiAgICAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgICB9XHJcbiAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material-module */ "./src/app/material-module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./security/token-interceptor.service */ "./src/app/security/token-interceptor.service.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/__ivy_ngcc__/fesm2015/ngx-countdown.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./baseUrl */ "./src/app/baseUrl.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./autocomplete/autocomplete.component */ "./src/app/autocomplete/autocomplete.component.ts");


/**************** library      **********************************/





/**************** component      **********************************/















_fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"].registerPlugins([
    _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__["default"],
    _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__["default"]
]);
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
            useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
            multi: true,
        }
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
            _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
        _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
        _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
        _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_20__["AutocompleteComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
        ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
        _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
                    _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
                    _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
                    _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_20__["AutocompleteComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                    ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
                    _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
                ],
                providers: [
                    { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
                    { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
                    {
                        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                        useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
                        multi: true,
                    }
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
                schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/autocomplete/autocomplete.component.ts":
/*!********************************************************!*\
  !*** ./src/app/autocomplete/autocomplete.component.ts ***!
  \********************************************************/
/*! exports provided: AutocompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutocompleteComponent", function() { return AutocompleteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class AutocompleteComponent {
    constructor() { }
    ngOnInit() {
    }
}
AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(); };
AutocompleteComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AutocompleteComponent, selectors: [["app-autocomplete"]], decls: 2, vars: 0, template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "autocomplete works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dG9jb21wbGV0ZS9hdXRvY29tcGxldGUuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AutocompleteComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-autocomplete',
                templateUrl: './autocomplete.component.html',
                styleUrls: ['./autocomplete.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/baseUrl.ts":
/*!****************************!*\
  !*** ./src/app/baseUrl.ts ***!
  \****************************/
/*! exports provided: baseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
const baseUrl = "http://api.cpn-aide-aux-entreprises.com";


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");






function HomeComponent_a_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
} }
function HomeComponent_a_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_tpe_pme");
} }
function HomeComponent_a_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agence");
} }
function HomeComponent_a_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_collectivite");
} }
function HomeComponent_a_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agenda");
} }
function HomeComponent_a_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/calendar");
} }
function HomeComponent_a_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Inscription");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Inscription");
} }
function HomeComponent_a_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Connexion");
} }
function HomeComponent_li_31_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "profile");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_li_31_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.logout(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "d\u00E9connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r8.user.first_name, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/profile");
} }
class HomeComponent {
    constructor(tokenStorage, route) {
        this.tokenStorage = tokenStorage;
        this.route = route;
        this.token = "";
        this.user = null;
        this.connect = false;
        this.role = "logout";
    }
    ngOnInit() {
        if (this.tokenStorage.getUser() != false) {
            this.token = this.tokenStorage.getUser();
            console.log("singin", this.tokenStorage.getUser());
            this.user = JSON.parse(this.token);
            this.role = this.user.role;
            this.connect = true;
        }
        this.serachbar();
        this.compteur();
    }
    serachbar() {
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
    compteur() {
        $(document).ready(function () {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    }
    logout() {
        this.tokenStorage.signOut();
        console.log("singout", this.tokenStorage.getUser());
        this.connect = false;
        this.role = "logout";
        location.href = '/home';
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 353, vars: 18, consts: [[1, "navbar", "navbar-expand-lg", "nav_g"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "Logo", 1, "brand_logo", "d-inline-block", "align-text-top", "nav_img"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 2, "box-shadow", "none"], [1, "far", "fa-bars", "navbar-toggler-icon", 2, "color", "white", "z-index", "1"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto", "topnav"], [1, "nav-item"], ["class", "nav-link", 3, "routerLink", 4, "ngIf"], [1, "nav-link", 3, "routerLink"], ["class", "nav-link con", 3, "routerLink", 4, "ngIf"], ["class", "nav-item", 4, "ngIf"], [1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-8", "col-12"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper", "content"], ["src", "assets/cpnimages/home/33.png", "alt", "", 1, "heading_img"], [1, "col-md-4", "p-3", "block0"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto", "transtion"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px", "text-align", "start"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 20px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "our_success", "mb-3"], [1, "success_wrapper", "container", "px-4", "g-0"], [1, "col-md-6", "py-2", "title"], [1, "success_txt"], [1, "success_desc"], [1, "col-md-6", "py-2", "chiffre"], [1, "success_list"], [1, "success_item"], [1, "k"], [1, "item_num"], [1, "item_desc"], [1, "actuality", "mb-5"], [1, "float_actions"], [1, "actions_content"], [1, "action_items"], ["href", "cpn/Home_tpe_pme", 1, "item_href"], [1, "ihref_logo"], ["width", "40%", "src", "assets/cpnimages/sidebar/Entreprise.png", "alt", ""], [1, "ihref_text"], [1, "testmegi", 3, "routerLink"], ["href", "/cpn/agence", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Agence.png"], ["href", "/cpn/Home_collectivite", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Collectivit\u00E9.png"], [1, "card-blog", "mb-5"], [1, "card_wrapper", "container", "px-4", "g-0"], [1, "row", "py-3"], [1, "col-md-4"], [1, "card", "d-flex", "flex-column", "align-items-center", "justify-content-center", "block2"], ["src", "assets/cpnimages/home/agent.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-body", "py-0", "d-flex", "flex-column", "justify-content-center", "align-items-center"], [1, "card-title", "home"], [1, "card-text", "home"], [1, "card-footer", "py-0"], [1, "test-btn1", "btn", "btn-light", 3, "routerLink"], ["src", "assets/cpnimages/home/casque.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-text", "home", 2, "text-align", "center"], ["href", "#", 1, "test-btn2", "btn", "btn-light"], ["src", "assets/cpnimages/home/money.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], ["href", "#", 1, "test-btn3", "btn", "btn-light"], [1, "text-bloc", "g-0", "mb-5", "block3"], [1, "container", "px-4"], [1, "text_body"], [1, "text_content"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "block4"], [1, "container", "px-4", "block1"], [1, "wavy"], [1, "outer-div", "one"], [1, "inner-div"], [1, "front"], [1, "front__face-photo1"], ["src", "assets/cpnimages/home/icone-2.png", "alt", "", 2, "width", "100%"], [1, "front__text"], [1, "front__text-header"], [1, "front__text-para"], [1, "outer-div", "two"], [1, "front__face-photo2"], ["src", "assets/cpnimages/home/icone-1.png", "alt", "", 2, "width", "100%"], [1, "outer-div", "three"], [1, "front__face-photo3"], ["src", "assets/cpnimages/home/icone-3.png", "alt", "", 2, "width", "100%"], [1, "third-bloc", "g-0", "mb-5"], [1, "container", "px-4", "text-center"], [2, "color", "#111d5e"], [1, "subvention-text", "text-center", 2, "color", "gray"], [1, "col-6", "block5"], ["src", "assets/cpnimages/home/old.PNG", "alt", "..."], [1, "col-6", "block6"], [1, "third-bloc-border"], [1, "block7"], [1, "container", "px-4", "lastB"], [1, "text-center"], [1, "row", "row-cols-1", "row-cols-md-3", "g-4", "justify-content-center"], [1, "col-md-3", "card1"], [1, "card", "h-100", "box1"], [1, "card-body"], [1, "card-text"], [1, "image"], ["src", "assets/cpnimages/home/avis3.png", "alt", " avis 1", 1, "img-fluid"], [1, "col-md-3", "card2"], [1, "card", "h-100", "box2"], ["src", "assets/cpnimages/home/avis1.png", "alt", " avis 2", 1, "img-fluid"], [1, "col-md-3", "card3"], [1, "card", "h-100", "box3"], [1, "card-text", 2, "margin-right", "-43px"], ["src", "assets/cpnimages/home/avis2.png", "alt", "avis 3", 1, "img-fluid"], [1, "text-lg-start", "text-muted"], [1, "d-flex", "justify-content-center", "justify-content-lg-between"], [1, ""], [1, "container", "text-md-start", "mt-5", "footer", 2, "font-size", "12px"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "logo", "width", "50px", "height", "50px"], [2, "color", "white", "font-size", "12px"], [2, "font-size", "25px"], ["href", "https://www.instagram.com/cpn_aideauxentreprises/?hl=fr"], ["aria-hidden", "true", 1, "fab", "fa-instagram"], ["href", "https://www.youtube.com/channel/UC2KAUP-XzalYUGGPLEXBUBQ"], ["aria-hidden", "true", 1, "fab", "fa-youtube", 2, "margin-left", "5px"], ["href", "https://twitter.com/cpn_officiel"], ["aria-hidden", "true", 1, "fab", "fa-twitter", 2, "margin-left", "5px"], ["href", "https://www.linkedin.com/company/76078573/admin/"], ["aria-hidden", "true", 1, "fab", "fa-linkedin", 2, "margin-left", "5px"], ["href", "https://www.facebook.com/CPN.aideauxentreprises"], ["aria-hidden", "true", 1, "fab", "fa-facebook", 2, "margin-left", "5px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4", 2, "color", "white"], ["href", "#!", 1, "text-reset", 2, "color", "white"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto"], ["href", "#!", 1, "text-reset", "text-left", 2, "color", "white"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0"], [2, "color", "white"], [1, "fas", "fa-phone", "me-3"], [2, "color", "white", "width", "-moz-available"], [1, "fas", "fa-envelope", "me-3"], ["href", "mailto:votreconseiller@cpn-aide-aux-entreprise.com", 2, "color", "#fff", "width", "-moz-available", "text-decoration", "none"], [1, "copyright"], ["href", "https://jobid.fr/", 1, "text-reset", "fw-bold", 2, "color", "white"], [1, "nav-link", "con", 3, "routerLink"], [1, "dropdown"], ["role", "button", "id", "dropdownMenuLink", "data-toggle", "dropdown", "aria-haspopup", "true", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle"], ["aria-labelledby", "dropdownMenuLink", 1, "dropdown-menu", "drop"], [1, "dropdown-item", 3, "routerLink"], [1, "dropdown-item", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, HomeComponent_a_8_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, HomeComponent_a_9_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, HomeComponent_a_10_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, HomeComponent_a_11_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, HomeComponent_a_19_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, HomeComponent_a_20_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "aide-aux-entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, HomeComponent_a_28_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, HomeComponent_a_30_Template, 2, 1, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, HomeComponent_li_31_Template, 9, 2, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "section", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h2", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Transition Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h4", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "form", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h4", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "1500K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "section", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "h5", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Notre succ\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h3", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Peut importe votre secteur d'acitivit\u00E9 nous vous offrons une subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "ul", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "562");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "10");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Subventions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "200");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "K+ ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Adh\u00E9rants");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "section", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "ul", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "a", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "img", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "a", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "a", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "img", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Collectivites");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "section", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](127, "img", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Subvention imm\u00E9diate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "p", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "Besoin d'un ch\u00E8que Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Testez Votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "img", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "p", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Des Experts \u00E0 votre disposition pour digitaliser votre entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "a", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](149, "img", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "h5", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Financement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "p", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, "Obtenez le imm\u00E9diatement sur votre devis ou facture");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "a", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "section", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "ul", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](161, "li", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, " Conseils et Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, "Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons et accompagnons tous type de projet de d\u00E9veloppement informatique nous vous orientons au-pr\u00E8s d\u2019agence de d\u00E9veloppement v\u00E9rifier et d\u00E9sign\u00E9.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "section", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "span", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "section", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](172, "Facilit\u00E9 & Rapidit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, " Tous les services du CPN Aide aux entreprises sont destin\u00E9e aux petites et moyennes entreprises et Start-up. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "Nous faisons un suivie et donnons acc\u00E8s a notre r\u00E9seau de partenaires et d\u2019entreprise pour favoris\u00E9 leurs croissance. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](177, "div", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "div", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](182, "img", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "div", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "img", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, "Transformation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](196, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, " digitale");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](198, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](199, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "div", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](201, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](204, "img", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "h3", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](207, "Financement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](208, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, " Im\u00E9diat");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](210, "p", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](211, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "section", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](213, "div", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](214, "h1", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, "Pour obtenir votre subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](216, "p", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, " Le CPN s'engage \u00E0 vous mettre en relation avec une agence. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](218, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](219, " Le cabinet vous permet d'obtenir une subvention sur votre ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](220, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, " d\u00E9veloppement informatique, et vous met a disposition \u00E9galement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](222, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](223, " sont r\u00E9seaux d'entreprises et de partenaire international. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "div", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](225, "img", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](226, "div", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](228, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](229, "Economiser");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, "nos subventions sont calcul\u00E9es par rapport \u00E0 votre investissement quel que soit le poids de votre projet digital. Nos aides varient de 1000 \u00E0 10 000 euro de subvention imm\u00E9diate.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](233, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, "Gagner du temps");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, " Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](239, "Service de qualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](240, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](241, "Tous nos conseillers d\u00E9tiennent un domaine d\u2019expertise qui leurs est propre et pourront vous accompagner dans la r\u00E9alisation de vos projets.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](242, "section", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](243, "div", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "div", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](245, "h1", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](246, "Avis D'entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "p", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](248, " Ils nous ont fait confiance , ont \u00E9t\u00E9 accompagn\u00E9s par le CPN et ont r\u00E9ussi \u00E0 avoir leur ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](249, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](250, " subventions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](251, "div", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](252, "div", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "div", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](254, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](255, "p", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](256, "\"J\u2019ai \u00E9t\u00E9 accompagn\u00E9 par le CPN et j\u2019ai eu ma subvention, excellent service.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](257, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](258, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](259, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](260, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](261, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](262, "img", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](263, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "div", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](265, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](266, "p", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](267, "\"Rapidit\u00E9, efficacit\u00E9 et un tr\u00E8s bon service client, le CPN m\u2019a aid\u00E9 \u00E0 num\u00E9riser mon entreprise.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](269, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](270, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](271, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](272, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](273, "img", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](274, "div", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](275, "div", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](276, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](277, "p", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](278, "\"Gr\u00E2ce \u00E0 la digitalisation de mon entreprise j\u2019ai pu augmenter mon chiffre d\u2019affaire, et c\u2019est gr\u00E2ce au CPN que j\u2019ai pu \u00EAtre bien accompagn\u00E9 et conseill\u00E9.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](279, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](280, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](281, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](282, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](284, "img", 125);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](285, "footer", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](286, "section", 127);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](287, "section", 128);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](288, "div", 129);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](289, "div", 130);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](290, "div", 131);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](291, "h6", 132);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](292, "img", 133);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](293, "p", 134);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](294, " Le Cabinet de Propulsion Num\u00E9rique aide les entreprises \u00E0 se propulser num\u00E9riquement et \u00E0 b\u00E9n\u00E9ficier de financement. CPN est un organisme de financement \u00E0 but non lucratif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](295, "p", 135);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](296, "a", 136);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](297, "i", 137);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](298, "a", 138);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](299, "i", 139);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](300, "a", 140);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](301, "i", 141);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](302, "a", 142);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](303, "i", 143);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](304, "a", 144);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](305, "i", 145);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](306, "div", 146);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](307, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](308, " Menu ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](309, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](310, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](311, "Acceuil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](312, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](313, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](314, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](315, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](316, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](317, "Agenda");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](318, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](319, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](320, "A propos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](321, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](322, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](323, "Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](324, "div", 149);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](325, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](326, " Support ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](327, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](328, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](329, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](330, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](331, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](332, "Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](333, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](334, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](335, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](336, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](337, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](338, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](339, "div", 151);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](340, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](341, " Contact ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](342, "p", 152);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](343, "i", 153);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](344, "+33 0184142394");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](345, "p", 154);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](346, "i", 155);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](347, "a", 156);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](348, "votreconseiller@cpn-aide-aux-entreprise.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](349, "div", 157);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](350, " \u00A9 2021 Copyright:Tous droits r\u00E9serv\u00E9s ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](351, "a", 158);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](352, "Jobid.fr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "logout");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "tpe");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "age");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "col");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Actualite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"]], styles: ["footer[_ngcontent-%COMP%]{\r\n  background: #111D5E !important;\r\n  color: white !important;\r\n\r\n}\r\nfooter[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\r\n  color: white;\r\n}\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: -50px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height: 40px\r\n  }\r\n.nav-link[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n.navlinkwhit[_ngcontent-%COMP%]{\r\n      color: #111D5E !important;\r\n  }\r\n.con[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n      border: none;\r\n      background: red;\r\n      border-radius: 25px;\r\n      width: 120px;\r\n      text-align: center;\r\n  height: 100%;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  }\r\n.topnav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n      border-bottom: 0.1px solid red;\r\n\r\n  }\r\n.nav_t[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n.nav_g[_ngcontent-%COMP%]{\r\n      background-color: #111D5E;\r\n  }\r\n.navwhit[_ngcontent-%COMP%]{\r\n      background-color: #EBECF0;\r\n\r\n  }\r\n.nav_img[_ngcontent-%COMP%]{\r\n      width: 80px;\r\n      margin-bottom: 10px;\r\n  }\r\n.navbar-brand[_ngcontent-%COMP%] {\r\n      display: inline-block;\r\n      padding-top: .3125rem;\r\n      padding-bottom: .3125rem;\r\n      margin-right: 1rem;\r\n      font-size: 1.25rem;\r\n      line-height: inherit;\r\n      white-space: nowrap;\r\n      margin-left: 75px;\r\n      z-index: 5;\r\n  }\r\n\r\n.float_actions[_ngcontent-%COMP%] {\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 6%;\r\nz-index:5900;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\npadding: 5px;\r\nwidth: 120px;\r\nheight: 120px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-content: center;\r\nposition: relative;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\ntext-decoration: none;\r\ndisplay: block;\r\nflex-direction: column;\r\njustify-content: center;\r\nfont-size: 14px;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\nwidth: 100px;\r\nheight: 100px;\r\nmargin-left: 25px;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\ntext-align: center;\r\nmargin: 0;\r\ncolor: white;\r\nmargin-top: 7px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\n content: \">\";\r\n position: absolute;\r\n right: -10px;\r\n top: 15%;\r\n color: white;\r\n font-size: 20px;\r\n width: 40%;\r\n font-weight: bold;\r\n }\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\r\n  text-decoration: none;\r\n   position: absolute;\r\n   right: -180px;\r\n   top: 15%;\r\n   color: black;\r\n   font-size: 17px;\r\n   width: 40%;\r\n   background: white;\r\n   width: 180px;\r\n   border-radius: 25px;\r\n   text-align: center;\r\n   height: 40px;\r\n   display: none;\r\n   justify-content: center;\r\n   align-items: center;\r\n }\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\r\n display: flex;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  display: block;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  font-size: 14px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n  width: 100px;\r\n  height: 100px;\r\n  margin-left: 25px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  margin: 0;\r\n  color: white;\r\n  margin-top: 7px;\r\n }\r\n\r\n\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 5px;\r\n  background: #111d5e;\r\n  display: block;\r\n  position: relative;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\r\n  content: \"\";\r\n  position: absolute;\r\n  width: 30%;\r\n  top: 0;\r\n  left: 0;\r\n  height: 5px;\r\n  background: red;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\r\n  content: \"\";\r\n  position: absolute;\r\n  width: 30%;\r\n  top: 0;\r\n  right: 0;\r\n  height: 5px;\r\n  background: red;\r\n }\r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n.transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n    margin-left: -350px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 1000px;\r\n  max-height: 640px;\r\n  margin-top: 1;\r\n  width: 1050px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 60px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 60px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n.carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: -290px;\r\n  margin-top: 385px;\r\n  }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\r\n    box-sizing: border-box;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\r\n   background: #f5f5f5;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n   display: flex;\r\n   flex-direction: row;\r\n   justify-content: flex-start;\r\n   margin-left: unset;\r\n   width: 650px;\r\n   margin-left: 100px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\r\n    left: 50%;\r\n    position: absolute;\r\n    top: 50%;\r\n    transform: translateX(-50%) translateY(-50%);\r\n    width: 300px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n    content: \"\";\r\n    display: block;\r\n    position: absolute;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff ;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    transition: all 0.3s ease-out;\r\n    transition-delay: 0.3s;\r\n    width: 40px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n    background: #ffffff;\r\n    border-radius: 3px;\r\n    height: 5px;\r\n    transform: rotate(-45deg);\r\n    transform-origin: 0% 100%;\r\n    transition: all 0.3s ease-out;\r\n    width: 15px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    background: transparent;\r\n    border: none;\r\n    border-radius: 20px;\r\n    display: block;\r\n    font-size: 20px;\r\n    height: 40px;\r\n    line-height: 40px;\r\n    opacity: 0;\r\n    outline: none;\r\n    padding: 0 15px;\r\n    position: relative;\r\n    transition: all 0.3s ease-out;\r\n    transition-delay: 0.6s;\r\n    width: 40px;\r\n    z-index: 1;\r\n    color: rgb(223, 223, 223);\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\r\n    transition-delay: 0.3s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\r\n    transition-delay: 0.6s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    transition-delay: 0s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\r\n    transform: rotate(45deg) translateX(15px) translateY(-2px);\r\n    width: 0;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 500px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: 500px;\r\n   }\r\ninput[type=\"text\"][_ngcontent-%COMP%] {\r\n  height: 50px;\r\n  font-size: 30px;\r\n  display: inline-block;\r\n  \r\n  font-weight: 100;\r\n  border: none;\r\n  outline: none;\r\n  color: white;\r\n  padding: 3px;\r\n  padding-right: 60px;\r\n  width: 0px;\r\n  position: absolute;\r\n  top: 0;\r\n  right: 0;\r\n  background: none;\r\n  z-index: 3;\r\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\r\n  cursor: pointer;\r\n  }\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\r\n  border-bottom: 1px solid white;\r\n  }\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\r\n  width: 700px;\r\n  z-index: 1;\r\n  border-bottom: 1px solid white;\r\n  cursor: text;\r\n  }\r\ninput[type=\"submit\"][_ngcontent-%COMP%] {\r\n  height: 50px;\r\n  width: 50px;\r\n  display: inline-block;\r\n  color: white;\r\n  float: right;\r\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\r\n    center center no-repeat;\r\n  text-indent: -10000px;\r\n  border: none;\r\n  position: absolute;\r\n  top: 0;\r\n  right: 0;\r\n  z-index: 2;\r\n  cursor: pointer;\r\n  opacity: 0.4;\r\n  cursor: pointer;\r\n  transition: opacity 0.4s ease;\r\n  }\r\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\r\n  opacity: 0.8;\r\n  }\r\n\r\n.item_num[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  font-weight: 700;\r\n  color: #111d5e;\r\n}\r\n.success_item[_ngcontent-%COMP%] {\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n.item_desc[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  color: #111d5e;\r\n}\r\n.success_list[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  justify-content: space-between;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n.success_txt[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n  font-weight: 400;\r\n  font-size: 14px;\r\n  color: #111d5e;\r\n}\r\n.success_desc[_ngcontent-%COMP%] {\r\n  font-size: 28px;\r\n  color: #111d5e;\r\n}\r\n.container_box[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-direction: row;\r\n\r\n}\r\n.k[_ngcontent-%COMP%]{\r\n  font-weight: bold;\r\n  margin: inherit;\r\n  }\r\n.chiffre[_ngcontent-%COMP%]{\r\n  display: flex;\r\n              flex-direction: column;\r\n              justify-content: center;\r\n              width: 40%;\r\n              align-items: inherit;\r\n  }\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_txt[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n  font-weight: 400;\r\n  font-size: 14px;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_desc[_ngcontent-%COMP%] {\r\n  font-size: 28px;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  justify-content: space-between;\r\n  margin: 0;\r\n  padding: 0;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%] {\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_num[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  font-weight: 700;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_desc[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  color: #111d5e;\r\n }\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%] {\r\n  background-color: #111d5e;\r\n  border-radius: 30px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n  margin-bottom: 14px;\r\n  margin-top: -22px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\n  border: none;\r\n  background: red;\r\n  border-radius: 25px;\r\n  color: white;\r\n  padding: 8px 30px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\r\n  border: none;\r\n  position: relative;\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 0;\r\n  word-wrap: break-word;\r\n  background-color: #111d5e;\r\n  border-radius: 12.25rem;\r\n  color: white;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-img-top[_ngcontent-%COMP%] {\r\n  width: 50%;\r\n  margin-left: 91px;\r\n }\r\n.block2[_ngcontent-%COMP%]{\r\n  background: transparent;\r\n   color: white;\r\n   padding: 19px;\r\n  }\r\n.block2[_ngcontent-%COMP%]:hover{\r\n   border: 5px solid white;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n    border-radius: 25px;\r\n    margin-top: 30px;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n    border-radius: 25px;\r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\n    border: none;\r\n    margin-top: 30px;\r\n    color: white;\r\n    background-color:red; \r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\n    color: white;\r\n    border: none;\r\n    background-color:red; \r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\n    border: none;\r\n    color: white;\r\n    background-color:red; \r\n  }\r\n.home[_ngcontent-%COMP%]{\r\n    color: white;\r\n    font-size: 18px;\r\n    }\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 25rem;\r\n  width: 800px;\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]{\r\n  margin-left: 15px;\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   .text_content[_ngcontent-%COMP%]{\r\n  font-size: 70px;\r\n  color:#111d5e\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  margin-top: -75px;\r\n  margin-left: -17px;\r\n  color:#111d5e\r\n}\r\n\r\n.block4[_ngcontent-%COMP%]{\r\n  background-image: url('sin.png');\r\n  background-repeat: no-repeat;\r\n  background-position:658px 234px;\r\n  margin-top:100px;\r\n}\r\n.block4[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  color:#111d5e;\r\n  font-weight: bold;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{padding-right: 48rem;}\r\n.outer-div[_ngcontent-%COMP%], .inner-div[_ngcontent-%COMP%] {\r\n  height: 378px;\r\n  max-width: 300px;\r\n  margin: 0 auto;\r\n  position: relative;\r\n}\r\n.outer-div[_ngcontent-%COMP%] {\r\n  perspective: 900px;\r\n  perspective-origin: 50% calc(50% - 18em);\r\n}\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 11px 0px 0px 556px\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: -580px 0 0 1065px\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: -12px 0 0 1059px\r\n}\r\n.inner-div[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n  border-radius: 5px;\r\n  font-weight: 400;\r\n  color: black;\r\n  font-size: 1rem;\r\n  text-align: center;\r\n \r\n}\r\n.front[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n  height: 85%;\r\n  background: white;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;\r\n  box-shadow: 0px 1px 15px grey;\r\n  border-radius: 25px;\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\r\n}\r\n.front__face-photo1[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__face-photo2[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__face-photo3[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__text[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 35px;\r\n  margin: 0 auto;\r\n  font-family: \"Montserrat\";\r\n  font-size: 18px;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n}\r\n.front__text-header[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  font-family: \"Oswald\";\r\n  text-transform: uppercase;\r\n  font-size: 20px;\r\n}\r\n.front__text-para[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: -5px;\r\n  color: #000;\r\n  font-size: 14px;\r\n  letter-spacing: 0.4px;\r\n  font-weight: 400;\r\n  font-family: \"Montserrat\", sans-serif;\r\n}\r\n.front-icons[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 0;\r\n  font-size: 14px;\r\n  margin-right: 6px;\r\n  color: gray;\r\n}\r\n.front__text-hover[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  font-size: 10px;\r\n  color: red;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n\r\n  font-weight: 700;\r\n  text-transform: uppercase;\r\n  letter-spacing: .4px;\r\n\r\n  border: 2px solid red;\r\n  padding: 8px 15px;\r\n  border-radius: 30px;\r\n\r\n  background: red;\r\n  color: white;\r\n}\r\n\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  margin-left: 190px;\r\n  height:80%;\r\n  width:80%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\n  padding-top: 30px; \r\n  padding-left: 110px;\r\n  margin-top: -501px; \r\n  float:right\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\n  border-radius: 20px;\r\n   border: 2px solid #C5C5C5;\r\n    margin-top: 20px; \r\n    width: 90%; \r\n  padding: 20px;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\r\n  font-size: 26px;\r\n  font-weight: 700;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover{\r\nborder: 2px solid blue;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover   h3[_ngcontent-%COMP%]{\r\n  color: blue;  \r\n}\r\n\r\n.lastB[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n \r\n}\r\n.block7[_ngcontent-%COMP%]{\r\n  padding-top: 100px\r\n}\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 50px;height: 345px;width: 315px;\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card2[_ngcontent-%COMP%]{\r\n  margin-right: 50px;\r\n  height: 345px;\r\n  width: 315px;\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card3[_ngcontent-%COMP%]{\r\n  margin-right: 50px;\r\n  height: 345px;\r\n  width: 315px;\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 69px 0 0 26px;\r\n  font-size: 20px;\r\n  color:#00BFFF\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n}\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 54px 0 0 26px;\r\n  font-size: 20px;\r\n   color:#00BFFF\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n   top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n   background-size: contain;\r\n   overflow: hidden;\r\n}\r\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 44px 0 0 26px;\r\n  font-size: 20px;\r\n   color:#00BFFF\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n}\r\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n\r\nh1[_ngcontent-%COMP%]{\r\nfont-weight: bold;\r\ncolor: #111d5e;\r\nmargin: 50px 0 50px 0;\r\n}\r\nh5[_ngcontent-%COMP%]{\r\ncolor: #111d5e;\r\nfont-size: 15px;\r\n}\r\np[_ngcontent-%COMP%]{\r\nfont-size: 12px;\r\ncolor: #111d5e;\r\n\r\n}\r\n.footer[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  font-size: 12px;\r\ncolor: #ffffff;\r\n}\r\n.copyright[_ngcontent-%COMP%]{\r\n  background-color: #0c133a;\r\n  color:#fff;\r\n  font-size:13px;\r\n  text-align: center;\r\n}\r\n\r\n\r\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\r\n     \r\n  \r\n    .navbar-brand[_ngcontent-%COMP%] {\r\n        display: inline-block;\r\n        padding-top: .3125rem;\r\n        padding-bottom: .3125rem;\r\n        margin-right: 1rem;\r\n        font-size: 1.25rem;\r\n        line-height: inherit;\r\n        white-space: nowrap;\r\n        margin-left: 0;\r\n        z-index: 5;\r\n  }\r\n  \r\n  .drop[_ngcontent-%COMP%]{\r\n    min-width: -moz-available;\r\n    margin-left: 0px;\r\n  }\r\n  \r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n  }\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\n  content: \">\";\r\n  color: white;\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\n  content: \">\";\r\n  color: white;\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  margin: 0 0 -30px 0;\r\n  display: none;\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\n  position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 25%;\r\n  height: 40%;\r\n  z-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n    \r\n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\n    border-bottom-right-radius:0;\r\n  }\r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #111d5e;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 300px;\r\n  }\r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: 300px;\r\n  }\r\n\r\n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n    font-size: 40px;\r\n    font-weight: 800;\r\n    color: #ffffff;\r\n    width: max-content;\r\n    text-align: center;\r\n    margin-top: auto;\r\n    height: auto;\r\n }\r\n\r\n\r\n .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  margin-left: unset;\r\n  width: -moz-available;\r\n  align-items: center;\r\n  text-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  width: -moz-available;\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: auto;\r\n  margin-top: 250px;\r\n  align-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 30px;\r\n  font-weight: 800;\r\n  color: #ffffff;\r\n  width: -moz-available;\r\n  text-align: center;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  margin-top: 30px;\r\n  border: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  border: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\n  border: none;\r\n  margin-top: 30px;\r\n  color: white;\r\n  background-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\n  color: white;\r\n  border: none;\r\n  background-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\n  border: none;\r\n  color: white;\r\n  background-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\n  display: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nalign-items: center;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\n  margin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\n  margin-left: 20px;\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n \r\n .block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 0;\r\n  width: 250px;\r\n  }\r\n\r\n  \r\n\r\n.block4[_ngcontent-%COMP%]{\r\n  background-image: url('sin.png');\r\n  background-repeat: no-repeat;\r\n  background-position:658px 234px;\r\n  margin-top:100px;\r\n  }\r\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  align-items: flex-start;\r\n  }\r\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 0;\r\n  }\r\n  \r\n   .one[_ngcontent-%COMP%]{\r\n  margin: 40px 0 0 0 ;\r\n  }\r\n   .two[_ngcontent-%COMP%]{\r\n  margin: 0;\r\n  }\r\n   .three[_ngcontent-%COMP%]{\r\n  margin: 0;\r\n  }\r\n  .outer-div[_ngcontent-%COMP%]{\r\n    display: contents;\r\n  }\r\n  \r\n  \r\n  .block5[_ngcontent-%COMP%]{\r\n  max-width: 100%;\r\n  width: 100%;\r\n  }\r\n  .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  margin-left: 0;\r\n  height:100%;\r\n  width:100%\r\n  }\r\n  \r\n  .block6[_ngcontent-%COMP%]{\r\n  padding-top: 30px; \r\n  padding-left:0;\r\n  margin-top: 0; \r\n  max-width: 100%;\r\n  display: contents;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  }\r\n  \r\n  .block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\n    margin: 20px auto 0 auto;\r\n  }\r\n  \r\n  \r\n \r\n .block7[_ngcontent-%COMP%]{\r\n  padding-top: 10px;\r\n}\r\n\r\n    .card1[_ngcontent-%COMP%]{\r\n      margin-right: 0px;\r\n      height: 345px;\r\n      width: 315px;\r\n      }\r\n      .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n      box-shadow: 0px 1px 15px grey;\r\n      border: none;\r\n      border-radius: 71px 14px 71px 14px;\r\n      background-color: #ffffff;\r\n      padding: 40px;\r\n      }\r\n      .card2[_ngcontent-%COMP%]{\r\n        margin-right: 0px;\r\n        height: 345px;\r\n        width: 315px;\r\n      }\r\n      .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n        box-shadow: 0px 1px 15px grey;\r\n        border: none;\r\n        border-radius: 71px 14px 71px 14px;\r\n        background-color: #ffffff;\r\n        padding: 40px;\r\n        }\r\n      .card3[_ngcontent-%COMP%]{\r\n        margin-right: 0px;\r\n        height: 345px;\r\n        width: 315px;\r\n      }\r\n      .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n        box-shadow: 0px 1px 15px grey;\r\n        border: none;\r\n        border-radius: 71px 14px 71px 14px;\r\n        background-color: #ffffff;\r\n        padding: 40px;\r\n        }\r\n        \r\n      .lastB[_ngcontent-%COMP%]{\r\n      width: 80%;\r\n      margin: 50px;\r\n      }\r\n\r\n\r\n      .text-center[_ngcontent-%COMP%]{\r\n      margin-bottom: 50px;\r\n      }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\r\n            \r\n    \r\n  \r\n  .navbar-brand[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    padding-top: .3125rem;\r\n    padding-bottom: .3125rem;\r\n    margin-right: 1rem;\r\n    font-size: 1.25rem;\r\n    line-height: inherit;\r\n    white-space: nowrap;\r\n    margin-left: 0;\r\n    z-index: 5;\r\n}\r\n.nav_img[_ngcontent-%COMP%] {\r\n  width: 80px;\r\n  margin-bottom: 10px;\r\n  margin-left: 38px;\r\n}\r\n\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: 0px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n  }\r\n\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: none;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 18%;\r\nheight: 40%;\r\nz-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\nborder-bottom-right-radius:0;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\nborder: 5px solid #111d5e;\r\nborder-radius: 20px;\r\nheight: 40px;\r\nwidth: 300px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\nopacity: 1;\r\nwidth: 300px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\r\n  width: auto;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 0;\r\n  width: 750px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\nmargin-left: unset;\r\nwidth: -moz-available;\r\nalign-items: center;\r\ntext-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: -moz-available;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nmargin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\nwidth: 200px;\r\nheight: 90px;\r\nbackground: white;\r\nborder-radius: 18px;\r\nmargin-left: auto;\r\nmargin-top: 250px;\r\nalign-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\nfont-size: 30px;\r\nfont-weight: 800;\r\ncolor: #ffffff;\r\nwidth: -moz-available;\r\ntext-align: center;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nmargin-top: 30px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\nborder: none;\r\nmargin-top: 30px;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\ncolor: white;\r\nborder: none;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\nborder: none;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nalign-items: center;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\nmargin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\nmargin-left: 20px;\r\nlist-style: none;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\nwidth: 250px;\r\n}\r\n\r\n\r\n\r\n.block4[_ngcontent-%COMP%]{\r\nbackground-image: url('sin.png');\r\nbackground-repeat: no-repeat;\r\nbackground-position:0px 576px;\r\nmargin-top:100px;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: flex-start;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\n}\r\n\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 40px 0 0 0 ;\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.outer-div[_ngcontent-%COMP%]{\r\ndisplay: contents;\r\n}\r\n\r\n\r\n.block5[_ngcontent-%COMP%]{\r\nmax-width: 100%;\r\nwidth: 100%;\r\n}\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nmargin-left: 0;\r\nheight:100%;\r\nwidth:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\npadding-top: 30px; \r\npadding-left:0;\r\nmargin-top: 0; \r\nmax-width: 100%;\r\ndisplay: contents;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\nmargin: 20px auto 0 auto;\r\n}\r\n\r\n\r\n\r\n.block7[_ngcontent-%COMP%]{\r\npadding-top: 10px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 0px;\r\n  height: 345px;\r\n  width: 315px;\r\n  }\r\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n  .card3[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n    \r\n  .lastB[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin: none;\r\n  }\r\n\r\n\r\n  .text-center[_ngcontent-%COMP%]{\r\n  margin-bottom: 50px;\r\n  }\r\n\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\r\n    \r\n       \r\n  .navbar-brand[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    padding-top: .3125rem;\r\n    padding-bottom: .3125rem;\r\n    margin-right: 1rem;\r\n    font-size: 1.25rem;\r\n    line-height: inherit;\r\n    white-space: nowrap;\r\n    margin-left: 0;\r\n    z-index: 5;\r\n}\r\n.nav_img[_ngcontent-%COMP%] {\r\n  width: 80px;\r\n  margin-bottom: 10px;\r\n  margin-left: 38px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n\r\n  }\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: 0px;\r\n}\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: none;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 12%;\r\nheight: 40%;\r\nz-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n  \r\n.block0[_ngcontent-%COMP%]{\r\n  flex: 0 0 auto;\r\nwidth: -moz-available;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\nborder-bottom-right-radius:0;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\nborder: 5px solid #111d5e;\r\nborder-radius: 20px;\r\nheight: 40px;\r\nwidth: 300px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\nopacity: 1;\r\nwidth: 300px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\r\n  width: auto;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 0;\r\n  width: 750px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\nmargin-left: unset;\r\nwidth: -moz-available;\r\nalign-items: center;\r\ntext-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: -moz-available;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nmargin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\nwidth: 200px;\r\nheight: 90px;\r\nbackground: white;\r\nborder-radius: 18px;\r\nmargin-left: auto;\r\nmargin-top: 250px;\r\nalign-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\nfont-size: 30px;\r\nfont-weight: 800;\r\ncolor: #ffffff;\r\nwidth: -moz-available;\r\ntext-align: center;\r\n}\r\n\r\n\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n  margin-bottom: 14px;\r\n  margin-top: -22px;\r\n  text-align: center;\r\n }\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nmargin-top: 0px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\nborder: none;\r\nmargin-top: 30px;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\ncolor: white;\r\nborder: none;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\nborder: none;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: 100%;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\nmargin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\nmargin-left: 20px;\r\nlist-style: none;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\r\n  width: -moz-available;\r\n}\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\nwidth: -moz-available;\r\n}\r\n\r\n\r\n\r\n.block4[_ngcontent-%COMP%]{\r\nbackground-image: url('sin.png');\r\nbackground-repeat: no-repeat;\r\nbackground-position:658px 234px;\r\nmargin-top:0px;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: flex-start;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0px;\r\n}\r\n\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 40px 0 0 0 ;\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.outer-div[_ngcontent-%COMP%]{\r\ndisplay: contents;\r\n}\r\n\r\n\r\n.block5[_ngcontent-%COMP%]{\r\nmax-width: 100%;\r\nwidth: 100%;\r\n}\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nmargin-left: 0;\r\nheight:100%;\r\nwidth:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\npadding-top: 30px; \r\npadding-left:0;\r\nmargin-top: 0; \r\nmax-width: 100%;\r\ndisplay: contents;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\nmargin: 20px auto 0 auto;\r\n}\r\n\r\n\r\n\r\n.block7[_ngcontent-%COMP%]{\r\npadding-top: 10px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 0px;\r\n  height: 345px;\r\n  width: 315px;\r\n  }\r\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n  .card3[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n    \r\n  .lastB[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin: none;\r\n  }\r\n\r\n\r\n  .text-center[_ngcontent-%COMP%]{\r\n  margin-bottom: 50px;\r\n  }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\r\n \r\n .drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: -50px;\r\n }     \r\n\r\n.float_actions[_ngcontent-%COMP%] {\r\n  position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 8%;\r\n  z-index:5900;\r\n  }\r\n  \r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  margin: 0 0 -30px 0;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\n  padding: 5px;\r\n  width: 120px;\r\n  height: 120px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-content: center;\r\n  position: relative;\r\n  }\r\n  \r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  display: block;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  font-size: 14px;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n  width: 100px;\r\n  height: 100px;\r\n  margin-left: 25px;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  margin: 0;\r\n  color: white;\r\n  margin-top: 7px;\r\n  }\r\n  \r\n  \r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\n   content: \">\";\r\n   position: absolute;\r\n   right: -10px;\r\n   top: 15%;\r\n   color: white;\r\n   font-size: 20px;\r\n   width: 40%;\r\n   font-weight: bold;\r\n   }\r\n   \r\n   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\r\n    text-decoration: none;\r\n     position: absolute;\r\n     right: -180px;\r\n     top: 15%;\r\n     color: black;\r\n     font-size: 17px;\r\n     width: 40%;\r\n     background: white;\r\n     width: 180px;\r\n     border-radius: 25px;\r\n     text-align: center;\r\n     height: 40px;\r\n     display: none;\r\n     justify-content: center;\r\n     align-items: center;\r\n   }\r\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\r\n   display: flex;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n    text-decoration: none;\r\n    display: block;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    font-size: 14px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n    width: 100px;\r\n    height: 100px;\r\n    margin-left: 25px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n    margin: 0;\r\n    color: white;\r\n    margin-top: 7px;\r\n   }\r\n   \r\n      \r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n  .transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n    margin-left: -140px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 1;\r\n  width: 993px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: 80px;\r\n  margin-top: 335px;\r\n  }\r\n  \r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    margin-left: unset;\r\n    width: -moz-available;\r\n    margin-left: 0px;\r\n    }\r\n    \r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 400px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: -moz-available;\r\n   }\r\n\r\n\r\n   \r\n   .home[_ngcontent-%COMP%]{\r\n    color: white;\r\n    font-size: 18px;\r\n    text-align: center;\r\n    }\r\n\r\n    .block2[_ngcontent-%COMP%]{\r\n      background: transparent;\r\n       color: white;\r\n       padding: 22px;\r\n      }\r\n      .block2[_ngcontent-%COMP%]:hover{\r\n       border: 5px solid white;\r\n       padding: 18px;\r\n      }\r\n\r\n       \r\n \r\n       .block4[_ngcontent-%COMP%]{\r\n        background-image: url('sin.png');\r\n        background-repeat: no-repeat;\r\n        background-position:258px 220px;\r\n        margin-top:0px;\r\n        }\r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        align-items: flex-start;\r\n        }\r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n          padding-right: 50%;\r\n           }\r\n\r\n        .one[_ngcontent-%COMP%]{\r\n          margin: 11px 0px 0px 263px\r\n          }\r\n           .two[_ngcontent-%COMP%]{\r\n          margin: -580px 0 0 650px\r\n          }\r\n          .three[_ngcontent-%COMP%]{\r\n          margin: -12px 0 0 650px\r\n          }\r\n\r\n          \r\n          .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n            margin-left: 60px;\r\n            height:100%;\r\n            width:100%\r\n          }\r\n\r\n          \r\n \r\n          .block6[_ngcontent-%COMP%]{\r\n            padding-top: 30px; \r\n            padding-left: 110px;\r\n            margin-top: -490px; \r\n            float:right\r\n          }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 1200px) and (max-width : 1500px) {\r\n      \r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n  .transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n   padding-left: 115px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 1;\r\n  width: 993px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: 80px;\r\n  margin-top: 335px;\r\n  }\r\n  \r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    margin-left: unset;\r\n    width: -moz-available;\r\n    margin-left: 0px;\r\n    }\r\n    \r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 400px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: -moz-available;\r\n   }\r\n\r\n       \r\n \r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        align-items: flex-start;\r\n        }\r\n     \r\n\r\n        .one[_ngcontent-%COMP%]{\r\n          margin: 11px 0px 0px 456px\r\n          }\r\n           .two[_ngcontent-%COMP%]{\r\n          margin: -580px 0 0 792px\r\n          }\r\n          .three[_ngcontent-%COMP%]{\r\n          margin: -12px 0 0 792px\r\n          }\r\n          \r\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsOEJBQThCO0VBQzlCLHVCQUF1Qjs7QUFFekI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZUFBZTtFQUNmO0VBQ0E7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0Y7TUFDTSx1QkFBdUI7TUFDdkIsWUFBWTtNQUNaLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLGtCQUFrQjtFQUN0QixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7QUFDQTtNQUNJLDhCQUE4Qjs7RUFFbEM7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0E7TUFDSSx5QkFBeUI7O0VBRTdCO0FBQ0E7TUFDSSxXQUFXO01BQ1gsbUJBQW1CO0VBQ3ZCO0FBQ0c7TUFDQyxxQkFBcUI7TUFDckIscUJBQXFCO01BQ3JCLHdCQUF3QjtNQUN4QixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtNQUNwQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLFVBQVU7RUFDZDtBQUdGLDRHQUE0RztBQUM1RztBQUNBLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxZQUFZO0FBQ1o7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjtBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCLGNBQWM7QUFDZCxzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLGVBQWU7QUFDZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLGFBQWE7QUFDYixpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixTQUFTO0FBQ1QsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUdDO0NBQ0EsWUFBWTtDQUNaLGtCQUFrQjtDQUNsQixZQUFZO0NBQ1osUUFBUTtDQUNSLFlBQVk7Q0FDWixlQUFlO0NBQ2YsVUFBVTtDQUNWLGlCQUFpQjtDQUNqQjtBQUNBO0VBQ0MscUJBQXFCO0dBQ3BCLGtCQUFrQjtHQUNsQixhQUFhO0dBQ2IsUUFBUTtHQUNSLFlBQVk7R0FDWixlQUFlO0dBQ2YsVUFBVTtHQUNWLGlCQUFpQjtHQUNqQixZQUFZO0dBQ1osbUJBQW1CO0dBQ25CLGtCQUFrQjtHQUNsQixZQUFZO0dBQ1osYUFBYTtHQUNiLHVCQUF1QjtHQUN2QixtQkFBbUI7Q0FDckI7QUFDQztDQUNELGFBQWE7Q0FDYjtBQUVBO0VBQ0MscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCO0FBQ0E7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFlBQVk7RUFDWixlQUFlO0NBQ2hCO0FBSUQsZ0hBQWdIO0FBQ2hILG1IQUFtSDtBQUVuSDtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFFRCxrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7QUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztBQUNBO0VBQ0MsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsYUFBYTtDQUNkO0FBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakI7QUFHQTtJQUNFLHNCQUFzQjtHQUN2QjtBQUNBO0dBQ0EsbUJBQW1CO0dBQ25CO0FBQ0E7R0FDQSxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDJCQUEyQjtHQUMzQixrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLGtCQUFrQjtHQUNsQjtBQUNBO0lBQ0MsU0FBUztJQUNULGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsNENBQTRDO0lBQzVDLFlBQVk7R0FDYjtBQUNBO0lBQ0MsV0FBVztJQUNYLGNBQWM7SUFDZCxrQkFBa0I7R0FDbkI7QUFDQTtJQUNDLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDZCQUE2QjtJQUM3QixzQkFBc0I7SUFDdEIsV0FBVztHQUNaO0FBQ0E7SUFDQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIseUJBQXlCO0lBQ3pCLDZCQUE2QjtJQUM3QixXQUFXO0dBQ1o7QUFDQTtJQUNDLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxlQUFlO0lBQ2YsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsYUFBYTtJQUNiLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsVUFBVTtJQUNWLHlCQUF5QjtHQUMxQjtBQUNBO0lBQ0Msc0JBQXNCO0dBQ3ZCO0FBQ0E7SUFDQyxzQkFBc0I7R0FDdkI7QUFDQTtJQUNDLG9CQUFvQjtHQUNyQjtBQUNBO0lBQ0MsMERBQTBEO0lBQzFELFFBQVE7R0FDVDtBQUNBO0lBQ0MseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtHQUNiO0FBQ0E7SUFDQyxVQUFVO0lBQ1YsWUFBWTtHQUNiO0FBSUg7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHFCQUFxQjs7RUFFckIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUTtFQUNSLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsbURBQW1EO0VBQ25ELGVBQWU7RUFDZjtBQUVBO0VBQ0EsOEJBQThCO0VBQzlCO0FBRUE7RUFDQSxZQUFZO0VBQ1osVUFBVTtFQUNWLDhCQUE4QjtFQUM5QixZQUFZO0VBQ1o7QUFDQTtFQUNBLFlBQVk7RUFDWixXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixZQUFZO0VBQ1o7MkJBQ3lCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1IsVUFBVTtFQUNWLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZTtFQUNmLDZCQUE2QjtFQUM3QjtBQUVBO0VBQ0EsWUFBWTtFQUNaO0FBSUQsd0hBQXdIO0FBRXpIO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxTQUFTO0VBQ1QsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtBQUNaO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1COztBQUVyQjtBQUdBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZjtBQUdGO0VBQ0UsYUFBYTtjQUNELHNCQUFzQjtjQUN0Qix1QkFBdUI7Y0FDdkIsVUFBVTtjQUNWLG9CQUFvQjtFQUNoQztBQUVELGtIQUFrSDtBQUVuSDtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtDQUNYO0FBQ0E7RUFDQyxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsU0FBUztFQUNULGNBQWM7Q0FDZjtBQUVBLGtIQUFrSDtBQUNsSDtFQUNDLHlCQUF5QjtFQUN6QixtQkFBbUI7Q0FDcEI7QUFDQTtFQUNDLG1CQUFtQjtFQUNuQixpQkFBaUI7Q0FDbEI7QUFDQTtFQUNDLG1CQUFtQjtDQUNwQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGlCQUFpQjtDQUNsQjtBQUNBO0VBQ0MsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6Qix1QkFBdUI7RUFDdkIsWUFBWTtDQUNiO0FBQ0E7RUFDQyxVQUFVO0VBQ1YsaUJBQWlCO0NBQ2xCO0FBRUE7RUFDQyx1QkFBdUI7R0FDdEIsWUFBWTtHQUNaLGFBQWE7RUFDZDtBQUNBO0dBQ0MsdUJBQXVCO0VBQ3hCO0FBRUE7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0VBQ2xCO0FBQ0E7RUFDQSxtQkFBbUI7RUFDbkI7QUFDQTtJQUNFLG1CQUFtQjtFQUNyQjtBQUNBO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osb0JBQW9CO0VBQ3RCO0FBQ0E7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLG9CQUFvQjtFQUN0QjtBQUNBO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixvQkFBb0I7RUFDdEI7QUFDQTtJQUNFLFlBQVk7SUFDWixlQUFlO0lBQ2Y7QUFDSCxrSEFBa0g7QUFFbkg7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCO0FBQ0Y7QUFFQyxrSEFBa0g7QUFFbkg7RUFDRSxnQ0FBMEQ7RUFDMUQsNEJBQTRCO0VBQzVCLCtCQUErQjtFQUMvQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGFBQWE7RUFDYixpQkFBaUI7QUFDbkI7QUFFQSxrQkFBa0Isb0JBQW9CLENBQUM7QUFHdkM7O0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsd0NBQXdDO0FBQzFDO0FBQ0M7QUFDRDtBQUNBO0FBQ0M7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQjs7QUFFcEI7QUFJQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLG1DQUEyQjtVQUEzQiwyQkFBMkI7RUFDM0IsNkNBQTZDO0VBQzdDLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0FBQ1Q7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCOztFQUVsQix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCOzttQkFFaUI7QUFDbkI7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7O0VBRWxCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixtQ0FBMkI7VUFBM0IsMkJBQTJCO0FBQzdCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6QixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFdBQVc7RUFDWCxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixxQ0FBcUM7QUFDdkM7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0FBQ2I7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsZUFBZTtFQUNmLFVBQVU7RUFDVixtQ0FBMkI7VUFBM0IsMkJBQTJCOztFQUUzQixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLG9CQUFvQjs7RUFFcEIscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixtQkFBbUI7O0VBRW5CLGVBQWU7RUFDZixZQUFZO0FBQ2Q7QUFPQyxrSEFBa0g7QUFDbEg7RUFDQyxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWO0FBQ0Y7QUFDQyxrSEFBa0g7QUFFbkg7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQjtBQUNGO0FBQ0E7RUFDRSxtQkFBbUI7R0FDbEIseUJBQXlCO0lBQ3hCLGdCQUFnQjtJQUNoQixVQUFVO0VBQ1osYUFBYTtBQUNmO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUdDLGtIQUFrSDtBQUNsSDtFQUNDLFdBQVc7O0FBRWI7QUFDQTtFQUNFO0FBQ0Y7QUFFQTtFQUNFLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxZQUFZO0FBQy9DO0FBQ0E7RUFDRSw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLGtDQUFrQztFQUNsQyx5QkFBeUI7RUFDekIsYUFBYTtBQUNmO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLFlBQVk7RUFDWixrQ0FBa0M7RUFDbEMseUJBQXlCO0VBQ3pCLGFBQWE7QUFDZjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0FBQ2Q7QUFDQTtFQUNFLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0FBQ2Y7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Y7QUFDRjtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7R0FDakIsV0FBVztFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7R0FDakIsd0JBQXdCO0dBQ3hCLGdCQUFnQjtBQUNuQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFDQSwrR0FBK0c7QUFHL0c7QUFDQSxpQkFBaUI7QUFDakIsY0FBYztBQUNkLHFCQUFxQjtBQUNyQjtBQUVBO0FBQ0EsY0FBYztBQUNkLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGNBQWM7O0FBRWQ7QUFDQTtFQUNFLGVBQWU7QUFDakIsY0FBYztBQUNkO0FBR0E7RUFDRSx5QkFBeUI7RUFDekIsVUFBVTtFQUNWLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFFRSwwR0FBMEc7QUFHMUcsMkJBQTJCO0FBQ3pCOztFQUVGLHNHQUFzRztJQUNwRztRQUNJLHFCQUFxQjtRQUNyQixxQkFBcUI7UUFDckIsd0JBQXdCO1FBQ3hCLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsVUFBVTtFQUNoQjs7RUFFQTtJQUNFLHlCQUF5QjtJQUN6QixnQkFBZ0I7RUFDbEI7O0FBRUY7RUFDRSxzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCO0FBQ0YsdUhBQXVIO0FBQ3ZIO0VBQ0UsZUFBZTtBQUNqQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLGlCQUFpQjtBQUNqQixlQUFlOztBQUVmO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7RUFDRSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0FBQ2hDO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsV0FBVztFQUNYLGNBQWM7QUFDaEI7O0FBRUE7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0VBQ0UsaUdBQWlHO0VBQ2pHO0lBQ0UsNEJBQTRCO0VBQzlCO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7OztDQUdBO0VBQ0MsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLHVCQUFrQjtFQUFsQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLHVCQUFrQjtFQUFsQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxxQkFBcUI7RUFDckIsa0JBQWtCO0FBQ3BCOzs7QUFHQSx5R0FBeUc7Ozs7QUFJekc7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFNBQVM7QUFDWDtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixZQUFZO0FBQ1osU0FBUztBQUNUO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBQ0E7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixvQkFBb0I7QUFDdEI7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLG9CQUFvQjtBQUN0Qjs7OztBQUlBLGdIQUFnSDtBQUNoSDtFQUNFLGFBQWE7QUFDZixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLFdBQVc7QUFDWCxtQkFBbUI7QUFDbkI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtBQUNyQjtDQUNDLDhHQUE4RztDQUM5RztFQUNDLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1o7O0VBRUEsK0ZBQStGOztBQUVqRztFQUNFLGdDQUEwRDtFQUMxRCw0QkFBNEI7RUFDNUIsK0JBQStCO0VBQy9CLGdCQUFnQjtFQUNoQjtFQUNBO0VBQ0EsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkI7RUFDQTtFQUNBLGdCQUFnQjtFQUNoQjs7R0FFQztFQUNELG1CQUFtQjtFQUNuQjtHQUNDO0VBQ0QsU0FBUztFQUNUO0dBQ0M7RUFDRCxTQUFTO0VBQ1Q7RUFDQTtJQUNFLGlCQUFpQjtFQUNuQjs7RUFFQSxrR0FBa0c7RUFDbEc7RUFDQSxlQUFlO0VBQ2YsV0FBVztFQUNYO0VBQ0E7RUFDQSxjQUFjO0VBQ2QsV0FBVztFQUNYO0VBQ0E7RUFDQSxrR0FBa0c7RUFDbEc7RUFDQSxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25COztFQUVBO0lBQ0Usd0JBQXdCO0VBQzFCOzs7Q0FHRCxrSEFBa0g7Q0FDbEg7RUFDQyxpQkFBaUI7QUFDbkI7O0lBRUk7TUFDRSxpQkFBaUI7TUFDakIsYUFBYTtNQUNiLFlBQVk7TUFDWjtNQUNBO01BQ0EsNkJBQTZCO01BQzdCLFlBQVk7TUFDWixrQ0FBa0M7TUFDbEMseUJBQXlCO01BQ3pCLGFBQWE7TUFDYjtNQUNBO1FBQ0UsaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixZQUFZO01BQ2Q7TUFDQTtRQUNFLDZCQUE2QjtRQUM3QixZQUFZO1FBQ1osa0NBQWtDO1FBQ2xDLHlCQUF5QjtRQUN6QixhQUFhO1FBQ2I7TUFDRjtRQUNFLGlCQUFpQjtRQUNqQixhQUFhO1FBQ2IsWUFBWTtNQUNkO01BQ0E7UUFDRSw2QkFBNkI7UUFDN0IsWUFBWTtRQUNaLGtDQUFrQztRQUNsQyx5QkFBeUI7UUFDekIsYUFBYTtRQUNiOztNQUVGO01BQ0EsVUFBVTtNQUNWLFlBQVk7TUFDWjs7O01BR0E7TUFDQSxtQkFBbUI7TUFDbkI7O0lBRUY7QUFFSCxnQ0FBZ0M7QUFDN0I7OztFQUdGLHNHQUFzRztFQUN0RztJQUNFLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsVUFBVTtBQUNkO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLHNCQUFzQjtFQUN0Qiw4QkFBOEI7RUFDOUIsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7RUFDbEI7O0FBRUYsdUhBQXVIO0FBQ3ZIO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixZQUFZOztBQUVaO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsWUFBWTtBQUNaLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsaUJBQWlCO0FBQ2pCLGVBQWU7O0FBRWY7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVjtBQUNBO0FBQ0EsVUFBVTtBQUNWLG1CQUFtQjtBQUNuQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFdBQVc7QUFDWCxjQUFjO0FBQ2Q7O0FBRUE7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0EsaUdBQWlHO0FBQ2pHO0FBQ0EsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWixZQUFZO0FBQ1o7QUFDQTtBQUNBLFVBQVU7QUFDVixZQUFZO0FBQ1o7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsa0JBQWtCO0FBQ2xCLHFCQUFxQjtBQUNyQixtQkFBbUI7QUFDbkIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVDtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CLGlCQUFpQjtBQUNqQixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSx1QkFBa0I7QUFBbEIsa0JBQWtCO0FBQ2xCOztBQUVBO0FBQ0EsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQixjQUFjO0FBQ2QscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjs7QUFFQSx5R0FBeUc7Ozs7QUFJekc7QUFDQSxtQkFBbUI7QUFDbkIsZ0JBQWdCO0FBQ2hCLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixZQUFZO0FBQ1osU0FBUztBQUNUO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQSxZQUFZO0FBQ1osZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjs7OztBQUlBLGdIQUFnSDtBQUNoSDtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLFdBQVc7QUFDWCxtQkFBbUI7QUFDbkI7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixnQkFBZ0I7QUFDaEIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CO0FBQ0EsOEdBQThHO0FBQzlHO0FBQ0EsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWjs7QUFFQSwrRkFBK0Y7O0FBRS9GO0FBQ0EsZ0NBQTBEO0FBQzFELDRCQUE0QjtBQUM1Qiw2QkFBNkI7QUFDN0IsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCOztBQUVBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLGtHQUFrRztBQUNsRztBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBLGNBQWM7QUFDZCxXQUFXO0FBQ1g7QUFDQTtBQUNBLGtHQUFrRztBQUNsRztBQUNBLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2QsYUFBYTtBQUNiLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkI7O0FBRUE7QUFDQSx3QkFBd0I7QUFDeEI7OztBQUdBLGtIQUFrSDtBQUNsSDtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtFQUNaO0VBQ0E7RUFDQSw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLGtDQUFrQztFQUNsQyx5QkFBeUI7RUFDekIsYUFBYTtFQUNiO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjtFQUNGO0lBQ0UsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixZQUFZO0VBQ2Q7RUFDQTtJQUNFLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osa0NBQWtDO0lBQ2xDLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2I7O0VBRUY7SUFDRSxXQUFXO0lBQ1gsWUFBWTtFQUNkOzs7RUFHQTtFQUNBLG1CQUFtQjtFQUNuQjs7O0lBR0U7QUFHSCwwQkFBMEI7QUFDdkI7O09BRUcsc0dBQXNHO0VBQzNHO0lBQ0UscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7QUFDQTtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSxzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCOztFQUVsQjtBQUNGO0VBQ0UseUJBQXlCO0VBQ3pCLGdCQUFnQjtBQUNsQjtBQUNBLHVIQUF1SDtBQUN2SDtBQUNBLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsWUFBWTtBQUNaLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsaUJBQWlCO0FBQ2pCLGVBQWU7O0FBRWY7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVjtBQUNBO0FBQ0EsVUFBVTtBQUNWLG1CQUFtQjtBQUNuQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFdBQVc7QUFDWCxjQUFjO0FBQ2Q7O0FBRUE7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0EsaUdBQWlHO0FBQ2pHO0VBQ0UsY0FBYztBQUNoQixxQkFBcUI7QUFDckI7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osWUFBWTtBQUNaO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsWUFBWTtBQUNaOztBQUVBO0VBQ0UsV0FBVztBQUNiO0FBQ0E7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0FBQ2Q7QUFDQTtBQUNBLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLGtCQUFrQjtBQUNsQixxQkFBcUI7QUFDckIsbUJBQW1CO0FBQ25CLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0EsYUFBYTtBQUNiLHFCQUFxQjtBQUNyQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1Q7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osaUJBQWlCO0FBQ2pCLG1CQUFtQjtBQUNuQixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsdUJBQWtCO0FBQWxCLGtCQUFrQjtBQUNsQjs7QUFFQTtBQUNBLGVBQWU7QUFDZixnQkFBZ0I7QUFDaEIsY0FBYztBQUNkLHFCQUFxQjtBQUNyQixrQkFBa0I7QUFDbEI7O0FBRUEseUdBQXlHOztBQUV6RztFQUNFLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0NBQ25COztBQUVEO0FBQ0EsbUJBQW1CO0FBQ25CLGVBQWU7QUFDZixTQUFTO0FBQ1Q7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsWUFBWTtBQUNaLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVDtBQUNBO0FBQ0EsWUFBWTtBQUNaLGdCQUFnQjtBQUNoQixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7Ozs7QUFJQSxnSEFBZ0g7QUFDaEg7QUFDQSxhQUFhO0FBQ2IsV0FBVztBQUNYOztBQUVBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsZ0JBQWdCO0FBQ2hCLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjtBQUNBO0VBQ0UscUJBQXFCO0FBQ3ZCO0FBQ0EsOEdBQThHO0FBQzlHO0FBQ0EsZ0JBQWdCO0FBQ2hCLHFCQUFxQjtBQUNyQjs7QUFFQSwrRkFBK0Y7O0FBRS9GO0FBQ0EsZ0NBQTBEO0FBQzFELDRCQUE0QjtBQUM1QiwrQkFBK0I7QUFDL0IsY0FBYztBQUNkO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCOztBQUVBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLGtHQUFrRztBQUNsRztBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBLGNBQWM7QUFDZCxXQUFXO0FBQ1g7QUFDQTtBQUNBLGtHQUFrRztBQUNsRztBQUNBLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2QsYUFBYTtBQUNiLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkI7O0FBRUE7QUFDQSx3QkFBd0I7QUFDeEI7OztBQUdBLGtIQUFrSDtBQUNsSDtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtFQUNaO0VBQ0E7RUFDQSw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLGtDQUFrQztFQUNsQyx5QkFBeUI7RUFDekIsYUFBYTtFQUNiO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjtFQUNGO0lBQ0UsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixZQUFZO0VBQ2Q7RUFDQTtJQUNFLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osa0NBQWtDO0lBQ2xDLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2I7O0VBRUY7SUFDRSxXQUFXO0lBQ1gsWUFBWTtFQUNkOzs7RUFHQTtFQUNBLG1CQUFtQjtFQUNuQjs7SUFFRTtBQUdILDZCQUE2QjtBQUMxQjtDQUNILG1FQUFtRTtDQUNuRTtFQUNDLHlCQUF5QjtFQUN6QixrQkFBa0I7Q0FDbkI7QUFDRCw0R0FBNEc7QUFDNUc7RUFDRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsWUFBWTtFQUNaOztFQUVBO0VBQ0EsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDhCQUE4QjtFQUM5QjtFQUNBO0VBQ0EsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCOztFQUVBO0VBQ0EscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtFQUNBO0VBQ0EsWUFBWTtFQUNaLGFBQWE7RUFDYixpQkFBaUI7RUFDakI7RUFDQTtFQUNBLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtFQUNaLGVBQWU7RUFDZjs7O0dBR0M7R0FDQSxZQUFZO0dBQ1osa0JBQWtCO0dBQ2xCLFlBQVk7R0FDWixRQUFRO0dBQ1IsWUFBWTtHQUNaLGVBQWU7R0FDZixVQUFVO0dBQ1YsaUJBQWlCO0dBQ2pCOztHQUVBO0lBQ0MscUJBQXFCO0tBQ3BCLGtCQUFrQjtLQUNsQixhQUFhO0tBQ2IsUUFBUTtLQUNSLFlBQVk7S0FDWixlQUFlO0tBQ2YsVUFBVTtLQUNWLGlCQUFpQjtLQUNqQixZQUFZO0tBQ1osbUJBQW1CO0tBQ25CLGtCQUFrQjtLQUNsQixZQUFZO0tBQ1osYUFBYTtLQUNiLHVCQUF1QjtLQUN2QixtQkFBbUI7R0FDckI7SUFDQztHQUNELGFBQWE7R0FDYjtHQUNBO0lBQ0MscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGVBQWU7R0FDaEI7R0FDQTtJQUNDLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0dBQ2xCO0dBQ0E7SUFDQyxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7SUFDWixlQUFlO0dBQ2hCOzs7QUFHSCxrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7RUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztDQUNBO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtDQUNiO0NBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakI7O0VBRUE7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQjs7R0FFRDtJQUNDLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7R0FDYjtHQUNBO0lBQ0MsVUFBVTtJQUNWLHFCQUFxQjtHQUN0Qjs7O0dBR0EsMkZBQTJGO0dBQzNGO0lBQ0MsWUFBWTtJQUNaLGVBQWU7SUFDZixrQkFBa0I7SUFDbEI7O0lBRUE7TUFDRSx1QkFBdUI7T0FDdEIsWUFBWTtPQUNaLGFBQWE7TUFDZDtNQUNBO09BQ0MsdUJBQXVCO09BQ3ZCLGFBQWE7TUFDZDs7T0FFQyxrSEFBa0g7O09BRWxIO1FBQ0MsZ0NBQTBEO1FBQzFELDRCQUE0QjtRQUM1QiwrQkFBK0I7UUFDL0IsY0FBYztRQUNkO1FBQ0E7UUFDQSxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QjtRQUNBO1VBQ0Usa0JBQWtCO1dBQ2pCOztRQUVIO1VBQ0U7VUFDQTtXQUNDO1VBQ0Q7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQSxrSEFBa0g7VUFDbEg7WUFDRSxpQkFBaUI7WUFDakIsV0FBVztZQUNYO1VBQ0Y7O1VBRUEsa0hBQWtIOztVQUVsSDtZQUNFLGlCQUFpQjtZQUNqQixtQkFBbUI7WUFDbkIsa0JBQWtCO1lBQ2xCO1VBQ0Y7O0lBRU47QUFHQyw4QkFBOEI7QUFDL0I7O0FBRUosa0hBQWtIO0FBQ2xIO0VBQ0Usa0JBQWtCO0VBQ2xCO0VBQ0E7SUFDRSxhQUFhO0lBQ2Isa0JBQWtCO0dBQ25CLG1CQUFtQjtJQUNsQixrQkFBa0I7SUFDbEIsc0JBQXNCO0lBQ3RCO0FBQ0o7RUFDRSxrQkFBa0I7RUFDbEIsNEJBQXVCO0VBQXZCLHVCQUF1QjtFQUN2QixpQ0FBaUM7Q0FDbEM7Q0FDQTtFQUNDLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLFlBQVk7Q0FDYjtDQUNBO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25CO0NBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCOztFQUVBO0lBQ0UsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0Isa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEI7O0dBRUQ7SUFDQyx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0dBQ2I7R0FDQTtJQUNDLFVBQVU7SUFDVixxQkFBcUI7R0FDdEI7O09BRUksa0hBQWtIOztRQUVqSDtRQUNBLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCOzs7UUFHQTtVQUNFO1VBQ0E7V0FDQztVQUNEO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O0lBRU4iLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5mb290ZXJ7XHJcbiAgYmFja2dyb3VuZDogIzExMUQ1RSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG5cclxufVxyXG5mb290ZXIgYXtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5kcm9we1xyXG4gIG1pbi13aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgbWFyZ2luLWxlZnQ6IC01MHB4O1xyXG59XHJcblxyXG4ubmF2YmFyLW5hdntcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBoZWlnaHQ6IDQwcHhcclxuICB9XHJcbiAgLm5hdi1saW5re1xyXG4gICAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLm5hdmxpbmt3aGl0e1xyXG4gICAgICBjb2xvcjogIzExMUQ1RSAhaW1wb3J0YW50O1xyXG4gIH1cclxuLmNvbntcclxuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAudG9wbmF2IGxpIGE6aG92ZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAwLjFweCBzb2xpZCByZWQ7XHJcblxyXG4gIH1cclxuICAubmF2X3R7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubmF2X2d7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxMTFENUU7XHJcbiAgfVxyXG4gIC5uYXZ3aGl0e1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xyXG5cclxuICB9XHJcbiAgLm5hdl9pbWd7XHJcbiAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIH1cclxuICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICBtYXJnaW4tbGVmdDogNzVweDtcclxuICAgICAgei1pbmRleDogNTtcclxuICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBzaWRlIGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uZmxvYXRfYWN0aW9ucyB7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiA2JTtcclxuei1pbmRleDo1OTAwO1xyXG59XHJcblxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyB7XHJcbnBhZGRpbmc6IDVweDtcclxud2lkdGg6IDEyMHB4O1xyXG5oZWlnaHQ6IDEyMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24tY29udGVudDogY2VudGVyO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIHtcclxudGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5kaXNwbGF5OiBibG9jaztcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmZvbnQtc2l6ZTogMTRweDtcclxufVxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbndpZHRoOiAxMDBweDtcclxuaGVpZ2h0OiAxMDBweDtcclxubWFyZ2luLWxlZnQ6IDI1cHg7XHJcbn1cclxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX3RleHQge1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbm1hcmdpbjogMDtcclxuY29sb3I6IHdoaXRlO1xyXG5tYXJnaW4tdG9wOiA3cHg7XHJcbn1cclxuXHJcblxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6OmJlZm9yZXtcclxuIGNvbnRlbnQ6IFwiPlwiO1xyXG4gcG9zaXRpb246IGFic29sdXRlO1xyXG4gcmlnaHQ6IC0xMHB4O1xyXG4gdG9wOiAxNSU7XHJcbiBjb2xvcjogd2hpdGU7XHJcbiBmb250LXNpemU6IDIwcHg7XHJcbiB3aWR0aDogNDAlO1xyXG4gZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiB9XHJcbiAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLnRlc3RtZWdpe1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICByaWdodDogLTE4MHB4O1xyXG4gICB0b3A6IDE1JTtcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gICBmb250LXNpemU6IDE3cHg7XHJcbiAgIHdpZHRoOiA0MCU7XHJcbiAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICB3aWR0aDogMTgwcHg7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICBkaXNwbGF5OiBub25lO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuIH1cclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6aG92ZXIgLnRlc3RtZWdpe1xyXG4gZGlzcGxheTogZmxleDtcclxuIH1cclxuIFxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDI1cHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tdG9wOiA3cHg7XHJcbiB9XHJcbiBcclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmhvbWUgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmRpdmlkZXIgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA1cHg7XHJcbiAgYmFja2dyb3VuZDogIzExMWQ1ZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lOjpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgaGVpZ2h0OiA1cHg7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZTo6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgdG9wOiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGhlaWdodDogNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuIH1cclxuIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5jb250ZW50e1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuICB9XHJcbiAgLnRyYW5zdGlvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTM1MHB4O1xyXG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XHJcbiAgYmFja2dyb3VuZDojMTExZDVlO1xyXG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcclxuICBoZWlnaHQ6IDEwMDBweDtcclxuICBtYXgtaGVpZ2h0OiA2NDBweDtcclxuICBtYXJnaW4tdG9wOiAxO1xyXG4gIHdpZHRoOiAxMDUwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNjBweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiA2MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICAjZmZmZmZmO1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuIH1cclxuIC5jYXJyZSB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogOTBweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtMjkwcHg7XHJcbiAgbWFyZ2luLXRvcDogMzg1cHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKiwgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKjpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YWZ0ZXIge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBib2R5IHtcclxuICAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbiAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xyXG4gICB3aWR0aDogNjUwcHg7XHJcbiAgIG1hcmdpbi1sZWZ0OiAxMDBweDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBtYWluIHtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaDpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlIHtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmYgO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIGhlaWdodDogNXB4O1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcclxuICAgIHRyYW5zZm9ybS1vcmlnaW46IDAlIDEwMCU7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcclxuICAgIHdpZHRoOiAxNXB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2hfX2lucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHBhZGRpbmc6IDAgMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC42cztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGNvbG9yOiByZ2IoMjIzLCAyMjMsIDIyMyk7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTpiZWZvcmUge1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlOmFmdGVyIHtcclxuICAgIHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZSAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAwcztcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmFmdGVyIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKSB0cmFuc2xhdGVYKDE1cHgpIHRyYW5zbGF0ZVkoLTJweCk7XHJcbiAgICB3aWR0aDogMDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHdpZHRoOiA1MDBweDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB3aWR0aDogNTAwcHg7XHJcbiAgIH1cclxuXHJcbiAgIFxyXG5cclxuaW5wdXRbdHlwZT1cInRleHRcIl0ge1xyXG4gIGhlaWdodDogNTBweDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIFxyXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDNweDtcclxuICBwYWRkaW5nLXJpZ2h0OiA2MHB4O1xyXG4gIHdpZHRoOiAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICByaWdodDogMDtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIHotaW5kZXg6IDM7XHJcbiAgdHJhbnNpdGlvbjogd2lkdGggMC40cyBjdWJpYy1iZXppZXIoMCwgMC43OTUsIDAsIDEpO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXM6aG92ZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXMge1xyXG4gIHdpZHRoOiA3MDBweDtcclxuICB6LWluZGV4OiAxO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICBjdXJzb3I6IHRleHQ7XHJcbiAgfVxyXG4gIGlucHV0W3R5cGU9XCJzdWJtaXRcIl0ge1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogNTBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBiYWNrZ3JvdW5kOiB1cmwoZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFEQUFBQUF3Q0FNQUFBQmczQW0xQUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUFETlFURlJGVTFOVDlmWDFsSlNVWGw1ZTFkWFZmbjUrYzNOejZ1cnF2NysvdExTMGlZbUpxYW1wbjUrZnlzckszOS9mYVdscC8vLy9WaTRaeXdBQUFCRjBVazVULy8vLy8vLy8vLy8vLy8vLy8vLy8vd0FsclpsaUFBQUJMa2xFUVZSNDJyU1dXUmJESUFoRkhlT1V0TjMvYWdzMXphQTRjSHJLWjhKRlJId29Ya3dUdndHUDFRbzBiWU9iQVB3aUxtYk5BSEJXRkJabEQ5ajBKeGZsRFZpSU9iTkhHL0RvOFBSSFRKazBUZXpBaHY3cWxvSzBKSkVCaCtGOCtVL2hvcElFTE9XZmlaVUNET1pEMVJBRE9RS0E3NW9xNGN2VmtjVCtPZEhucXFwUUNJVFdBam5XVmdHUVVXejEybEp1R3dHb2FXZ0JLelJWQmNDeXBnVWtPQW9XZ0JYL0wwQ214TjQwdTZ4d2NJSjFjT3pXWURmZnAzYXhzUU95dmRrWGlIOUZLUkZ3UFJIWVpVYVhNZ1BMZWlXN1FoYkRSY2l5TFhKYUtoZUN1TGJpVm9xeDFEVlJ5SDI2eWIwaHN1b09GRVBzb3orQlZFME1SbFpOakdaY1JReUhZa21NcDJoQlRJemRrekNUYy9wTHFPbkJyazcveVpkQU9xL3E1TlBCSDFmN3g3ZkdQNEMzQUFNQVFyaHpYOXpoY0dzQUFBQUFTVVZPUks1Q1lJST0pXHJcbiAgICBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcclxuICB0ZXh0LWluZGVudDogLTEwMDAwcHg7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgei1pbmRleDogMjtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgb3BhY2l0eTogMC40O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuNHMgZWFzZTtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInN1Ym1pdFwiXTpob3ZlciB7XHJcbiAgb3BhY2l0eTogMC44O1xyXG4gIH1cclxuICBcclxuXHJcblxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIFxyXG4uaXRlbV9udW0ge1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2l0ZW0ge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLml0ZW1fZGVzYyB7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2xpc3Qge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcbi5zdWNjZXNzX3R4dCB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4uc3VjY2Vzc19kZXNjIHtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLmNvbnRhaW5lcl9ib3gge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG59XHJcblxyXG5cclxuLmt7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luOiBpbmhlcml0O1xyXG4gIH1cclxuICBcclxuICAgXHJcbi5jaGlmZnJle1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBpbmhlcml0O1xyXG4gIH1cclxuXHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMSAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiBcclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfdHh0IHtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfZGVzYyB7XHJcbiAgZm9udC1zaXplOiAyOHB4O1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfbGlzdCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0ge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0gLml0ZW1fbnVtIHtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogIzExMWQ1ZTtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3QgLnN1Y2Nlc3NfaXRlbSAuaXRlbV9kZXNjIHtcclxuICBtYXJnaW46IDA7XHJcbiAgY29sb3I6ICMxMTFkNWU7XHJcbiB9XHJcbiBcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTExZDVlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ib2R5IHtcclxuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMnB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4xIHtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4yIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDhweCAzMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4zIHtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWdyb3VwIC5jYXJkIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBtaW4td2lkdGg6IDA7XHJcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxMTFkNWU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTIuMjVyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAgLmNhcmQgLmNhcmQtaW1nLXRvcCB7XHJcbiAgd2lkdGg6IDUwJTtcclxuICBtYXJnaW4tbGVmdDogOTFweDtcclxuIH1cclxuXHJcbiAuYmxvY2sye1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmc6IDE5cHg7XHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXJ7XHJcbiAgIGJvcmRlcjogNXB4IHNvbGlkIHdoaXRlO1xyXG4gIH1cclxuIFxyXG4gIC5ibG9jazIgLnRlc3QtYnRuMSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICB9XHJcbiAgLmJsb2NrMiAudGVzdC1idG4yIHtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIH1cclxuICAuYmxvY2syIC50ZXN0LWJ0bjMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICB9XHJcbiAgLmJsb2NrMjpob3ZlciAudGVzdC1idG4xIHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMiB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ob21le1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgfVxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazMgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbi5ibG9jazMgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAyNXJlbTtcclxuICB3aWR0aDogODAwcHg7XHJcbn1cclxuLnRleHQtYmxvYyAudGV4dF9ib2R5e1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcbi50ZXh0LWJsb2MgLnRleHRfYm9keSAudGV4dF9jb250ZW50e1xyXG4gIGZvbnQtc2l6ZTogNzBweDtcclxuICBjb2xvcjojMTExZDVlXHJcbn1cclxuLnRleHQtYmxvYyAudGV4dF9ib2R5ICBoMXtcclxuICBtYXJnaW4tdG9wOiAtNzVweDtcclxuICBtYXJnaW4tbGVmdDogLTE3cHg7XHJcbiAgY29sb3I6IzExMWQ1ZVxyXG59XHJcblxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbi5ibG9jazR7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246NjU4cHggMjM0cHg7XHJcbiAgbWFyZ2luLXRvcDoxMDBweDtcclxufVxyXG4uYmxvY2s0IGgxe1xyXG4gIGNvbG9yOiMxMTFkNWU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5ibG9jazQgLmJsb2NrMSBwe3BhZGRpbmctcmlnaHQ6IDQ4cmVtO31cclxuXHJcblxyXG4ub3V0ZXItZGl2LFxyXG4uaW5uZXItZGl2IHtcclxuICBoZWlnaHQ6IDM3OHB4O1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4ub3V0ZXItZGl2IHtcclxuICBwZXJzcGVjdGl2ZTogOTAwcHg7XHJcbiAgcGVyc3BlY3RpdmUtb3JpZ2luOiA1MCUgY2FsYyg1MCUgLSAxOGVtKTtcclxufVxyXG4gLm9uZXtcclxubWFyZ2luOiAxMXB4IDBweCAwcHggNTU2cHhcclxufVxyXG4gLnR3b3tcclxubWFyZ2luOiAtNTgwcHggMCAwIDEwNjVweFxyXG59XHJcbi50aHJlZXtcclxubWFyZ2luOiAtMTJweCAwIDAgMTA1OXB4XHJcbn1cclxuXHJcbi5pbm5lci1kaXYge1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gXHJcbn1cclxuXHJcblxyXG5cclxuLmZyb250IHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiA4NSU7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIGJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xKSBpbnNldDtcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxufVxyXG5cclxuXHJcbi5mcm9udF9fZmFjZS1waG90bzEge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG5cclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8yIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8zIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgLyogYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgICAgdHJhbnNpdGlvbjogYWxsIDAuNnMgY3ViaWMtYmV6aWVyKDAuOCwgLTAuNCwgMC4yLCAxLjcpO1xyXG4gICAgICAgei1pbmRleDogMzsqL1xyXG59XHJcblxyXG4uZnJvbnRfX3RleHQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDM1cHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1oZWFkZXIge1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiT3N3YWxkXCI7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1wYXJhIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtNXB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC40cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCIsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5mcm9udC1pY29ucyB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1ob3ZlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMTBweDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgY29sb3I6IHJlZDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcblxyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBsZXR0ZXItc3BhY2luZzogLjRweDtcclxuXHJcbiAgYm9yZGVyOiAycHggc29saWQgcmVkO1xyXG4gIHBhZGRpbmc6IDhweCAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcblxyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazUgaW1ne1xyXG4gIG1hcmdpbi1sZWZ0OiAxOTBweDtcclxuICBoZWlnaHQ6ODAlO1xyXG4gIHdpZHRoOjgwJVxyXG59XHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiBcclxuLmJsb2NrNntcclxuICBwYWRkaW5nLXRvcDogMzBweDsgXHJcbiAgcGFkZGluZy1sZWZ0OiAxMTBweDtcclxuICBtYXJnaW4tdG9wOiAtNTAxcHg7IFxyXG4gIGZsb2F0OnJpZ2h0XHJcbn1cclxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXJ7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgYm9yZGVyOiAycHggc29saWQgI0M1QzVDNTtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7IFxyXG4gICAgd2lkdGg6IDkwJTsgXHJcbiAgcGFkZGluZzogMjBweDtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlciBoM3tcclxuICBmb250LXNpemU6IDI2cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcjpob3ZlcntcclxuYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcjpob3ZlciBoM3tcclxuICBjb2xvcjogYmx1ZTsgIFxyXG59XHJcblxyXG5cclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5sYXN0QntcclxuICB3aWR0aDogMTAwJTtcclxuIFxyXG59XHJcbi5ibG9jazd7XHJcbiAgcGFkZGluZy10b3A6IDEwMHB4XHJcbn1cclxuXHJcbi5jYXJkMXtcclxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7aGVpZ2h0OiAzNDVweDt3aWR0aDogMzE1cHg7XHJcbn1cclxuLmNhcmQxIC5ib3gxe1xyXG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNDBweDtcclxufVxyXG4uY2FyZDJ7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xyXG4gIGhlaWdodDogMzQ1cHg7XHJcbiAgd2lkdGg6IDMxNXB4O1xyXG59XHJcbi5jYXJkMiAuYm94MntcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDQwcHg7XHJcbn1cclxuLmNhcmQze1xyXG4gIG1hcmdpbi1yaWdodDogNTBweDtcclxuICBoZWlnaHQ6IDM0NXB4O1xyXG4gIHdpZHRoOiAzMTVweDtcclxufVxyXG4uY2FyZDMgLmJveDN7XHJcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA0MHB4O1xyXG59XHJcbi5jYXJkMSAuYm94MSBoNHtcclxuICBwYWRkaW5nOiA2OXB4IDAgMCAyNnB4O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQxIC5ib3gxIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQxIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtMTMycHg7XHJcbiAgbGVmdDogLTExNnB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDEgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG5cclxuLmNhcmQyIC5ib3gyIGg0e1xyXG4gIHBhZGRpbmc6IDU0cHggMCAwIDI2cHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQyIC5ib3gyIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQyIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIHRvcDogLTEzMnB4O1xyXG4gIGxlZnQ6IC0xMTZweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDIgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG5cclxuLmNhcmQzIC5ib3gzIGg0e1xyXG4gIHBhZGRpbmc6IDQ0cHggMCAwIDI2cHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQzIC5ib3gzIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQzIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtMTMycHg7XHJcbiAgbGVmdDogLTExNnB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDMgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5cclxuaDF7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5jb2xvcjogIzExMWQ1ZTtcclxubWFyZ2luOiA1MHB4IDAgNTBweCAwO1xyXG59XHJcblxyXG5oNXtcclxuY29sb3I6ICMxMTFkNWU7XHJcbmZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5we1xyXG5mb250LXNpemU6IDEycHg7XHJcbmNvbG9yOiAjMTExZDVlO1xyXG5cclxufVxyXG4uZm9vdGVyIHB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG5jb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuXHJcbi5jb3B5cmlnaHR7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBjMTMzYTtcclxuICBjb2xvcjojZmZmO1xyXG4gIGZvbnQtc2l6ZToxM3B4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuXHJcbiAgLyogQ3VzdG9tLCBpUGhvbmUgUmV0aW5hICAqL1xyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzIwcHgpIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpICB7XHJcbiAgICAgXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgLm5hdmJhci1icmFuZCB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgICAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgICAgIHotaW5kZXg6IDU7XHJcbiAgfVxyXG4gIFxyXG4gIC5kcm9we1xyXG4gICAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4ubmF2YmFyLW5hdntcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBoZWlnaHQ6bWF4LWNvbnRlbnQ7XHJcbiAgfVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMyU7XHJcbmhlaWdodDogNSU7XHJcbnotaW5kZXg6NTkwMDtcclxufVxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczo6YWZ0ZXJ7XHJcbiAgY29udGVudDogXCI+XCI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDUlO1xyXG5oZWlnaHQ6IDUlO1xyXG5mb250LXdlaWdodDogYm9sZDtcclxuZm9udC1zaXplOiAyMHB4O1xyXG5cclxufVxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcclxuICBjb250ZW50OiBcIj5cIjtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAxMHB4O1xyXG5kaXNwbGF5OiBub25lO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDMlO1xyXG5oZWlnaHQ6IDUlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCB7XHJcbiAgcGFkZGluZzogMDtcclxuICBtYXJnaW46IDAgMCAtMzBweCAwO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6aG92ZXJ7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAzNSU7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBoZWlnaHQ6IDQwJTtcclxuICB6LWluZGV4OiA5OTk5OTtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9uczpob3ZlciAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4gIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmd7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czowO1xyXG4gIH1cclxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gIH1cclxuXHJcbiAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogYXV0bztcclxuICAgIGhlaWdodDogYXV0bztcclxuIH1cclxuXHJcblxyXG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogdW5zZXQ7XHJcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50cmFuc3Rpb257XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB3aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuLmNhcnJlIHtcclxuICB3aWR0aDogMjAwcHg7XHJcbiAgaGVpZ2h0OiA5MHB4O1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMjUwcHg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uY29udGVudHtcclxuICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcblxyXG5cclxuLmJsb2NrMiAudGVzdC1idG4xIHtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazIgLnRlc3QtYnRuMiB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5jb2xvcjogYmxhY2s7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syIC50ZXN0LWJ0bjMge1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMSB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjIge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuXHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmNoaWZmcmV7XHJcbiAgZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4ua3tcclxuICBtYXJnaW46IGluaXRpYWw7XHJcbn1cclxuLnN1Y2Nlc3NfaXRlbXtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazMgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gIHdpZHRoOiAyNTBweDtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLmJsb2NrNHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcclxuICBtYXJnaW4tdG9wOjEwMHB4O1xyXG4gIH1cclxuICAuYmxvY2s0IC5ibG9jazF7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gIH1cclxuICAuYmxvY2s0IC5ibG9jazEgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gIH1cclxuICBcclxuICAgLm9uZXtcclxuICBtYXJnaW46IDQwcHggMCAwIDAgO1xyXG4gIH1cclxuICAgLnR3b3tcclxuICBtYXJnaW46IDA7XHJcbiAgfVxyXG4gICAudGhyZWV7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICAub3V0ZXItZGl2e1xyXG4gICAgZGlzcGxheTogY29udGVudHM7XHJcbiAgfVxyXG4gIFxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5ibG9jazV7XHJcbiAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuYmxvY2s1IGltZ3tcclxuICBtYXJnaW4tbGVmdDogMDtcclxuICBoZWlnaHQ6MTAwJTtcclxuICB3aWR0aDoxMDAlXHJcbiAgfVxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5ibG9jazZ7XHJcbiAgcGFkZGluZy10b3A6IDMwcHg7IFxyXG4gIHBhZGRpbmctbGVmdDowO1xyXG4gIG1hcmdpbi10b3A6IDA7IFxyXG4gIG1heC13aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBjb250ZW50cztcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5ibG9jazYgLnRoaXJkLWJsb2MtYm9yZGVye1xyXG4gICAgbWFyZ2luOiAyMHB4IGF1dG8gMCBhdXRvO1xyXG4gIH1cclxuICBcclxuICBcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazd7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbn1cclxuXHJcbiAgICAuY2FyZDF7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgICB3aWR0aDogMzE1cHg7XHJcbiAgICAgIH1cclxuICAgICAgLmNhcmQxIC5ib3gxe1xyXG4gICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkMntcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgICAgIHdpZHRoOiAzMTVweDtcclxuICAgICAgfVxyXG4gICAgICAuY2FyZDIgLmJveDJ7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgLmNhcmQze1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMzQ1cHg7XHJcbiAgICAgICAgd2lkdGg6IDMxNXB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkMyAuYm94M3tcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIHBhZGRpbmc6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAubGFzdEJ7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIG1hcmdpbjogNTBweDtcclxuICAgICAgfVxyXG5cclxuXHJcbiAgICAgIC50ZXh0LWNlbnRlcntcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDQ4MHB4KSBhbmQgKG1heC13aWR0aCA6IDc2OHB4KSAge1xyXG4gICAgICAgICAgICBcclxuICAgIFxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5hdiBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAubmF2YmFyLWJyYW5kIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcclxuICAgIG1hcmdpbi1yaWdodDogMXJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgei1pbmRleDogNTtcclxufVxyXG4ubmF2X2ltZyB7XHJcbiAgd2lkdGg6IDgwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW4tbGVmdDogMzhweDtcclxufVxyXG5cclxuLmRyb3B7XHJcbiAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICBtYXJnaW4tbGVmdDogMHB4O1xyXG59XHJcbi5uYXZiYXItbmF2e1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGhlaWdodDptYXgtY29udGVudDtcclxuICB9XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAxMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDMlO1xyXG5oZWlnaHQ6IDUlO1xyXG56LWluZGV4OjU5MDA7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcclxuY29udGVudDogXCI+XCI7XHJcbmNvbG9yOiB3aGl0ZTtcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogNSU7XHJcbmhlaWdodDogNSU7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5mb250LXNpemU6IDIwcHg7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xyXG5jb250ZW50OiBcIj5cIjtcclxuY29sb3I6IHdoaXRlO1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IG5vbmU7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMyU7XHJcbmhlaWdodDogNSU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogbm9uZTtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMTglO1xyXG5oZWlnaHQ6IDQwJTtcclxuei1pbmRleDogOTk5OTk7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XHJcbnBhZGRpbmc6IDA7XHJcbm1hcmdpbjogMCAwIC0zMHB4IDA7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmd7XHJcbmJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOjA7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xyXG5ib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xyXG5ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG5oZWlnaHQ6IDQwcHg7XHJcbndpZHRoOiAzMDBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcclxub3BhY2l0eTogMTtcclxud2lkdGg6IDMwMHB4O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIHtcclxuICB3aWR0aDogYXV0bztcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU4NXB4O1xyXG4gIG1hcmdpbi10b3A6IDA7XHJcbiAgd2lkdGg6IDc1MHB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIHtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IHJvdztcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbm1hcmdpbi1sZWZ0OiB1bnNldDtcclxud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnRyYW5zdGlvbntcclxuZGlzcGxheTogZmxleDtcclxud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxubWFyZ2luOiAwO1xyXG59XHJcbi5jYXJyZSB7XHJcbndpZHRoOiAyMDBweDtcclxuaGVpZ2h0OiA5MHB4O1xyXG5iYWNrZ3JvdW5kOiB3aGl0ZTtcclxuYm9yZGVyLXJhZGl1czogMThweDtcclxubWFyZ2luLWxlZnQ6IGF1dG87XHJcbm1hcmdpbi10b3A6IDI1MHB4O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi5jb250ZW50e1xyXG53aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxuZm9udC13ZWlnaHQ6IDgwMDtcclxuY29sb3I6ICNmZmZmZmY7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5cclxuXHJcbi5ibG9jazIgLnRlc3QtYnRuMSB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbm1hcmdpbi10b3A6IDMwcHg7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syIC50ZXN0LWJ0bjIge1xyXG5ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuY29sb3I6IGJsYWNrO1xyXG5ib3JkZXI6IDA7XHJcbn1cclxuLmJsb2NrMiAudGVzdC1idG4zIHtcclxuYm9yZGVyLXJhZGl1czogMjVweDtcclxuYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMSB7XHJcbmJvcmRlcjogbm9uZTtcclxubWFyZ2luLXRvcDogMzBweDtcclxuY29sb3I6IHdoaXRlO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4yIHtcclxuY29sb3I6IHdoaXRlO1xyXG5ib3JkZXI6IG5vbmU7XHJcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjMge1xyXG5ib3JkZXI6IG5vbmU7XHJcbmNvbG9yOiB3aGl0ZTtcclxuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbm90cmUgc3VjY2VzKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5jaGlmZnJle1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxud2lkdGg6IDEwMCU7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5re1xyXG5tYXJnaW46IGluaXRpYWw7XHJcbn1cclxuLnN1Y2Nlc3NfaXRlbXtcclxubWFyZ2luLWxlZnQ6IDIwcHg7XHJcbmxpc3Qtc3R5bGU6IG5vbmU7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgMyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2szIHB7XHJcbnBhZGRpbmctcmlnaHQ6IDA7XHJcbndpZHRoOiAyNTBweDtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgNCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4uYmxvY2s0e1xyXG5iYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xyXG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5iYWNrZ3JvdW5kLXBvc2l0aW9uOjBweCA1NzZweDtcclxubWFyZ2luLXRvcDoxMDBweDtcclxufVxyXG4uYmxvY2s0IC5ibG9jazF7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG59XHJcbi5ibG9jazQgLmJsb2NrMSBwe1xyXG5wYWRkaW5nLXJpZ2h0OiAwO1xyXG59XHJcblxyXG4ub25le1xyXG5tYXJnaW46IDQwcHggMCAwIDAgO1xyXG59XHJcbi50d297XHJcbm1hcmdpbjogMDtcclxufVxyXG4udGhyZWV7XHJcbm1hcmdpbjogMDtcclxufVxyXG4ub3V0ZXItZGl2e1xyXG5kaXNwbGF5OiBjb250ZW50cztcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ibG9jazV7XHJcbm1heC13aWR0aDogMTAwJTtcclxud2lkdGg6IDEwMCU7XHJcbn1cclxuLmJsb2NrNSBpbWd7XHJcbm1hcmdpbi1sZWZ0OiAwO1xyXG5oZWlnaHQ6MTAwJTtcclxud2lkdGg6MTAwJVxyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2s2e1xyXG5wYWRkaW5nLXRvcDogMzBweDsgXHJcbnBhZGRpbmctbGVmdDowO1xyXG5tYXJnaW4tdG9wOiAwOyBcclxubWF4LXdpZHRoOiAxMDAlO1xyXG5kaXNwbGF5OiBjb250ZW50cztcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5ibG9jazYgLnRoaXJkLWJsb2MtYm9yZGVye1xyXG5tYXJnaW46IDIwcHggYXV0byAwIGF1dG87XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ibG9jazd7XHJcbnBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZDF7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgaGVpZ2h0OiAzNDVweDtcclxuICB3aWR0aDogMzE1cHg7XHJcbiAgfVxyXG4gIC5jYXJkMSAuYm94MXtcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDQwcHg7XHJcbiAgfVxyXG4gIC5jYXJkMntcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgaGVpZ2h0OiAzNDVweDtcclxuICAgIHdpZHRoOiAzMTVweDtcclxuICB9XHJcbiAgLmNhcmQyIC5ib3gye1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDQwcHg7XHJcbiAgICB9XHJcbiAgLmNhcmQze1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgd2lkdGg6IDMxNXB4O1xyXG4gIH1cclxuICAuY2FyZDMgLmJveDN7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNDBweDtcclxuICAgIH1cclxuICAgIFxyXG4gIC5sYXN0QntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luOiBub25lO1xyXG4gIH1cclxuXHJcblxyXG4gIC50ZXh0LWNlbnRlcntcclxuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG4gIH1cclxuXHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAvKiBTbWFsbCBEZXZpY2VzLCBUYWJsZXRzKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDk5MnB4KSAge1xyXG4gICAgXHJcbiAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipuYXYgYmFyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLm5hdmJhci1icmFuZCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBtYXJnaW4tbGVmdDogMDtcclxuICAgIHotaW5kZXg6IDU7XHJcbn1cclxuLm5hdl9pbWcge1xyXG4gIHdpZHRoOiA4MHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDM4cHg7XHJcbn1cclxuLm5hdmJhci1uYXZ7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgaGVpZ2h0Om1heC1jb250ZW50O1xyXG5cclxuICB9XHJcbi5kcm9we1xyXG4gIG1pbi13aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgbWFyZ2luLWxlZnQ6IDBweDtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAxMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDMlO1xyXG5oZWlnaHQ6IDUlO1xyXG56LWluZGV4OjU5MDA7XHJcbn1cclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6OmFmdGVye1xyXG5jb250ZW50OiBcIj5cIjtcclxuY29sb3I6IHdoaXRlO1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiA1JTtcclxuaGVpZ2h0OiA1JTtcclxuZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbmZvbnQtc2l6ZTogMjBweDtcclxuXHJcbn1cclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6aG92ZXI6YWZ0ZXJ7XHJcbmNvbnRlbnQ6IFwiPlwiO1xyXG5jb2xvcjogd2hpdGU7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogbm9uZTtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiAzJTtcclxuaGVpZ2h0OiA1JTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQge1xyXG5wYWRkaW5nOiAwO1xyXG5tYXJnaW46IDAgMCAtMzBweCAwO1xyXG5kaXNwbGF5OiBub25lO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6aG92ZXJ7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiAxMiU7XHJcbmhlaWdodDogNDAlO1xyXG56LWluZGV4OiA5OTk5OTtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9uczpob3ZlciAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovICBcclxuLmJsb2NrMHtcclxuICBmbGV4OiAwIDAgYXV0bztcclxud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmd7XHJcbmJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOjA7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xyXG5ib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xyXG5ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG5oZWlnaHQ6IDQwcHg7XHJcbndpZHRoOiAzMDBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcclxub3BhY2l0eTogMTtcclxud2lkdGg6IDMwMHB4O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIHtcclxuICB3aWR0aDogYXV0bztcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU4NXB4O1xyXG4gIG1hcmdpbi10b3A6IDA7XHJcbiAgd2lkdGg6IDc1MHB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIHtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IHJvdztcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbm1hcmdpbi1sZWZ0OiB1bnNldDtcclxud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnRyYW5zdGlvbntcclxuZGlzcGxheTogZmxleDtcclxud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxubWFyZ2luOiAwO1xyXG59XHJcbi5jYXJyZSB7XHJcbndpZHRoOiAyMDBweDtcclxuaGVpZ2h0OiA5MHB4O1xyXG5iYWNrZ3JvdW5kOiB3aGl0ZTtcclxuYm9yZGVyLXJhZGl1czogMThweDtcclxubWFyZ2luLWxlZnQ6IGF1dG87XHJcbm1hcmdpbi10b3A6IDI1MHB4O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi5jb250ZW50e1xyXG53aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxuZm9udC13ZWlnaHQ6IDgwMDtcclxuY29sb3I6ICNmZmZmZmY7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ib2R5IHtcclxuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMnB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIH1cclxuXHJcbi5ibG9jazIgLnRlc3QtYnRuMSB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbm1hcmdpbi10b3A6IDBweDtcclxuYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazIgLnRlc3QtYnRuMiB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5jb2xvcjogYmxhY2s7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syIC50ZXN0LWJ0bjMge1xyXG5ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5ib3JkZXI6IDA7XHJcbn1cclxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4xIHtcclxuYm9yZGVyOiBub25lO1xyXG5tYXJnaW4tdG9wOiAzMHB4O1xyXG5jb2xvcjogd2hpdGU7XHJcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjIge1xyXG5jb2xvcjogd2hpdGU7XHJcbmJvcmRlcjogbm9uZTtcclxuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XHJcbmJvcmRlcjogbm9uZTtcclxuY29sb3I6IHdoaXRlO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuXHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmNoaWZmcmV7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbndpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ua3tcclxubWFyZ2luOiBpbml0aWFsO1xyXG59XHJcbi5zdWNjZXNzX2l0ZW17XHJcbm1hcmdpbi1sZWZ0OiAyMHB4O1xyXG5saXN0LXN0eWxlOiBub25lO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4ub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAudGl0bGV7XHJcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgMyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2szIHB7XHJcbnBhZGRpbmctcmlnaHQ6IDA7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sgNCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4uYmxvY2s0e1xyXG5iYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xyXG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5iYWNrZ3JvdW5kLXBvc2l0aW9uOjY1OHB4IDIzNHB4O1xyXG5tYXJnaW4tdG9wOjBweDtcclxufVxyXG4uYmxvY2s0IC5ibG9jazF7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG59XHJcbi5ibG9jazQgLmJsb2NrMSBwe1xyXG5wYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbn1cclxuXHJcbi5vbmV7XHJcbm1hcmdpbjogNDBweCAwIDAgMCA7XHJcbn1cclxuLnR3b3tcclxubWFyZ2luOiAwO1xyXG59XHJcbi50aHJlZXtcclxubWFyZ2luOiAwO1xyXG59XHJcbi5vdXRlci1kaXZ7XHJcbmRpc3BsYXk6IGNvbnRlbnRzO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJsb2NrNXtcclxubWF4LXdpZHRoOiAxMDAlO1xyXG53aWR0aDogMTAwJTtcclxufVxyXG4uYmxvY2s1IGltZ3tcclxubWFyZ2luLWxlZnQ6IDA7XHJcbmhlaWdodDoxMDAlO1xyXG53aWR0aDoxMDAlXHJcbn1cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ibG9jazZ7XHJcbnBhZGRpbmctdG9wOiAzMHB4OyBcclxucGFkZGluZy1sZWZ0OjA7XHJcbm1hcmdpbi10b3A6IDA7IFxyXG5tYXgtd2lkdGg6IDEwMCU7XHJcbmRpc3BsYXk6IGNvbnRlbnRzO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXJ7XHJcbm1hcmdpbjogMjBweCBhdXRvIDAgYXV0bztcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJsb2NrN3tcclxucGFkZGluZy10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5jYXJkMXtcclxuICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICBoZWlnaHQ6IDM0NXB4O1xyXG4gIHdpZHRoOiAzMTVweDtcclxuICB9XHJcbiAgLmNhcmQxIC5ib3gxe1xyXG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNDBweDtcclxuICB9XHJcbiAgLmNhcmQye1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgd2lkdGg6IDMxNXB4O1xyXG4gIH1cclxuICAuY2FyZDIgLmJveDJ7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNDBweDtcclxuICAgIH1cclxuICAuY2FyZDN7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIGhlaWdodDogMzQ1cHg7XHJcbiAgICB3aWR0aDogMzE1cHg7XHJcbiAgfVxyXG4gIC5jYXJkMyAuYm94M3tcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgLmxhc3RCe1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IG5vbmU7XHJcbiAgfVxyXG5cclxuXHJcbiAgLnRleHQtY2VudGVye1xyXG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgXHJcbiAvKiBNZWRpdW0gRGV2aWNlcywgRGVza3RvcHMgKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk5MnB4KSBhbmQgKG1heC13aWR0aCA6IDEyMDBweCkgIHtcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKipuYXYgYmFyICoqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAuZHJvcHtcclxuICBtaW4td2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIG1hcmdpbi1sZWZ0OiAtNTBweDtcclxuIH0gICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqIHNpZGUgYmFyICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5mbG9hdF9hY3Rpb25zIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgbGVmdDogMDtcclxuICB0b3A6IDM1JTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogOCU7XHJcbiAgei1pbmRleDo1OTAwO1xyXG4gIH1cclxuICBcclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMCAwIC0zMHB4IDA7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIHtcclxuICBwYWRkaW5nOiA1cHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICBcclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDI1cHg7XHJcbiAgfVxyXG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tdG9wOiA3cHg7XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gICAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtczo6YmVmb3Jle1xyXG4gICBjb250ZW50OiBcIj5cIjtcclxuICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICByaWdodDogLTEwcHg7XHJcbiAgIHRvcDogMTUlO1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgd2lkdGg6IDQwJTtcclxuICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgIH1cclxuICAgXHJcbiAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAudGVzdG1lZ2l7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgIHJpZ2h0OiAtMTgwcHg7XHJcbiAgICAgdG9wOiAxNSU7XHJcbiAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgICB3aWR0aDogNDAlO1xyXG4gICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgIHdpZHRoOiAxODBweDtcclxuICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICB9XHJcbiAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6aG92ZXIgLnRlc3RtZWdpe1xyXG4gICBkaXNwbGF5OiBmbGV4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI1cHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiA3cHg7XHJcbiAgIH1cclxuICAgXHJcbiAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5jb250ZW50e1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuICB9XHJcbiAgLnRyYW5zdGlvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTE0MHB4O1xyXG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XHJcbiAgYmFja2dyb3VuZDojMTExZDVlO1xyXG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbWF4LWhlaWdodDogNTg1cHg7XHJcbiAgbWFyZ2luLXRvcDogMTtcclxuICB3aWR0aDogOTkzcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICAjZmZmZmZmO1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuIH1cclxuIC5jYXJyZSB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogOTBweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA4MHB4O1xyXG4gIG1hcmdpbi10b3A6IDMzNXB4O1xyXG4gIH1cclxuICBcclxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIG1hcmdpbi1sZWZ0OiB1bnNldDtcclxuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogNDAwcHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gICB9XHJcblxyXG5cclxuICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgLmhvbWV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmJsb2NrMntcclxuICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICBwYWRkaW5nOiAyMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5ibG9jazI6aG92ZXJ7XHJcbiAgICAgICBib3JkZXI6IDVweCBzb2xpZCB3aGl0ZTtcclxuICAgICAgIHBhZGRpbmc6IDE4cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiBcclxuICAgICAgIC5ibG9jazR7XHJcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246MjU4cHggMjIwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDowcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5ibG9jazQgLmJsb2NrMXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5ibG9jazQgLmJsb2NrMSBwe1xyXG4gICAgICAgICAgcGFkZGluZy1yaWdodDogNTAlO1xyXG4gICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgLm9uZXtcclxuICAgICAgICAgIG1hcmdpbjogMTFweCAwcHggMHB4IDI2M3B4XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAgLnR3b3tcclxuICAgICAgICAgIG1hcmdpbjogLTU4MHB4IDAgMCA2NTBweFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLnRocmVle1xyXG4gICAgICAgICAgbWFyZ2luOiAtMTJweCAwIDAgNjUwcHhcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNSAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAgICAuYmxvY2s1IGltZ3tcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDYwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDoxMDAlO1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlXHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazYgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbiAgICAgICAgICAuYmxvY2s2e1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMzBweDsgXHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTEwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00OTBweDsgXHJcbiAgICAgICAgICAgIGZsb2F0OnJpZ2h0XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAgLypMYXJnZSBEZXZpY2VzLCBXaWRlIFNjcmVlbnMqL1xyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMTIwMHB4KSBhbmQgKG1heC13aWR0aCA6IDE1MDBweCkge1xyXG4gICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uY29udGVudHtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiAgfVxyXG4gIC50cmFuc3Rpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICBwYWRkaW5nLWxlZnQ6IDExNXB4O1xyXG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XHJcbiAgYmFja2dyb3VuZDojMTExZDVlO1xyXG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbWF4LWhlaWdodDogNTg1cHg7XHJcbiAgbWFyZ2luLXRvcDogMTtcclxuICB3aWR0aDogOTkzcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICAjZmZmZmZmO1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuIH1cclxuIC5jYXJyZSB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogOTBweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA4MHB4O1xyXG4gIG1hcmdpbi10b3A6IDMzNXB4O1xyXG4gIH1cclxuICBcclxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIG1hcmdpbi1sZWZ0OiB1bnNldDtcclxuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogNDAwcHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gICB9XHJcblxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbiAgICAgICAgLmJsb2NrNCAuYmxvY2sxe1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB9XHJcbiAgICAgXHJcblxyXG4gICAgICAgIC5vbmV7XHJcbiAgICAgICAgICBtYXJnaW46IDExcHggMHB4IDBweCA0NTZweFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgIC50d297XHJcbiAgICAgICAgICBtYXJnaW46IC01ODBweCAwIDAgNzkycHhcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC50aHJlZXtcclxuICAgICAgICAgIG1hcmdpbjogLTEycHggMCAwIDc5MnB4XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgIH0gIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "./src/app/map-french/map-french.component.ts":
/*!****************************************************!*\
  !*** ./src/app/map-french/map-french.component.ts ***!
  \****************************************************/
/*! exports provided: MapFrenchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapFrenchComponent", function() { return MapFrenchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");




const _c0 = function (a0) { return { choix: a0 }; };
class MapFrenchComponent {
    constructor() {
        this.cp = 0;
        this.message = " Info about the action";
    }
    ngOnInit() {
        this.map = document.getElementById('svgContent');
        this.paths = this.map.getElementsByTagName('path');
        for (var i = 0; i < this.paths.length; i++) {
            this.paths[i].addEventListener("click", function (e) {
                this.cp = e.target.getAttribute('data-department');
                this.dept = this.cp.toString();
                console.log('cp', this.cp);
                console.log('cp', this.dept);
            });
        }
    }
    region() {
        let map = document.getElementById('map');
        let paths = map.getElementsByTagName('path');
    }
}
MapFrenchComponent.ɵfac = function MapFrenchComponent_Factory(t) { return new (t || MapFrenchComponent)(); };
MapFrenchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MapFrenchComponent, selectors: [["app-map-french"]], decls: 128, vars: 303, consts: [[1, "mapfrench_container"], [1, "mapfrench_wrapper"], ["id", "svgContent", "version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 700 590", 0, "xml", "space", "preserve", 1, "map_content"], ["data-name", "Guadeloupe", "data-department", "971", "data-code_insee", "01", 1, "region"], ["data-name", "Guadeloupe", "data-department", "971", "d", "M35.87,487.13l0.7,7.2l-4.5-1.1l-2,1.7l-5.8-0.6l-1.7-1.2l4.9,0.5l3.2-4.4L35.87,487.13z M104.87,553.63 l-4.4-1.8l-1.9,0.8l0.2,2.1l-1.9,0.3l-2.2,4.9l0.7,2.4l1.7,2.9l3.4,1.2l3.4-0.5l5.3-5l-0.4-2.5L104.87,553.63z M110.27,525.53 l-6.7-2.2l-2.4-4.2l-11.1-2.5l-2.7-5.7l-0.7-7.7l-6.2-4.7l-5.9,5.5l-0.8,2.9l1.2,4.5l3.1,1.2l-1,3.4l-2.6,1.2l-2.5,5.1l-1.9-0.2 l-1,1.9l-4.3-0.7l1.8-0.7l-3.5-3.7l-10.4-4.1l-3.4,1.6l-2.4,4.8l-0.5,3.5l3.1,9.7l0.6,12l6.3,9l0.6,2.7c3-1.2,6-2.5,9.1-3.7l5.9-6.9 l-0.4-8.7l-2.8-5.3l0.2-5.5l3.6,0.2l0.9-1.7l1.4,3.1l6.8,2l13.8-4.9L110.27,525.53z", 1, "departement", 3, "ngClass"], ["data-name", "Martinique", "data-department", "972", "data-code_insee", "02", 1, "region"], ["data-name", "Martinique", "data-department", "972", "d", "m44.23,433.5l1.4-4.1l-6.2-7.5l0.3-5.8l4.8-4 l4.9-0.9l17,9.9l7,8.8l9.4-5.2l1.8,2.2l-2.8,0.8l0.7,2.6l-2.9,1l-2.2-2.4l-1.9,1.7l0.6,2.5l5.1,1.6l-5.3,4.9l1.6,2.3l4.5-1.5 l-0.8,5.6l3.7,0.2l7.6,19l-1.8,5.5l-4.1,5.1h-2.6l-2-3l3.7-5.7l-4.3,1.7l-2.5-2.5l-2.4,1.2l-6-2.8l-5.5,0.1l-5.4,3.5l-2.4-2.1 l0.2-2.7l-2-2l2.5-4.9l3.4-2.5l4.9,3.4l3.2-1.9l-4.4-4.7l0.2-2.4l-1.8,1.2l-7.2-1.1l-7.6-7L44.23,433.5z", 1, "departement", 3, "ngClass"], ["data-name", "Guyane", "data-department", "973", "data-code_insee", "03", 1, "region"], ["data-name", "Guyane", "data-department", "973", "d", "m95.2,348.97l-11.7,16.4l0.3,2.4l-7.3,14.9 l-4.4,3.9l-2.6,1.3l-2.3-1.7l-4.4,0.8l0.7-1.8l-10.6-0.3l-4.3,0.8l-4.1,4.1l-9.1-4.4l6.6-11.8l0.3-6l4.2-10.8l-8.3-9.6l-2.7-8 l-0.6-11.4l3.8-7.5l5.9-5.4l1-4l4.2,0.5l-2.3-2l24.7,8.6l9.2,8.8l3.1,0.3l-0.7,1.2l6.1,4l1.4,4.1l-2.4,3.1l2.6-1.6l0.1-5.5l4,3.5 l2.4,7L95.2,348.97z", 1, "departement", 3, "ngClass"], ["data-name", "La R\u00E9union", "data-department", "974", "data-code_insee", "04", 1, "region"], ["data-name", "La R\u00E9union", "data-department", "974", "d", "m41.33,265.3l-6.7-8.5l1.3-6l4.1-2.4l0.7-7.9 l3.3,0.4l7.6-6.1l5.7-0.8l21,4l5,5.3v4.1l7.3,10.1l6.7,4.5l1,3.6l-3.3,7.9l0.9,9.6l-3.4,3.5l-17.3,2.9l-19.6-6.5l-3.8-3.6l-4.7-1.2 l-0.9-2.5l-3.6-2.3L41.33,265.3z", 1, "departement", 3, "ngClass"], ["data-name", "Mayotte", "data-department", "976", "data-code_insee", "06", 1, "region"], ["data-name", "Mayotte", "data-department", "976", "d", "m57.79,157.13l11.32,5.82l-3.24,7.46l-5.66,7.52l5.66,8.37l-4.04,5.7l-5.66,8.01l5.66,4.37l-7.28,4.37l-8.09-2.73l-4.04-5.04v-4.85l-3.24-6.55l7.28,3.88l4.04,1.13v-7.14l-4.85-8.43v-14.8l-8.09-2.61l-3.24-2.67v-5.76l8.9-6.79l7.28,10.19L57.79,157.13z M78.07,164.38l-5.56,3.42l4.81,5.59l3.93-4.79L78.07,164.38z", 1, "departement", 3, "ngClass"], ["data-name", "Ile-de-France", "data-code_insee", "11", 1, "region"], ["data-name", "Paris", "data-department", "75", "d", "M641.8,78.3l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.4-0.1l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2 l0.4-1.9l1.3-3.1l2.7-2.1l2.9-1.1l3.9,0.5h0.1l0.9-2.2l7.1-4.6l14-0.1l1.8,3.6l1.8,2.4l0.6,0.9l0.1,0.4L631,68l0.4,5.4l0.4,1.8v0.1 l-0.3,0.8l0.1,3.6l0.6-0.5l1.6-1.6l2-0.5l2-0.5L641.8,78.3z M396.8,154.7l-3.2-0.5l-2.5,1.7l3,3.5l5.3-0.1l-1.8-1.9L396.8,154.7z", 1, "departement", 3, "ngClass"], ["data-name", "Seine-et-Marne", "data-department", "77", "d", "m441.1,176.1l-2.9,0.8l0.4,8.5l-15.4,3 l-0.2,5.8l-3.9,5.4l-11.2,2.7l-9.2-0.7l2.6-1.5l0.6-2.7l-4.2-4.3L397,190l3.4-4.8l4-17.2l-0.5-1l1.1-4.1l-0.3-2.9v-0.1l-1.3-4.7 l1.3-2.5l-1.7-5.1l0.1-0.1l1.7-2.3l-0.2-2l6.9,1l2-2.2l2.5,1.6l8.1-2.9l2.6,0.7l1.8,2.5l-0.7,2.8l3.9,4.2l9.3,6l-0.4,2l-2.6,2.2 l3.5,8.3l2.6,1.7L441.1,176.1z", 1, "departement", 3, "ngClass"], ["data-name", "Yvelines", "data-department", "78", "d", "m364.1,158.1l-3.6-6.6l-1.8-5.8l2.3-2.6 l3.8,0.1l9.5,0.8l9,3.6l5.5,6.1l-2,3.1l3.2,5.2l-7.1,5.4l-1.6,2.6l0.7,2.9l-4.6,8.6l-3.1,0.7L372,180l-1.2-5.6l-6.2-5.4L364.1,158.1z", 1, "departement", 3, "ngClass"], ["data-name", "Essonne", "data-department", "91", "d", "m401.6,164.8l2.3,2.2l0.5,1l-4,17.2L397,190 l-3.7-0.6l-2.8,1.8l-1.5-2.7l-1.9,2.9l-6.9,0.7l-2.8-10.6l4.6-8.6l-0.7-2.9l1.6-2.6l7.1-5.4v-0.1l3.7,1.6l5.1,2.1L401.6,164.8z", 1, "departement", 3, "ngClass"], ["data-name", "Hauts-de-Seine", "data-department", "92", "d", "M391.1,155.9l3,3.5l-0.4,4.1l-3.7-1.6v0.1l-3.2-5.2l2-3.1l3.6-2.6l1.3,2l-0.1,1.1L391.1,155.9z M612.6,54.1 l1.6-0.7l0.7-1.9l0.5-1.8l-0.1-1.1l-0.2-1.4l-4.6-1.9l-4.6-0.9l-4,1.3l-7.6,5.6l-6.1,5.8l-5.3,3l-1,1l-3.75,7.4l1.79,7.17 l-0.06,0.07l0.01,0.06l-2.74,3.23l0.68,2.44l2.5,4.8l3.3-0.5l1,5.2l3.9-0.3l1.4,3.5l3.4,1.6l0.5,2.1l5.3,4.2l4.3,1.3l-0.1,4.9 l5.7,3.5l3.15-5.91l-0.7-5.46l0.72-1.2l0.4-1.3l0.7-2.1l-1.4-1.9l0.3-1.2l0.8-2.8l-1-2.6l0.5-0.3l0.5-0.3l0.9-0.5l0.7-1.1l-0.4-0.1 l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2l0.3-1.9l1.4-3.1l2.7-2.1l2.8-1.1h0.1l3.9,0.5l0.9-2.2l7.2-4.6l-0.7-2l-0.6-2l1.4-0.7L612.6,54.1z", 1, "departement", 3, "ngClass"], ["data-name", "Seine-Saint-Denis", "data-department", "93", "d", "M404.7,152.7l-1.3,2.5l1.3,4.7v0.1l-7.1-2.6l-0.8-2.7l-3.2-0.5l0.1-1.1l-1.3-2l3.3-1.3l2.6,1.1 c1.6-1.1,3.2-2.2,4.7-3.3L404.7,152.7z M663.2,73.89l0.06-0.08l-0.02-0.04l2.61-3.38l-3.95-0.3l-1.6-5.9l0.06-0.06l-0.02-0.06 l6.36-6.56l0.1-5.42l1.1-4l-1.2-3.4l-5.1-8l0.07-0.08l-0.03-0.04l2.65-3.33l-0.89-4.04l-4.5-2.9l-4.1,1.7l-6.4,8.8l-8.2,6.2 l-0.7-0.2l-7.8-1.1l-1.9,1l-5.1-4.6l-1.3-0.2l-1.9-0.7l-5.1,3l-1.6,2.7l-1-1.2l-5.9-2.1l-1.96,2.25v0.2l0.66,2.45l3.9,0.8l4.7,1.9 l0.1,1.4l0.1,1.1l-0.2,0.9l-0.3,0.9l-0.7,1.9l-1.6,0.7l-0.3,0.8l-1.4,0.7l0.6,2l0.7,2l13.9-0.2l0.1,0.1l1.8,3.6l1.8,2.4l0.6,0.8 l0.1,0.5L631,68l0.4,5.4l0.4,1.8l5.9-0.5l0.5-0.3c0.1,0,0.1,0,0.2,0l6.3-2.8l2.9,0.4l0.7,1.3l3,1.5l4,2.9c0,0.1,0.1,0.2,0.2,0.2 l0.7,0.5l6,6.2l0.8,0.6c0.1,0,0.2,0.1,0.3,0.1l3.6,2.6l0.04-0.13l0.43-1.3l0.23-0.68l-1.8-6L663.2,73.89z", 1, "departement", 3, "ngClass"], ["data-name", "Val-de-Marne", "data-department", "94", "d", "M404.7,160l0.3,2.9l-1.1,4.1l-2.3-2.2l-2.8,0.8l-5.1-2.1l0.4-4.1l5.3-0.1l-1.8-1.9L404.7,160z M668.09,102.2 h0.06l-0.02-0.12l3.31-0.19l-1.55-3.58l-3.69-2.41l0.8-8h-0.1l-3.6-2.6c-0.1,0-0.2-0.1-0.3-0.1l-0.8-0.6l-6-6.2l-0.7-0.5 c-0.1,0-0.2-0.1-0.2-0.2l-4-2.9l-3-1.5l-0.7-1.3l-2.9-0.4l-6.3,2.8c-0.1,0-0.1,0-0.2,0l-0.5,0.3l-5.9,0.5v0.1l-0.3,0.8l0.1,3.6 l0.6-0.5l1.6-1.7l2-0.4l2-0.5l4,1.7l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.6,1.1h-0.1l-0.9,0.5l-0.5,0.3l-0.5,0.3 l1,2.5v0.1l-0.8,2.8l-0.3,1.2l1.4,1.9l-0.7,2.1l-0.4,1.3l-0.7,1.2l0.78,5.38h0.06l2.1,0.2l4.7,2.8l3.1-2.2l0.1,5.5l3.3,2.4l4.9-1.8 l0.7,2.5l5.2-2.3l0.5,1.3l1.7,1.7l4.6-3.6l2.1-0.5l5.2-1.8l1.9,6.8l1.7,2.5l3.3,1.8l5.44,1.88l-0.68-5.05l0.05-0.08l-0.01-0.04 l2.5-4.2l2.73-2.74l-1.38-3.64l0.07-0.06l-0.03-0.07l2.35-1.96L668.09,102.2z", 1, "departement", 3, "ngClass"], ["data-name", "Val-d\u2019Oise", "data-department", "95", "d", "m374.3,144l-9.5-0.8l4-9.5l1.6,3.2l5.6,1.1 l6.3-1.8l9.2,2.2l2.2-1.6l10.9,6.4l0.2,2l-1.7,2.3l-0.1,0.1c-1.5,1.1-3.1,2.2-4.7,3.3l-2.6-1.1l-3.3,1.3l-3.6,2.6l-5.5-6.1 L374.3,144z", 1, "departement", 3, "ngClass"], ["data-name", "Centre-Val de Loire", "data-code_insee", "24", 1, "region"], ["data-name", "Cher", "data-department", "18", "d", "m385.3,235.4l5-2.4l13.5,3.1l3.9,4.8l9-1.7l2,6.5l-1.7,5.8l2.7,2.1 l3.1,7.6l0.3,5.9l2.2,2l-0.2,5.8l-1.3,8.9h-0.1h-4l-4.8,3.7l-8.4,2.9l-2.3,1.9l1.7,5.3l-1.7,2.4l-8.7,1l-3.5,5.9v0.1l-4.9-0.2 l1.5-3.5l-0.9-8.9l-4.7-7.9l1.4-2.7l-2.3-2.2l2.5-5.1l-2.3-11.7l-11.6-1.6l2.8-5.5l2.8,0.1l0.6-2.8l9.7-2l-2.1-5.9l5.9-4.1 L385.3,235.4z", 1, "departement", 3, "ngClass"], ["data-name", "Eure-et-Loir", "data-department", "28", "d", "m333.1,200.9l-2.1-3.8l-1.1-7.5l7.5-5.1 l-0.5-4.6l0.2-4.5l-4.8-4.4l-0.1-3.2l2.4-2.6l6-1.1l5.3-3.2l2.8,1.6l6-1.3l-0.2-2.8l6-6.9l3.6,6.6l0.5,10.9l6.2,5.4l1.2,5.6l2.3,2.2 l3.1-0.7l2.8,10.6l-0.5,1.5l-4.8,10.8l-8.5,0.6l-6,2.8l0.2,2.8l-3.3-1.9l-5.5,3.5L339,201.4l-6.3,1.3L333.1,200.9z", 1, "departement", 3, "ngClass"], ["data-name", "Indre", "data-department", "36", "d", "m357.8,308.5l-2.8,2.9l-1.7-2.5l-5.8,1.1 l-2.6-1.1l1.5-2.8l-2.5-1.3l-2.6-5.4h-2.9l-4.6-4.4l0.8-5.8l-2.1-3l5.6-0.5l-1-2.7l3.3-11.9l5.1-2.7l2.3,1.7l2.6-3.5l2.5-2.1l-1-4.9 l6-3.2l2.5,1.3l1.5-2.6l6.4-0.9l5.2,3.5l-2.8,5.5l11.6,1.6l2.3,11.7l-2.5,5.1l2.3,2.2l-1.4,2.7l4.7,7.9l0.9,8.9l-1.5,3.5l-2.7,0.8 l-13.2-2.7l-1.9,2.5L357.8,308.5z", 1, "departement", 3, "ngClass"], ["data-name", "Indre-et-Loire", "data-department", "37", "d", "m303.9,263l-5.5-3.2v-0.1l5.8-15.3l1.7-9.3 l0.7-2.4l6.1,2.6l-0.5-3.3l2.8,0.3l7.7-4.5l10.5,0.5l-0.2,5.5l2.2-1.8l6,3.4l-0.7,2.7l3.4,5.1l-1.2,9.1l2.4,1.9l2.6-1.3l4.2,6.7 l1,4.9l-2.5,2.1l-2.6,3.5l-2.3-1.7l-5.1,2.7l-3.3,11.9l1,2.7l-5.6,0.5l-7.1-10l-0.3-3.1l-5.3-3l1.4,2.9l-10,0.4l-2.8-1.4l-1.3-6.1 l-2.9,0.3L303.9,263z", 1, "departement", 3, "ngClass"], ["data-name", "Loir-et-Cher", "data-department", "41", "d", "m357.9,256.4l-6,3.2l-4.2-6.7l-2.6,1.3 l-2.4-1.9l1.2-9.1l-3.4-5.1l0.7-2.7l-6-3.4l-2.2,1.8l0.2-5.5l-10.5-0.5l0.6-3.5l3.2-1.1l6.3-10.6l-0.4-5.5l-1.7-2.2l2-2.1v-0.1 l6.3-1.3l12.8,10.8l5.5-3.5l3.3,1.9l2.5,7.1l-1.8,3.2l1.7,5.6l3-1.3l2.4,1.5l1.1,3.8l2.9,0.6l1.9-2.3l15.2,1.6l0.8,2.6l-5,2.4 l5.1,7.6l-5.9,4.1l2.1,5.9l-9.7,2l-0.6,2.8l-2.8-0.1l-5.2-3.5l-6.4,0.9l-1.5,2.6L357.9,256.4z", 1, "departement", 3, "ngClass"], ["data-name", "Loiret", "data-department", "45", "d", "m393.3,189.4l3.7,0.6l0.7,3.1l4.2,4.3l-0.6,2.7 l-2.6,1.5l9.2,0.7l11.2-2.7l6.7,7.5l0.4,5.8l-4.6,4.9l1.1,2.9l-1.6,2.4l-5.3,3.3l3,2.8l2.2,6.9l-2.8,0.7l-1.5,2.4l-9,1.7l-3.9-4.8 l-13.5-3.1l-0.8-2.6l-15.2-1.6l-1.9,2.3l-2.9-0.6l-1.1-3.8l-2.4-1.5l-3,1.3l-1.7-5.6l1.8-3.2l-2.5-7.1l-0.2-2.8l6-2.8l8.5-0.6 l4.8-10.8l0.5-1.5l6.9-0.7l1.9-2.9l1.5,2.7L393.3,189.4z", 1, "departement", 3, "ngClass"], ["data-name", "Bourgogne-Franche-Comt\u00E9", "data-code_insee", "27", 1, "region"], ["data-name", "Cote-d\u2019Or", "data-department", "21", "d", "m523.6,241.7l3.9,8.2l-1.2,1.3l-1.8,8.2 l-6.2,6.8l-1.1,4.1v-0.1l-15,1.5l-8.8,4.2l-5.6-6.3l-5.5-1.9l-1.3-2.6l-5.7-1.7l-2.4-2.6V260l0.4-3.2l-3.7-1.2l-1.3-6h0.1l-1.3-2.7 l1.3-8.1l6.7-10.4l-1.7-2.3l2.8-2.1l0.3-3.7l-3.1-3.9l1.9-3.1l2.2-2l6.1-0.9l4.7-3.9l3.9,0.5l3.5,0.7l0.5,2.7l2.6,1l-0.3,2.9 l2.9,0.3l1.8,2.2l1,3.1l-2.8,2.4l2.3,4.8l9.2,2l3,1.6v2.8l4.8-1.9h0.1l2.7-1.6l2,3l0.1,3.2l-4.6,4.1L523.6,241.7z", 1, "departement", 3, "ngClass"], ["data-name", "Doubs", "data-department", "25", "d", "m590.1,245.2l-2.4,2.2l0.4,3l-4.8,6.2l-4.8,4 l-0.4,2.9l-2.5,2.7l-5.7,1.7l-0.3,0.3l-1.7,2.3l0.9,2.7l-0.7,4.5l0.5,2.5l-9.5,8.8l-2.9,5.2l-0.22,0.69l-3.68-3.49l3.6-7.4l2.1-2.3 l-4.2-4.1l-2.9-0.5l-5.8-10.1l-3,0.8l-1.5-2.5l-2,2.1l-1.2-2.5l3-5.1l-5.2-7.8l22.3-10.2l3-4.7l5.6-1.9l2.8,0.9l1.8-2.2l3.2-0.4 l0.5-2.8l5.9,0.8l0.2-0.1h0.1l5.9,2.7l-1.4,2.5l1.4,2.4l0.41-0.46l-0.11,0.16l-2.2,4.9l7-0.7L590.1,245.2z", 1, "departement", 3, "ngClass"], ["data-name", "Jura", "data-department", "39", "d", "m552.3,291.4l3.68,3.49L553.4,303l-5.3,7.2 l-5.5,3.2l-3.8,0.2l-0.4-2.8l-3.4-1.6l-4,4.4l-2.9,0.1l-0.1-3h-2.9l-4.3-7.7l2.8-1.1l-0.8-5.3l2.8-5l-2.2-8.7l-2.5-1.6l5-3.7 l-8.3-4.4l-0.4-2.9l1.1-4.1l6.2-6.8l1.8-8.2l1.2-1.3l2.3,2l5.4,0.1l5.2,7.8l-3,5.1l1.2,2.5l2-2.1l1.5,2.5l3-0.8l5.8,10.1l2.9,0.5 l4.2,4.1l-2.1,2.3L552.3,291.4z", 1, "departement", 3, "ngClass"], ["data-name", "Ni\u00E8vre", "data-department", "58", "d", "m462.8,250l5.5-0.4l1.3,6l3.7,1.2l-0.4,3.2v0.8 l-1.1,0.3l-2.7,0.4v1.3l-2.8,1l0.3,5.9l-2.1,1.7l4,7l-1.9,2.1l0.7,2.9l-11.3,5.7l-7-2.8l-5.9,6l-4.4-3.7l-2.8,1.7l-6.4-0.2l-5.7-6.3 l1.3-8.9l0.2-5.8l-2.2-2l-0.3-5.9l-3.1-7.6l-2.7-2.1l1.7-5.8l-2-6.5l1.5-2.4l2.8-0.7v0.1h3.4l7.4,4.8h6l4.6-4.3l3.9,5.6l5.5,3 l5.8-0.9l0.9,3.7l2.8-0.9L462.8,250z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Saone", "data-department", "70", "d", "m579.1,225.9l1.4,5.5l-0.2,0.1l-5.9-0.8 l-0.5,2.8l-3.2,0.4l-1.8,2.2l-2.8-0.9l-5.6,1.9l-3,4.7L535.2,252l-5.4-0.1l-2.3-2l-3.9-8.2l-2.6-1.4l4.6-4.1l-0.1-3.2l-2-3l-2.7,1.6 h-0.1l1.2-2.5l6.6-3.9l2.1,1.8l3.2-1l0.3-8.3l2-2.4l2.9,0.3l2.3-3.2l-0.2-1.4l8-5.8l7,4.3l5.8-1.6l4.9,3.6l5.1-2.2l8.4,6.6l-2.3,5.7 L579.1,225.9z", 1, "departement", 3, "ngClass"], ["data-name", "Saone-et-Loire", "data-department", "71", "d", "m517.2,270.2v0.1l0.4,2.9l8.3,4.4l-5,3.7 l2.5,1.6l2.2,8.7l-2.8,5l0.8,5.3l-2.8,1.1l-4.8-3.3l-5.4,1.3l-5.9-1.5l-5.9,20.9l-5.7-7.7l-1.6,2.3l-2.5-1.5l-2.2,1.6l-2.2-1.7 l-2.3,1.9l-0.29,2.91L482,318.2v0.1l-5.7,3.8l-2.1-2.1l-8,1.5l-5.2-3.3v-3l3.7-4.6l0.5-5.5l-1.6-2.4l-7.9-2.9l-6.7-13.5l7,2.8 l11.3-5.7l-0.7-2.9l1.9-2.1l-4-7l2.1-1.7l-0.3-5.9l2.8-1l2.7-1.7l1.1-0.3l2.4,2.6l5.7,1.7l1.3,2.6l5.5,1.9l5.6,6.3l8.8-4.2 L517.2,270.2z", 1, "departement", 3, "ngClass"], ["data-name", "Yonne", "data-department", "89", "d", "m425.8,207.1l-6.7-7.5l3.9-5.4l0.2-5.8l15.4-3 l3.6,1.5l4.5,5.5l2.5,8.3l2-2.2l3.6,4.1l5,10.9l12.6-1.6l2.9,1.4l-1.9,3.1l3.1,3.9l-0.3,3.7l-2.8,2.1l1.7,2.3l-6.7,10.4l-1.3,8.1 l1.3,2.7h-0.1l-5.5,0.4l-1.5-2.8l-2.8,0.9l-0.9-3.7l-5.8,0.9l-5.5-3l-3.9-5.6l-4.6,4.3h-6l-7.4-4.8H421v-0.1l-2.2-6.9l-3-2.8 l5.3-3.3l1.6-2.4l-1.1-2.9l4.6-4.9L425.8,207.1z", 1, "departement", 3, "ngClass"], ["data-name", "Territoire de Belfort", "data-department", "90", "d", "m580.3,215.9l0.9-0.6l7.6,5l0.5,9l2.8-0.2l2,5 l-0.1,0.1l-2.79,0.39l-1.11-0.39l-3.19,4.34L586.5,239l-1.4-2.4l1.4-2.5l-5.9-2.7h-0.1l-1.4-5.5l-1.1-4.3L580.3,215.9z", 1, "departement", 3, "ngClass"], ["data-name", "Normandie", "data-code_insee", "28", 1, "region"], ["data-name", "Calvados", "data-department", "14", "d", "m316.9,148l-0.7,2.2l-5.6-1l-7,1.7l-7.2,5.4 l-2.9,0.3l-5.7-1.1l-2.6,1.7l-4.9-3l-6.4,2.3l-2.7-1.3l-0.9,2.7l-5.4,2.9l-9.7-2.1l-1.8-2.4l4.5-5.3l-1.6-2.3l8.1-4.9l-2.2-8.2 l2-2.6l-8.4-3.1l-0.5-6.6v-0.1l0.1-0.7l1.8,0.8l1.9-2.1l3.4-0.3l9.4,3.3l13.9,1.5l6.9,3.4l5.7-0.7l4.7-2.5l4.1-3.7l5.1-1.1l0.3,8.3 h2.9l-2.3,2.1l2.8,9.4l-1.4,3L316.9,148z", 1, "departement", 3, "ngClass"], ["data-name", "Eure", "data-department", "27", "d", "m316.4,153.4l-0.2-3.2l0.7-2.2l-2.3-4.1l1.4-3l-2.8-9.4l2.3-2.1h-2.9 l-0.3-8.3l1.7-0.4l0.28-0.1h1.52l-0.9-0.2l0.8-0.3l-1.29-0.3l5.89-2.4l7.6,5l3.4-0.7l4.9,3l-1.9,2.4l2.1,2.1l5.4,2.4l1.4-2.7 l8.2-2.5l4.8-7l13.1,3.3l3.5,8.4l-4,2.6l-4,9.5l-3.8-0.1l-2.3,2.6l1.8,5.8l-6,6.9l0.2,2.8l-6,1.3l-2.8-1.6l-5.3,3.2l-6,1.1l-2.4,2.6 l-3.4-2.1l1.7-2.3l-7.8-9.5L316.4,153.4z", 1, "departement", 3, "ngClass"], ["data-name", "Manche", "data-department", "50", "d", "m255.2,158.7l9.7,2.1l4.1,4.2l-1.8,6.7 l-3.6,4.5h-0.1l-8.6-0.8l-5.4-2.3l-7.1,4.8l-2.7-1l-4.7-9.6l1.9-0.2l4.8,0.4l2.5-1.1l0.5-2.2l-2.4,1.3l-5.1-5.6l-0.3-5.3l2-6.1 l-0.3-4.9l-1.8-3.6l0.4-7.4l1.5-2l-2.5,0.3l-2-5l0.3-2.2l-2.4-1.2l-2.9-4.1l-0.7-5.9l-1.4-1.9l1.8-1.8l0.1-2.8l-0.5-2.3l-2.2-1.1 l-1-2.5l2.1-0.2l11.9,4.2h2.4l4-2.6l5.1,0.6l1.8,1.7l0.9,2.7l-3.2,5.2l4,6.5l1.1,4.3l-0.1,0.7v0.1l0.5,6.6l8.4,3.1l-2,2.6l2.2,8.2 l-8.1,4.9l1.6,2.3l-4.5,5.3L255.2,158.7z", 1, "departement", 3, "ngClass"], ["data-name", "Orne", "data-department", "61", "d", "m266.9,179.9l-3.3-3.7l3.6-4.5l1.8-6.7 l-4.1-4.2l5.4-2.9l0.9-2.7l2.7,1.3l6.4-2.3l4.9,3l2.6-1.7l5.7,1.1l2.9-0.3l7.2-5.4l7-1.7l5.6,1l0.2,3.2l6.3,0.5l7.8,9.5l-1.7,2.3 l3.4,2.1l0.1,3.2l4.8,4.4l-0.2,4.5l0.5,4.6l-7.5,5.1l1.1,7.5l-3.2-0.7l-3.1-3.5l-2.9,1l-7.2-5l-1.6-8.4l-2.8-1.5l-11,5.9l-3-0.1 v-0.1v-2.9l-3.3-1.6l-1.9-6l-2.7-0.2l-0.7,2.7h-9.1l-6.7,3.3l-2.5-1.7L266.9,179.9z", 1, "departement", 3, "ngClass"], ["data-name", "Seine-Maritime", "data-department", "76", "d", "m314.41,119.8l-7.61-1.8l-1.2-2l-0.1-2.3 l4.4-9.7l13.8-7.4L326,95l10.3-2.1l4.8-1.8l2.4,0.3L352,87l5.11-4.09l11.79,9.99l3.4,8.4l-3.1,4.7l1.4,8.7l-1.3,8l-13.1-3.3l-4.8,7 l-8.2,2.5l-1.4,2.7l-5.4-2.4l-2.1-2.1l1.9-2.4l-4.9-3l-3.4,0.7l-7.6-5L314.41,119.8z", 1, "departement", 3, "ngClass"], ["data-name", "Hauts-de-France", "data-code_insee", "32", 1, "region"], ["data-name", "Aisne", "data-department", "02", "d", "m450.3,82.6l16.7,4.6l2.91,0.94L470.6,94l-1.3,3.5l1.3,3.1l-5,7.2 l-2.7,0.3l0.3,14.3l-1,2.8l-5.3-1.8l-8,4l-1.2,2.6l3.2,8l-5.5,2.3l1.6,2.4l-0.8,2.7l2.5,1.3l-7.7,10.2l-9.3-6l-3.9-4.2l0.7-2.8 l-1.8-2.5l-2.6-0.7l2.1-1.7l-0.5-2.8l-2.9-1.1l-2.4,1.5l-0.7-2.9l3,0.2l-2.9-4.5l2.6-1.7l2.4-5.7l2.6-1.1l-2.2-1.8l0.8-4.5 l-0.4-10.2l-2.3-7l3.9-8.1l0.4-3.8l12.6-0.6l2.6-2.2l2.3,1.7L450.3,82.6z", 1, "departement", 3, "ngClass"], ["data-name", "Nord", "data-department", "59", "d", "m384.33,25.06l0.87-0.26l2,0.8l1.1-2.1l7.9-2.1 l2.9,0.3l4.4-1.9v-0.1l1.2,4.8l2.3,3.7l-1.6,1.9l0.6,0.8l1.2,5.8h3.4l2.7,5.1l3.1,1.5h2.1l0.6-2.4l8.1-3l3.8,7.5l0.1,1l1.3,5.2 l2,3.5h0.1l2.8,0.6l2.1-1.4l2.4-0.2l-0.5,2.2l2.2-0.7l2.8,1l1.8,4.4l-0.6,2.3l0.7,2.3l1.4,1.9l1.1-2.6l4.6-0.3l2.4,1.1L462,64l5.5,6 l2.3,0.2l-2.1,2.4l-1.4,4.7l2.6,0.2l1.4,3.3l-3.5,3.9l0.2,2.5l-16.7-4.6l-5.2,1.8l-2.3-1.7l-2.6,2.2l-12.6,0.6l-3.3-2.6l3.5-10.6 l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6 L384.33,25.06z", 1, "departement", 3, "ngClass"], ["data-name", "Oise", "data-department", "60", "d", "m372.8,131.1l-3.5-8.4l1.3-8l-1.4-8.7l3.1-4.7 l4.1,3.7l3.1-1.2l14.4,2.2l12.8,6.7l8.6-6.8l10.3-1.5l0.4,10.2l-0.8,4.5l2.2,1.8l-2.6,1.1l-2.4,5.7l-2.6,1.7l2.9,4.5l-3-0.2l0.7,2.9 l2.4-1.5l2.9,1.1l0.5,2.8l-2.1,1.7l-8.1,2.9l-2.5-1.6l-2,2.2l-6.9-1l-10.9-6.4l-2.2,1.6l-9.2-2.2L376,138l-5.6-1.1l-1.6-3.2 L372.8,131.1z", 1, "departement", 3, "ngClass"], ["data-name", "Pas-de-Calais", "data-department", "62", "d", "m379.8,68.9l7.1,5.8l12-2.5l-2.6,5.7L398,81 l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7l0.8,3.2l8.6-1.8l3.5-10.6l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1 l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6l-6.27-12.14L372.6,28.5l-6.4,5.4l0.9,5.6l-1.7,4.6l0.6,6.7l2,4.2 l-1.7-1.4l-0.3,9.7l2.27,1.58l10.53,1.02L379.8,68.9z", 1, "departement", 3, "ngClass"], ["data-name", "Somme", "data-department", "80", "d", "m424.3,82.9l3.3,2.6l-0.4,3.8l-3.9,8.1l2.3,7 l-10.3,1.5l-8.6,6.8l-12.8-6.7l-14.4-2.2l-3.1,1.2l-4.1-3.7l-3.4-8.4l-11.79-9.99L359.5,81l3.4-6.6l1.9-1.1l0.1-0.1l1.4,1.8l3.5,0.3 l-5.6-6l1.2-5.1l2.9,0.7l-0.03-0.02l10.53,1.02l1,3l7.1,5.8l12-2.5l-2.6,5.7L398,81l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7 l0.8,3.2L424.3,82.9z", 1, "departement", 3, "ngClass"], ["data-name", "Grand Est", "data-code_insee", "44", 1, "region"], ["data-name", "Ardennes", "data-department", "08", "d", "m469.91,88.14l0.79,0.26l9.8,0.4l7.3-3.2l1.1-6 l4-3.8l2.8-0.2v3.8L494,81l-0.6,5.2l3.3,4.5l-1,2.4l0.6,3.1l1.4,1.9l3.3-0.9l4.3,2.4l2.8,3.8l4.9,0.6l2,1.7l-0.9,2.4l2.1-0.13 l-1.6,1.13l-2,2.7l-5.7-2.1l-1.9,2l0.8,8.8l-3.2,5.1l1.4,2.5l-4.2,3.6v0.1l-20.1-1.9l-9.8-6.6l-6.7-0.9l-0.3-14.3l2.7-0.3l5-7.2 l-1.3-3.1l1.3-3.5L469.91,88.14z", 1, "departement", 3, "ngClass"], ["data-name", "Aube", "data-department", "10", "d", "m442.2,186.9l-3.6-1.5l-0.4-8.5l2.9-0.8l3-5 l3.2,4.5l9,1.2v-3.3l9.5-7.6l6.5-0.9l3.1,0.5l0.4,6.1l2.6,2c1.9,0.8,3.8,1.5,5.6,2.3l2.5-1.5l3.3,1.1l-0.6,3.4l2.4,5.2l5.6,3 l0.5,9.9l-0.1,2.7l-5.6,2.5l0.2,4.8l-3.9-0.5l-4.7,3.9l-6.1,0.9l-2.2,2l-2.9-1.4l-12.6,1.6l-5-10.9l-3.6-4.1l-2,2.2l-2.5-8.3 L442.2,186.9z", 1, "departement", 3, "ngClass"], ["data-name", "Marne", "data-department", "51", "d", "m440.6,158.9l0.4-2l7.7-10.2l-2.5-1.3l0.8-2.7 l-1.6-2.4l5.5-2.3l-3.2-8l1.2-2.6l8-4l5.3,1.8l1-2.8l6.7,0.9l9.8,6.6l20.1,1.9l2.2,9l-1,4.1l2.6,1.3l-0.6,3.9l-3.1,1.1l-1.1,5.8 l3.2,4.6l0.5,4.1l-8.6,2.2l2.2,2.5l-2.3,2.2l0.7,2.9h-4.7l-3.3-1.1l-2.5,1.5c-1.8-0.8-3.7-1.5-5.6-2.3l-2.6-2l-0.4-6.1l-3.1-0.5 l-6.5,0.9l-9.5,7.6v3.3l-9-1.2l-3.2-4.5l-2.6-1.7l-3.5-8.3L440.6,158.9z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Marne", "data-department", "52", "d", "m493.9,167.9l8.6-2.2l3.4,5.2l16.9,10.4 l-2.4,2.3l12.7,9.5l-1.7,8.6l5.5,4.7l0.2,3.1l2.7-1.1l1.3,2.5v0.1l0.2,1.4l-2.3,3.2l-2.9-0.3l-2,2.4l-0.3,8.3l-3.2,1l-2.1-1.8 l-6.6,3.9l-1.2,2.5l-4.8,1.9v-2.8l-3-1.6l-9.2-2l-2.3-4.8l2.8-2.4l-1-3.1l-1.8-2.2l-2.9-0.3l0.3-2.9l-2.6-1l-0.5-2.7l-3.5-0.7 l-0.2-4.8l5.6-2.5l0.1-2.7l-0.5-9.9l-5.6-3l-2.4-5.2l0.6-3.4h4.7l-0.7-2.9l2.3-2.2L493.9,167.9z", 1, "departement", 3, "ngClass"], ["data-name", "Meurthe-et-Moselle", "data-department", "54", "d", "m588.2,170.9l1.9,1.3l-1.5,0.4l-10.6,7.6l-6.1-1.6l-1.6-2.7l-5.3,3.8 l-6,1l-2.4-1.8l-5.4,2l-1.1,2.8l-5.7,0.7l-4.1-4.8l0.1-2.9l-5.8-0.6l0.2-2.9l-2.5-2l1.7-2.8l-1.3-8.6l2.2-13.8l0.9-2.7l-4.9-11.5 l1.5-5.9l-1.2-2.7l-4.4-4.8l-5.3,2l-0.7-5.3l4.8-1.7l2-1.9h6.8l2.54,2.31L539.6,124l2.5,1.6l1.2,3.6l-1.7,3.1l1,5.6l-2.8,0.1 l4.3,7.5l11.5,4l-0.3,2.9l2.7,5.1l8.5,1.5l5.3,3.9l14.4,5.3L588.2,170.9z", 1, "departement", 3, "ngClass"], ["data-name", "Meuse", "data-department", "55", "d", "m516.2,107.97l1.2-0.07l1.5,1.6l1.9,5.6 l0.7,5.3l5.3-2l4.4,4.8l1.2,2.7l-1.5,5.9l4.9,11.5l-0.9,2.7l-2.2,13.8l1.3,8.6l-1.7,2.8l2.5,2l-0.2,2.9l-1.9,2.3l-3-0.5l-6.9,3.4 l-16.9-10.4l-3.4-5.2l-0.5-4.1l-3.2-4.6l1.1-5.8l3.1-1.1l0.6-3.9l-2.6-1.3l1-4.1l-2.2-9v-0.1l4.2-3.6l-1.4-2.5l3.2-5.1l-0.8-8.8 l1.9-2l5.7,2.1l2-2.7L516.2,107.97z", 1, "departement", 3, "ngClass"], ["data-name", "Moselle", "data-department", "57", "d", "m539.6,124l-2.65-10.19l0.65,0.59h2.4l1.5,2.1 l2.3,0.7l2.3-0.5l1-2.3l2-1.2l2.2-0.2l4.5,2.3l4.9-0.1l3.1,3.8l2.3,1.9l-0.5,2l3.7,3.2l2.8,4.5v2.3l4.2,0.7l1.2-1.9l-0.3-2.4 l2.6-0.2l3.8,1.8l1.4,3.5l2.1-1.5l2.5,1.9l5.8-0.4l5.3-4.2l2.2,1.4l0.5,2.1l2.4,2.4l3.2,1.5h0.03l-1.73,4.4l-1.4,2.6l-8.9,0.3 l-9.1-4.6l-0.8-2.8l-5,10.8l5.5,2.4l-1.6,2.5l2.3,1.7l1.3-2.5l3,0.3l4.3,3.4l-3,13.3l-2.3,1.8l-3.4-0.3l-2-2.7l-14.4-5.3l-5.3-3.9 l-8.5-1.5l-2.7-5.1l0.3-2.9l-11.5-4l-4.3-7.5l2.8-0.1l-1-5.6l1.7-3.1l-1.2-3.6L539.6,124z", 1, "departement", 3, "ngClass"], ["data-name", "Bas-Rhin", "data-department", "67", "d", "m631.8,140.7l-2.8,9.4l-7.8,10.5l-2,1.5l-1.4,3.3l0.3,4.9l-2.4,7.2 l0.7,3.6l-1.5,2l-1.2,5.5l-3.16,6.23L605.9,193l-0.3-2.8l-8.5-5.6l-3.1-0.2l-5.2-2.2l1.3-10l-1.9-1.3l3.4,0.3l2.3-1.8l3-13.3 l-4.3-3.4l-3-0.3l-1.3,2.5l-2.3-1.7l1.6-2.5l-5.5-2.4l5-10.8l0.8,2.8l9.1,4.6l8.9-0.3l1.4-2.6l1.73-4.4l8.87,0.6l2.4-0.6 L631.8,140.7z", 1, "departement", 3, "ngClass"], ["data-name", "Haut-Rhin", "data-department", "68", "d", "m605.9,193l4.64,1.83l-0.04,0.07v5.3l1.6,1.9 l0.2,3.4l-2.2,11.1l0.1,6.7l1.8,1.5l0.6,3.5l-2.2,2l-0.2,2.3l-3.1,0.9l0.5,2.2l-1.5,1.6h-2.7l-3.8,1.4l-3-1.1l0.3-2.5l-2.4-1.1 l-0.4,0.1l-2-5l-2.8,0.2l-0.5-9l-7.6-5l2.8-2.4v-6.2l4.8-7.8l4.1-13.5l1.1-1l3.1,0.2l8.5,5.6L605.9,193z", 1, "departement", 3, "ngClass"], ["data-name", "Vosges", "data-department", "88", "d", "m520.4,183.6l2.4-2.3l6.9-3.4l3,0.5l1.9-2.3 l5.8,0.6l-0.1,2.9l4.1,4.8l5.7-0.7l1.1-2.8l5.4-2l2.4,1.8l6-1l5.3-3.8l1.6,2.7l6.1,1.6l10.6-7.6l1.5-0.4l-1.3,10l5.2,2.2l-1.1,1 l-4.1,13.5l-4.8,7.8v6.2l-2.8,2.4l-0.9,0.6l-8.4-6.6l-5.1,2.2l-4.9-3.6l-5.8,1.6l-7-4.3l-8,5.8v-0.1l-1.3-2.5l-2.7,1.1l-0.2-3.1 l-5.5-4.7l1.7-8.6L520.4,183.6z", 1, "departement", 3, "ngClass"], ["data-name", "Pays de la Loire", "data-code_insee", "52", 1, "region"], ["data-name", "Loire-Atlantique", "data-department", "44", "d", "m213.1,265.2l1.8-1l-2.8-4.1l-7.8-3l3-1.3 l0.6-2.2l-0.5-2.5l1.4-2.1l5.8-1.1l-5.5-0.7l-6.6,3.7l-4.1-3.2l-2.2,1l-2.2-1.2l-0.5-4.9l0.9-2.5l3-0.5l-0.9-2.2l-0.18-0.31 l13.18-3.89l0.4-6l5.2-3.4l13.2-0.4l1.6-2.9l9-3.9l6.8,3.6l7.2,13.3l-2.7-0.4l-1.9,2.4l8.5,3.3l0.3,5.9l-14.3,2.1l-2.9,2.2l3,0.8 l3.6,4.7l0.8,2.8l-2.8,4.5l2.8,1.4l0.4,3l-4.8-3.5l-1.5,2.4l-3.2,0.7l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5 L213.1,265.2z", 1, "departement", 3, "ngClass"], ["data-name", "Maine-et-Loire", "data-department", "49", "d", "m270.6,269.2l-12.3,0.8l-10.6-3.8l-0.4-3 l-2.8-1.4l2.8-4.5l-0.8-2.8l-3.6-4.7l-3-0.8l2.9-2.2l14.3-2.1l-0.3-5.9l-8.5-3.3l1.9-2.4l2.7,0.4l-7.2-13.3l0.4-2.2l10.5,3.5 l2.1-1.9l8.7,3.6l3,0.4l5.9-2.7l5.1,1.7l0.6,2.7l6.7-0.2l0.2,3.5l2,2l3.1-1.3l5.2,3.3l7.4,0.1l-0.7,2.4l-1.7,9.3l-5.8,15.3v0.1 l-6.6,5.9l-2.3-2.3l-9.6,0.2l-5.6,0.8L270.6,269.2z", 1, "departement", 3, "ngClass"], ["data-name", "Mayenne", "data-department", "53", "d", "m256.6,221.5l-10.5-3.5l3.6-8.6l5.5-2.2 l-1.9-17.3l1.5-2.4l0.1-12.1l8.6,0.8h0.1l3.3,3.7l2.4-1.6l2.5,1.7l6.7-3.3h9.1l0.7-2.7l2.7,0.2l1.9,6l3.3,1.6v2.9v0.1l-4.3,2.7 l0.3,6.9l-4.4,4l1.2,2.9l-5,4.6l1.4,3.4l-5.5,7.7l1.5,5.6l-5.1-1.7l-5.9,2.7l-3-0.4l-8.7-3.6L256.6,221.5z", 1, "departement", 3, "ngClass"], ["data-name", "Sarthe", "data-department", "72", "d", "m312.7,235.3l-6.1-2.6l-7.4-0.1l-5.2-3.3 l-3.1,1.3l-2-2l-0.2-3.5l-6.7,0.2l-0.6-2.7l-1.5-5.6l5.5-7.7l-1.4-3.4l5-4.6l-1.2-2.9l4.4-4l-0.3-6.9l4.3-2.7l3,0.1l11-5.9l2.8,1.5 l1.6,8.4l7.2,5l2.9-1l3.1,3.5l3.2,0.7l2.1,3.8l-0.4,1.8v0.1l-2,2.1l1.7,2.2l0.4,5.5l-6.3,10.6l-3.2,1.1l-0.6,3.5l-7.7,4.5l-2.8-0.3 L312.7,235.3z", 1, "departement", 3, "ngClass"], ["data-name", "Vend\u00E9e", "data-department", "85", "d", "m269.3,305.1l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l-10.6-3.8l-4.8-3.5l-1.5,2.4l-3.2,0.7 l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5l-5.6-5.6l-0.3,0.1l-0.8,2.6l-3.4,4.3l-1.2,2.3l0.2,2.4l8.7,9.5l2.7,5.6 l1.2,5.3l8,5.4l3.4,0.5l3.9,4.3l2.9-0.1l2,1.2l1.8,2.5l-0.9-2.1l3.9,3.3l0.5-2.7l2.4,0.3l7.1-2.7l-1.4,2.9l6.5-0.3l2.4,1.8l9.1-4.5 L269.3,305.1z", 1, "departement", 3, "ngClass"], ["data-name", "Bretagne", "data-code_insee", "53", 1, "region", "region-53"], ["data-name", "Cotes-d\u2019Armor", "data-department", "22", "d", "m208.7,188.9l-4.9,7.1l-2.9,1.1l-1.5-2.7 l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9l-12.9-6.5l-7.9,3l-12.46-3.29l2.06-4.11l-2.5-9.3l2.5-8.3l-3.6-4.7l1.1-4.3l1.2,1.4l3.2-0.4 l1.1-7.7l1.5-1.6l2.2-0.6l1.9,1.4h2.5l2.1-1l2.2,0.3l1.5-1.8l0.9,2L170,153l3-3.6l2.9-0.8l-0.1,2.3l-1.2,4.4l1.7-3.1l2.6-0.5l-1.1,2 l7.2,7.8l2.2,5.4l3,2l0.8,3.7l0.7-2.2l3-1l2.4-2.7l8.1-3.3l2.7-0.2l-2,2.5l2.9-1.1l1.8,4.4l1.3-1.9l2.5,0.2v-0.09l1.6,3.99h-0.3h0.3 l2.5,0.3l0.7,0.2l0.4,1.7l-1.9,13L208.7,188.9z", 1, "departement", 3, "ngClass"], ["data-name", "Finist\u00E8re", "data-department", "29", "d", "m151.6,210.1l2,3.4l-0.8,1.4l-5.5-1.2l-1.2-1.9 l2.2-0.7l-3,0.8l-0.3-2.7v2.7l-2.5,0.7l-2.2-1l-4.2-6.1l-0.8,2.5l-2.3,0.2l-3.5-3.1l1.6-4.6l-2.4,4.3l1.3,1.9l-2.2,1l-1,2.8 l-5.9-0.2l-2.1-1.6l1.5-1.6l-1.5-5.5l-2.4-3.1l-2.8-1.8l1.6-1.7l-2.1,1.4l-7.5-2.2l2.2-1.3l12.5-1.8l1.8,1.8l2-1.3l0.7-2.5l-1.6-3.6 l-6.8-2.5l-1.5,2.6l-2.6-4.2l1.3-1.8l-0.3-2.2l1.7,2.3l4.9,1l4.6-0.8l2.1,3.1l5.4,1l-3.7-0.9l-2.8-2l2.2-0.5l-4.2-2l2-1.5l-2.6-0.2 l-2.7,0.8l-0.8-2.2l7.1-4.5l-4.4,2.2l-2.3,0.1l-7.5,2.9l-2.7-1.2l-2.7,1.2l-1.5-1.8l0.6-5.3l2.5-1.6l-2.2-0.9l0.8-2.6l1.8-1.6 l2.1-0.8l5.1,1.5l-1.9-1.1l2.5-1.2l1.6,1.4l-1.9-1.7l1.2-1.9l2.9-0.1l3.8-2l2.3,2.6l6.7-3.1l3,1.6l1-2.2l2.9-0.5l0.4,5l2.2-1.5 l1.3,2.5l1.2-4.5l4.7,0.3l1.2,1.7l-1.1,4.3l3.6,4.7l-2.5,8.3l2.5,9.3l-2.06,4.11l-0.04-0.01v0.1l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3 l0.1,5.4l-2.5,2.8L151.6,210.1z", 1, "departement", 3, "ngClass"], ["data-name", "Ille-et-Vilaine", "data-department", "35", "d", "m255.2,207.2l-5.5,2.2l-3.6,8.6l-0.4,2.2 l-6.8-3.6l-9,3.9l-1.6,2.9l-13.2,0.4l-5.2,3.4l-1-5.8l3-0.7l-2.8-1.5l2.4-2.2l1-3.2l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8 l3.5-0.9l-3.6-0.1l-1-4.4l4.9-7.1l9-2.5l1.9-13l-0.4-1.7l-0.7-0.2l-2.5-0.3l-1.6-3.99l0.05-0.86l0.05-0.85l0.7-0.1h2.1v0.1l1.7,4.4 l1.3,2l-0.5,2.1l1.4-2.1l-2.3-5.1l0.7-2.5l2.2-1.5l2.3-0.6l2.2,1l-1.5,2.3l2.9,2.4l7.3-0.6l4.7,9.6l2.7,1l7.1-4.8l5.4,2.3l-0.1,12.1 l-1.5,2.4L255.2,207.2z", 1, "departement", 3, "ngClass"], ["data-name", "Morbihan", "data-department", "56", "d", "M167.7,242.6l2.9,1.2l-1.1,2.1l-5.1-1.2l-1.3-2.7l0.4-3l2.1,1.4L167.7,242.6z M209.1,219.2l2.4-2.2l1-3.2 l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8l3.5-0.9l-3.6-0.1l-1-4.4l-2.9,1.1l-1.5-2.7l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9 l-12.9-6.5l-7.9,3l-12.46-3.29l-0.04,0.09l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3l0.1,5.4l-2.5,2.8l-2.8-0.8l2,3.4l0.1,1.5l2.9,4.4 l2.3-0.2l1.5-1.7l-0.8-5.1l0.6,2.4l1.7,1.7l1.9-1.7l-2.5,4.2l2.2,1.4l-2.3-0.6l3.2,1.9l0.1,0.1l1.6,1l1.7-2.5l-1.6,3.1l2.1,2.6 l0.6,3.5l-0.9,2.8l2.1,1.1l-1.2-3l0.5-3.8l2.2,1.6l5.1,0.1l-0.7-5l1.4,2l2.1,1.5l4.8-0.5l2.1,2.4l-1,2.2l-2.1-0.6l-4.8,0.4l3.8,3.3 l12.9-0.9l3.1,1.5l-3.4,0.1l1.42,2.39l13.18-3.89l0.4-6l-1-5.8l3-0.7L209.1,219.2z", 1, "departement", 3, "ngClass"], ["data-name", "Nouvelle-Aquitaine", "data-code_insee", "75", 1, "region"], ["data-name", "Charente", "data-department", "16", "d", "m294.8,379.2l-2,2v-0.1l-6.3-6.3l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6 l1.7-2.6l-2.4-1.7l-0.3-3l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l2.7-1.6l0.3-3l5.8-2.5l3.5,0.4l0.8-0.8h0.1l9.1,3 l2.9-0.8l-1.4-2.4l2.2-1.8l4.1,3.9l3.8-1.4l1.3-2.5l4.8,0.6l-0.2,5.1l4.7,3.6l-0.6,3.2l-2.6,1.1l-4,8l-2.8,0.6l-3.4,3.8h0.1 l-5.7,6.1l-2.1,5.3l-7.9,5.9l-0.7,5.7l-4.1,5.8L294.8,379.2z", 1, "departement", 3, "ngClass"], ["data-name", "Charente-Maritime", "data-department", "17", "d", "M242.8,341.1l-1.4-5l-3.5-3l-1.3-2.3l1.5-3.6l1.7,1.8l2.9,0.5l1.4,8.4L242.8,341.1z M241.9,318.9l-5.8-4.5 l-4.4-1.5l-0.6,2.9l2.7,0.1l4.8,3.3L241.9,318.9z M286.5,374.8l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6l1.7-2.6l-2.4-1.7l-0.3-3 l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l-3.6-4.7l-17.4-6.7l-5.9-6.5v-3.7l-2.4-1.8l-6.5,0.3l1.4-2.9l-7.1,2.7 l0.5,0.1l-0.6,3.4l-4.5,5.9l2.4,0.3l2.2,1.7l3,7.2l-1.5,1.9l-0.2,5.1l-3.3,3.1l-0.1,2.6l-2.2,0.4l-1.5,1.7l1.1,4.3l9,6.5l1.5,2.6 l4.3,2.7l3.7,4.8l1.81,7.3l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l2-5l0.1-0.4v-0.1L286.5,374.8z", 1, "departement", 3, "ngClass"], ["data-name", "Corr\u00E8ze", "data-department", "19", "d", "m363.6,392.3l-8.1,0.8l-3.5-7l-3.2-0.7l-0.2-3 l-2.3-1.5l2-1.8l-1.7-3l3.6-4.6l-2.9-4.7l1.6-2.7l2.5,1.2l4.7-4l5.7-1.3l4.9-4.6l8.7-4l7-3.4l11.2,5.2l2.3-2.6l2.7,0.8l2.4-2.4 l1.2,5.6l-1.7,2.4l1.2,7.9l0.7,6l-6.2-2l-0.6,3.5l-7.6,9.5l1.8,2.2l-2.3,1.9l-0.3,3.5l-3.1,1.1l1.5,3.4l-3.2,1.9h-0.1l-6.7-0.2 l-5.3,2.7L363.6,392.3z", 1, "departement", 3, "ngClass"], ["data-name", "Creuse", "data-department", "23", "d", "m396.6,343.5l4.4,5.5l-2.4,2.4l-2.7-0.8 l-2.3,2.6l-11.2-5.2l-7,3.4l-0.6-5.9l-4.7-3l-6.4-0.5l-0.1-2.8l-2.9-1.5l0.9-3.4l-1.8-5.2l-6.6-9.8l3-5.3l-1.2-2.6l2.8-2.9l11.5-1.1 l1.9-2.5l13.2,2.7l2.7-0.8l4.9,0.2l1.1,3.9c2.5,1.6,4.9,3.2,7.4,4.8l3.6,8.4l-0.5,4.1l2.3,6.7L396.6,343.5z", 1, "departement", 3, "ngClass"], ["data-name", "Dordogne", "data-department", "24", "d", "m307.7,414.3l-2.8-6.4l-1-1.3l0.9-2.9l-2.4-2.6l-2,3.2l-9.8-2.3l2-2 l0.2-5.7l2.8-5.5l-1.2-2.8l-3.7,0.6l2-5l0.1-0.4l2-2l5.5-0.7l4.1-5.8l0.7-5.7l7.9-5.9l2.1-5.3l5.7-6.1l6.2,3l-0.1,4.7l9.5-1.1 l7.2,5.6l-2,2.7l5.7,2.2l2.9,4.7l-3.6,4.6l1.7,3l-2,1.8l2.3,1.5l0.2,3l3.2,0.7l3.5,7l-0.7,5l-1.4,5.3l-4.5,3.2l0.6,3.6l-6,3.4 l-4.7,6.5l-4.2-4.2l-5.4,2.7l-1.5-6l-6.1,1l-2.2-1.8l-2.8,2L307.7,414.3z", 1, "departement", 3, "ngClass"], ["data-name", "Gironde", "data-department", "33", "d", "m243.9,420.1l-5.8,2.6v-4.6l2.2-3.2l0.5-2.3 l1.9-1.7l1.8,1.4l3.1-0.2l-1.1-4.6l-3.5-3.4l-2.8,4l-1.2,3.8l6.2-50l0.9-2.8l3.3-3.4l1.4,4.7l9,9l2.8,7.6l1.7-3.1l-0.59-2.4 l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l3.7-0.6l1.2,2.8l-2.8,5.5l-0.2,5.7l-2,2l9.8,2.3l2-3.2l2.4,2.6l-0.9,2.9l1,1.3 l-3.1-0.1l-1.2,2.5l-2.7-0.9l-1.1,3.3l2.9,1.4l-8.5,8.6l-0.6,8.9l-3,2.3l1.5,2.5l-4.5,4l-2.1-2.7l-1.6,3.6h-6.4l-0.6-4.7l-11-7.7 l0.4-2.8l-17.2,0.7l1.5-5.4L243.9,420.1z", 1, "departement", 3, "ngClass"], ["data-name", "Landes", "data-department", "40", "d", "m222.32,481.21l1.08-1.51l3.9-7.1l8.8-37.8 l2-11.7v-0.4l5.8-2.6l3.7,1.3l-1.5,5.4l17.2-0.7l-0.4,2.8l11,7.7l0.6,4.7h6.4l1.6-3.6l2.1,2.7l0.4,4.6l11.7,2.9l-3.6,5.2l0.7,2.6 l-0.4,2.9l-2.5,1.3l-0.6-3l-9.4,2.7l0.5,6.4l-4.2,11.1l1.6,2.7l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2 l-1.6,2.2l-2.5-1.4l-2.7,1.3l-1.2-2.8l-11,2.5L222.32,481.21z", 1, "departement", 3, "ngClass"], ["data-name", "Lot-et-Garonne", "data-department", "47", "d", "m293.8,455.6v0.1l-0.7-2.6l3.6-5.2L285,445 l-0.4-4.6l4.5-4l-1.5-2.5l3-2.3l0.6-8.9l8.5-8.6l-2.9-1.4l1.1-3.3l2.7,0.9l1.2-2.5l3.1,0.1l2.8,6.4l8.9-0.5l2.8-2l2.2,1.8l6.1-1 l1.5,6l5.4-2.7l4.2,4.2l-3.4,3.1l2.7,9.1l-7.5,2v2.9l2.4,1.4l-4.4,5.5l1.3,2.7l-2.8-0.2l-3.6,4.7l-2.7,1.3l-8.6-1l-5,2.9l-8.3-0.7 l-1.4,2.5L293.8,455.6z", 1, "departement", 3, "ngClass"], ["data-name", "Pyr\u00E9n\u00E9es-Atlantiques", "data-department", "64", "d", "m276.9,513.4l3.4-0.8l-0.4-2.9l8-9.3l-0.8-3.1 l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l-6.6-0.3l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2l-1.6,2.2l-2.5-1.4 l-2.7,1.3l-1.2-2.8l-11,2.5l-3.98-1.89l-3.52,4.89l-2.7,1.9l-4.5,0.9l1.9,4.5l4.5-0.2l0.2,2.2l2.4,1l2.2-2.1l2.4,1.3l2.5,0.1 l1.4,2.8l-2.5,6.7l-2.1,2.2l1.3,2.2l4.3-0.1l0.7-3.4l2.3-0.1l-1.3,2.4l5.9,2.3l1.5,1.8h2.5l6.1,3.8l5.8,0.4l2.3-1l1.4,2.1l0.3,2.8 l2.7,1.3l3.9,4l2.1,0.9l1.1-2.1l2.7,2.1l3.6-1.1l0.19-0.16l1.41-9.34L276.9,513.4z", 1, "departement", 3, "ngClass"], ["data-name", "Deux-S\u00E8vres", "data-department", "79", "d", "m292.3,331.6l-2.7,1.6l-3.6-4.7l-17.4-6.7 l-5.9-6.5v-3.7l9.1-4.5l-2.5-2l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l12.3-0.8l3.7-4.8l5.6-0.8l9.6-0.2l2.3,2.3l3.4,9l-0.8,3l2.7,1.2 l-4.5,14.1l2.7-0.9l1.5,3l-3.4,5.5l0.5,5.8l2.1,2l-0.1,2.8l6.4,0.2l-3.2,8.5l4.5,3l-0.8,2.8h-0.1l-0.8,0.8l-3.5-0.4l-5.8,2.5 L292.3,331.6z", 1, "departement", 3, "ngClass"], ["data-name", "Vienne", "data-department", "86", "d", "m329.6,320.8v3.5l-4.8-0.6l-1.3,2.5l-3.8,1.4 l-4.1-3.9l-2.2,1.8l1.4,2.4l-2.9,0.8l-9.1-3l0.8-2.8l-4.5-3l3.2-8.5l-6.4-0.2l0.1-2.8l-2.1-2l-0.5-5.8l3.4-5.5l-1.5-3l-2.7,0.9 l4.5-14.1l-2.7-1.2l0.8-3l-3.4-9l6.6-5.9l5.5,3.2l0.3,3.2l2.9-0.3l1.3,6.1l2.8,1.4l10-0.4l-1.4-2.9l5.3,3l0.3,3.1l7.1,10l2.1,3 l-0.8,5.8l4.6,4.4h2.9l2.6,5.4l2.5,1.3l-1.5,2.8l-0.8-0.3l-1.3,2.4l-3.3-0.9l-1.3,3l-5.6,2.7L329.6,320.8z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Vienne", "data-department", "87", "d", "m348.9,364.1l-1.6,2.7l-5.7-2.2l2-2.7l-7.2-5.6 l-9.5,1.1l0.1-4.7l-6.2-3h-0.1l3.4-3.8l2.8-0.6l4-8l2.6-1.1l0.6-3.2l-4.7-3.6l0.2-5.1v-3.5l3-5l5.6-2.7l1.3-3l3.3,0.9l1.3-2.4 l0.8,0.3l2.6,1.1l5.8-1.1l1.7,2.5l1.2,2.6l-3,5.3l6.6,9.8l1.8,5.2l-0.9,3.4l2.9,1.5l0.1,2.8l6.4,0.5l4.7,3l0.6,5.9l-8.7,4l-4.9,4.6 l-5.7,1.3l-4.7,4L348.9,364.1z", 1, "departement", 3, "ngClass"], ["data-name", "Occitanie", "data-code_insee", "76", 1, "region"], ["data-name", "Ari\u00E8ge", "data-department", "09", "d", "m369.82,543.59l0.78-0.89l-2.6-1.1l-2-2.1 l-3.7-0.1l-1.7-1.7l-2.8,0.4l-1.3,2.1l-2.4-0.8l-2.8-5.9l-10-0.6l-1.3-2.8l-13.2-3.9l-0.5-1.4l3.8-5.2l2.8-1v-5.9l3.9-4l2.8-1.1 l6.2,4.1l-0.4-5.6l5.4-1.6l-3-4.8l2.8-1.1l3.4,5.5l2.8-0.5l0.6-2.8l5.7,2.2l2-2.3l2.2,5.5l8.7,3.9l2.2,5.2l0.2,3.1l-2.2,2.3l2.4,2.5 l-1.2,3l-3.2,0.6l0.8,5.7l3.4,1.5l3.3-1.2l4.8,5.6l-7.4,0.2l-1.3,2.6L369.82,543.59z", 1, "departement", 3, "ngClass"], ["data-name", "Aude", "data-department", "11", "d", "m435.07,504.37l-1.47,1.53l-5.2,9.3l-0.9,3.5 l0.15,9.57l-9.45-5.57l-8.2,5.4l-13.6-1l-2.7,1.4l1.4,6l-8.6,3.9l-4.8-5.6l-3.3,1.2l-3.4-1.5l-0.8-5.7l3.2-0.6l1.2-3l-2.4-2.5 l2.2-2.3l-0.2-3.1l-2.2-5.2l-8.7-3.9l-2.2-5.5l8.4-10l1.4,2.7l5.2-1.8l0.5-0.8l1.8,2.3l6.3,0.9l1.1-3.3l2.8-0.5l12,1.4l-0.5,2.8 l3.5,5l2.5-1.6l1.4,2.9l3.1-0.8l3.8-5.3l1,2.9l13.8,4.7l1.7,2L435.07,504.37z", 1, "departement", 3, "ngClass"], ["data-name", "Aveyron", "data-department", "12", "d", "m430.8,440.7l9.4,4.5l-2,3.9l-2.8,1.1l8.4,4.1 l-4.3,5.3l0.3,1.5l-3.7,1l-3,5.3l-6.3-1.3l-0.1,8.7l-5.7-0.1l-1.3-2.8l-11.1-1.3l-4.2-5l-4.3-11.5l-4.8-4.3L385,444l-6.1,2.8 l-4.3-3.6l2.3-2.4l-3.1-2.7l0.4-3l-0.8-9.1l7.6-5l5.9-1.4l1.7-1.5h0.1l5.1-3.2l6.4,1.5l3.8-4.8l3-9.1l4.7-4.2l5.2,4l1.3,4.2l2.4,1.6 l-0.5,3l2.6,5.1v0.1l4.2,4.5l2.9,8.8l-0.5,8.7L430.8,440.7z", 1, "departement", 3, "ngClass"], ["data-name", "Gard", "data-department", "30", "d", "m480,487.2l-2.8-0.6l-1.9-1.6l-1.1-3.4h-0.1 l3.3-4.4l-1.5-3l-6.1-6.7l-3-0.2l-0.2-3l-6.8-1.4l0.9-2.7l-1.9-2.6l-3.9,0.6l-4.2,3.9l-0.1,2.8l-5.3-2.5l-2.2,1.7l-0.4-2.9l-2.9-0.1 l-0.3-1.5l4.3-5.3l-8.4-4.1l2.8-1.1l2-3.9l7.8,3.4l3.9-0.5l0.1-3.3l8.7,2.2l6.3-1.8l-1.4-3l1.2-2.9l-3.9-7.7l3.6-2.5l1.1-2.1 l2.7,5.9l7.8,5l7.1-4.3l0.1,3.1l2.5-2.3h2.8l6,3.5l2.6,4.4l0.2,5.5l6.3,6.4l-4.5,5l-3.9,4.1l-1.9,10.6l-3.3-0.9l-4.2,4.8l1,2.7 l-5.8,1.8L480,487.2z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Garonne", "data-department", "31", "d", "m326.8,526.2l-5.5-1.5l-1.2,2.4l0.2,7.6 l-8.8-0.7l-1.7,0.3l-0.6-7l5.5-3.2l2.6-5.3l-0.8-2.7l-3.1,0.3l0.6-3.5l-4.6-4l7.1-11.2l3.1-1.1l3.5-5.3l11.4,2.5l0.7-5.8l6.5-6.1 l-9.1-13.3l9.9-0.9l1.7,2.3l5.8-2.5l-2.2-2.3l11.7-4.3l1.4,6.3l2.6,1.2l0.2,2.8l2.3,2.1l-0.7,5.4l14.3,9.3l1,2.8l-0.5,0.8l-5.2,1.8 l-1.4-2.7l-8.4,10l-2,2.3l-5.7-2.2l-0.6,2.8l-2.8,0.5l-3.4-5.5l-2.8,1.1l3,4.8l-5.4,1.6l0.4,5.6l-6.2-4.1l-2.8,1.1l-3.9,4v5.9 l-2.8,1l-3.8,5.2L326.8,526.2z", 1, "departement", 3, "ngClass"], ["data-name", "Gers", "data-department", "32", "d", "m330.6,461.7l2,6.9l9.1,13.3l-6.5,6.1l-0.7,5.8 l-11.4-2.5l-3.5,5.3l-3.1,1.1l-12.4-2.2l-1.4-3l-5.5,0.6l-2.6-8.7l-3.3-1.3l-2-3.5l-3.9,0.5l-6.6-0.3l-1.6-2.7l4.2-11.1l-0.5-6.4 l9.4-2.7l0.6,3l2.5-1.3l0.4-2.9v-0.1l3.7,0.7l1.4-2.5l8.3,0.7l5-2.9l8.6,1l2.7-1.3l5.3,1.7l-3.3,4.6L330.6,461.7z", 1, "departement", 3, "ngClass"], ["data-name", "H\u00E9rault", "data-department", "34", "d", "m474.1,481.6l-2.4-0.1l-5.9,2.6l-3.6,3.2 l-7.2,4.6l-4.3,4.2l2.1-3.5l-4.3,6.6h-6.8l-5.5,4l-1.13,1.17l-0.17-0.17l-1.7-2l-13.8-4.7l-1-2.9l-3.8,5.3l-3.1,0.8l-1.4-2.9 l-2.5,1.6l-3.5-5l0.5-2.8l3.4-2l0.8-3l-0.7-9.7l6.1,2.2c2.3-1.5,4.6-2.9,6.8-4.4l5.7,0.1l0.1-8.7l6.3,1.3l3-5.3l3.7-1l2.9,0.1 l0.4,2.9l2.2-1.7l5.3,2.5l0.1-2.8l4.2-3.9l3.9-0.6l1.9,2.6l-0.9,2.7l6.8,1.4l0.2,3l3,0.2l6.1,6.7l1.5,3L474.1,481.6z", 1, "departement", 3, "ngClass"], ["data-name", "Lot", "data-department", "46", "d", "m385.4,413.1l3.3,5h-0.1l-1.7,1.5L381,421 l-7.6,5l0.8,9.1l-6.2,0.8l-7.5,5.5l-2.6-2.3l-8.7,2.5l-0.5-4l-2.4,1.5l-2.7-1l-4.5-4l2.1-2.3l-3.1,0.5l-2.7-9.1l3.4-3.1l4.7-6.5 l6-3.4l-0.6-3.6l4.5-3.2l1.4-5.3l0.7-5l8.1-0.8l6.7,6.1l5.3-2.7l6.7,0.2l1,5.4l3.8,6L385.4,413.1z", 1, "departement", 3, "ngClass"], ["data-name", "Loz\u00E8re", "data-department", "48", "d", "m463.4,418.7l4.2,8.3l-1.1,2.1l-3.6,2.5 l3.9,7.7l-1.2,2.9l1.4,3l-6.3,1.8l-8.7-2.2l-0.1,3.3l-3.9,0.5l-7.8-3.4l-9.4-4.5l-1.5-2.4l0.5-8.7l-2.9-8.8l-4.2-4.5v-0.1l6.9-15.9 l1.7,2.3l6.8-5.7l1-1l2.3,1.7l1.5,5.7l6.4,1.2l0.1-2.8l2.9,0.2l9,7.7L463.4,418.7z", 1, "departement", 3, "ngClass"], ["data-name", "Hautes-Pyr\u00E9n\u00E9es", "data-department", "65", "d", "m314.7,524.1l-5.5,3.2l0.6,7l-0.7,0.2l-2.3-1.6 l-2.4,1.8l-2.5-0.5l-1.9-1.7l-3.9-0.3l-6.9,2.1l-2.2-0.9l-2.1-1.7l-1.1-2.5l-7.8-5.5l-2.11,1.84l1.41-9.34l1.6-2.8l3.4-0.8l-0.4-2.9 l8-9.3l-0.8-3.1l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l3.9-0.5l2,3.5l3.3,1.3l2.6,8.7l5.5-0.6l1.4,3l12.4,2.2l-7.1,11.2l4.6,4 l-0.6,3.5l3.1-0.3l0.8,2.7L314.7,524.1z", 1, "departement", 3, "ngClass"], ["data-name", "Pyr\u00E9n\u00E9es-Orientales", "data-department", "66", "d", "m427.65,528.27l0.25,15.63l3.9,3.3l1.9,3.8 h-2.3l-8.1-2.7l-6.9,3.9l-3-0.2l-2.4,1.1l-0.6,2.4l-2.1,1.2l-2.4-0.7l-2.9,1l-4-3.1l-7-2.9l-2.5,1.4h-3l-1,2.1l-4.6,2l-1.9-1.7 l-1.7-4.8l-7.5-2l-2-2.1l2.02-2.31l7.98-2.39l1.3-2.6l7.4-0.2l8.6-3.9l-1.4-6l2.7-1.4l13.6,1l8.2-5.4L427.65,528.27z", 1, "departement", 3, "ngClass"], ["data-name", "Tarn", "data-department", "81", "d", "m419.7,471.9l1.3,2.8c-2.2,1.5-4.5,2.9-6.8,4.4 l-6.1-2.2l0.7,9.7l-0.8,3l-3.4,2l-12-1.4l-2.8,0.5l-1.1,3.3l-6.3-0.9l-1.8-2.3l-1-2.8l-14.3-9.3l0.7-5.4l-2.3-2.1l-0.2-2.8l-2.6-1.2 l-1.4-6.3l0.5-2.8l4.8-3.2l1-2.7L364,450l3-1.1l2.7,1.1l9.2-3.2l6.1-2.8l10.3,5.8l4.8,4.3l4.3,11.5l4.2,5L419.7,471.9z", 1, "departement", 3, "ngClass"], ["data-name", "Tarn-et-Garonne", "data-department", "82", "d", "m360,458.1l-0.5,2.8l-11.7,4.3l2.2,2.3 l-5.8,2.5l-1.7-2.3l-9.9,0.9l-2-6.9l-5.1-4.1l3.3-4.6l-5.3-1.7l3.6-4.7l2.8,0.2l-1.3-2.7l4.4-5.5l-2.4-1.4v-2.9l7.5-2l3.1-0.5 l-2.1,2.3l4.5,4l2.7,1l2.4-1.5l0.5,4l8.7-2.5l2.6,2.3l7.5-5.5l6.2-0.8l-0.4,3l3.1,2.7l-2.3,2.4l4.3,3.6l-9.2,3.2l-2.7-1.1l-3,1.1 l1.8,2.2l-1,2.7L360,458.1z", 1, "departement", 3, "ngClass"], ["data-name", "Auvergne-Rhone-Alpes", "data-code_insee", "84", 1, "region"], ["data-name", "Ain", "data-department", "01", "d", "m542,347l-5.7,6.7l-11.2-15.2l-2.8,0.7l-3,5.1 l-6-2l-6.4,0.5l-3.7-5.7l-2.8,0.5l-3.1-9.2l1.5-8l5.9-20.9l5.9,1.5l5.4-1.3l4.8,3.3l4.3,7.7h2.9l0.1,3l2.9-0.1l4-4.4l3.4,1.6 l0.4,2.8l3.8-0.2l5.5-3.2l5.3-7.2l4.5,2.7l-1.8,4.7l0.3,2.5l-4.4,1.5l-1.9,2l0.2,2.8l0.46,0.19l-4.36,4.71h-2.9l0.8,9.3L542,347z", 1, "departement", 3, "ngClass"], ["data-name", "Allier", "data-department", "03", "d", "m443.1,292.3l5.9-6l6.7,13.5l7.9,2.9l1.6,2.4l-0.5,5.5l-3.7,4.6 l-3.9,1.3l-0.5,3l1.5,12.4l-5.5,4.8l-3.5-4.3l-6.4-0.4l-1.4-3.2l-13.1-0.5l-1.6-2.5l-3.3,0.5l-4.4-4.5l1.2-2.8l-2.3-1.7l-11.2,8 l-2.5-1.2l-3.6-8.4c-2.5-1.6-4.9-3.2-7.4-4.8L392,307v-0.1l3.5-5.9l8.7-1l1.7-2.4l-1.7-5.3l2.3-1.9l8.4-2.9l4.8-3.7h4h0.1l5.7,6.3 l6.4,0.2l2.8-1.7L443.1,292.3z", 1, "departement", 3, "ngClass"], ["data-name", "Ard\u00E8che", "data-department", "07", "d", "m496.5,434.2l0.1,3.7l-6-3.5h-2.8l-2.5,2.3 l-0.1-3.1l-7.1,4.3l-7.8-5l-2.7-5.9l-4.2-8.3l-2.1-9.1l6.7-6.4l5.9-1.9l3.4-5.9l3.4-0.4l-0.7-2.8l2.6-2.3l1.5-5.2l2.6,1.2v-3.1 l0.9-4.1l3.5-0.8l3.2-4.9l5-2.7l2,4.2l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9L496.5,434.2z", 1, "departement", 3, "ngClass"], ["data-name", "Cantal", "data-department", "15", "d", "m435.6,387.9l3.5,8l-1,1l-6.8,5.7l-1.7-2.3 l-6.9,15.9l-2.6-5.1l0.5-3l-2.4-1.6l-1.3-4.2l-5.2-4l-4.7,4.2l-3,9.1l-3.8,4.8l-6.4-1.5l-5.1,3.2l-3.3-5l1.7-5.8l-3.8-6l-1-5.4h0.1 l3.2-1.9l-1.5-3.4l3.1-1.1l0.3-3.5l2.3-1.9l-1.8-2.2l7.6-9.5l0.6-3.5l6.2,2l-0.7-6l7.5,3.5l1.5,2.5l6.7,0.3l6.5,5.4l3.7-4.1v3.9 l5.5,1.5l3.3,8.7l2.6,1.1L435.6,387.9z", 1, "departement", 3, "ngClass"], ["data-name", "Drome", "data-department", "26", "d", "m535.1,404.4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9 l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9l-2.1,14.4l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l4.3-4.8l2.3-0.1l1-0.2l0.2-4.7l-10-5.7l-1.5-2.6l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3 l2.5-6.7l5.8-0.3l0.3-3.4l-5.9-0.8L535.1,404.4z", 1, "departement", 3, "ngClass"], ["data-name", "Is\u00E8re", "data-department", "38", "d", "m513.6,349.4l-0.3-7.1l6,2l3-5.1l2.8-0.7 l11.2,15.2l6.5,10.5l6.2,0.2l0.3-2.8l9.4,2.1l2.7,6.3l-2.3,5.5l1,5.4l5.2,1.5l-1.6,3.8l1.8,4.2l4.4,3.1l-0.4,5.8l-3.1-1.1l-12.6,3.9 l-0.9,2.8l-5.5,1.2l-1,3.1l-5.9-0.8l-5.4-4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l-2-4.2v-4.4 l-0.2-1.1h0.1l4.4-3.9l-1.9-2.5l2.5-2.5l6.9-1.5L513.6,349.4z", 1, "departement", 3, "ngClass"], ["data-name", "Loire", "data-department", "42", "d", "m499.3,365.9v4.4l-5,2.7l-3.2,4.9l-3.5,0.8 l-2.2-2.4l-2.6,1l-0.7-5.5l-6-2.2l-6.2,3l-2.8,0.4l-2.3-2l-2.8,0.8l3-7.1l-2.7-7.5l-4.6-3.8l-4.7-7.7l2.1-6.3l-2.5-2.7l5.5-4.8 l-1.5-12.4l0.5-3l3.9-1.3v3l5.2,3.3l8-1.5l2.1,2.1l5.7-3.8l0.01-0.09l2.09,2.99l-4.9,3.5l-1.6,8.6l5.2,6.7l-1.7,5.9l2.3,1.6 l-1.3,2.5l1.1,3l4.6,4.1l5.9,2.1l0.9,3l4.6,2.6h-0.1L499.3,365.9z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Loire", "data-department", "43", "d", "m485.4,376.3l2.2,2.4l-0.9,4.1v3.1l-2.6-1.2 l-1.5,5.2l-2.6,2.3l0.7,2.8l-3.4,0.4l-3.4,5.9l-5.9,1.9l-6.7,6.4l-9-7.7l-2.9-0.2l-0.1,2.8l-6.4-1.2l-1.5-5.7l-2.3-1.7l-3.5-8 l3.4-0.2l-2.6-1.1l-3.3-8.7l-5.5-1.5v-3.9v-0.1l9.6-3.2l8.5,0.1l5.2,3.2l11.1-0.7l2.8-0.8l2.3,2l2.8-0.4l6.2-3l6,2.2l0.7,5.5 L485.4,376.3z", 1, "departement", 3, "ngClass"], ["data-name", "Puy-de-Dome", "data-department", "63", "d", "m449.1,332.4l3.5,4.3l2.5,2.7l-2.1,6.3l4.7,7.7 l4.6,3.8l2.7,7.5l-3,7.1l-11.1,0.7l-5.2-3.2l-8.5-0.1l-9.6,3.2v0.1l-3.7,4.1l-6.5-5.4l-6.7-0.3l-1.5-2.5l-7.5-3.5l-1.2-7.9l1.7-2.4 L401,349l-4.4-5.5l9.3-8.6l-2.3-6.7l0.5-4.1l2.5,1.2l11.2-8l2.3,1.7l-1.2,2.8l4.4,4.5l3.3-0.5l1.6,2.5l13.1,0.5l1.4,3.2L449.1,332.4z", 1, "departement", 3, "ngClass"], ["data-name", "Rhone", "data-department", "69", "d", "m493.1,312.7l5.7,7.7l-1.5,8l3.1,9.2l2.8-0.5 l3.7,5.7l6.4-0.5l0.3,7.1l-2.5,5l-6.9,1.5l-2.5,2.5l1.9,2.5l-4.4,3.9l-4.6-2.6l-0.9-3l-5.9-2.1l-4.6-4.1l-1.1-3l1.3-2.5l-2.3-1.6 l1.7-5.9l-5.2-6.7l1.6-8.6l4.9-3.5l-2.09-2.99l0.29-2.91l2.3-1.9l2.2,1.7l2.2-1.6l2.5,1.5L493.1,312.7z", 1, "departement", 3, "ngClass"], ["data-name", "Savoie", "data-department", "73", "d", "m603.7,362l-1,10.3l-3.1,1.4l-2.2,0.7l-4.5,3.4 l-1.5,2.4l-2.5-1.4l-5.1,1.3l-2,1.8v0.1l-6.8,1.9l-2,2l-7.7-3.5l-5.2-1.5l-1-5.4l2.3-5.5l-2.7-6.3l-9.4-2.1l-0.3,2.8l-6.2-0.2 l-6.5-10.5l5.7-6.7l2.3-13.6l2.7,6.7l2.7,0.9l1.3,2.5l3,1.7l2.6-1.6l3.2,0.8l4.6,3.6l9.4-13.9l2.4,1.6l-0.6,3l2.3,1.8l6.2,2.3 l2.2-1.5l0.62-0.76l1.88,4.66l2.7,1.1l1.5,1.9l2.8,0.4l-0.7,3l1.3,5.2l5.1,4L603.7,362z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Savoie", "data-department", "74", "d", "m547,340.1l-2.7-6.7l-0.8-9.3h2.9l4.36-4.71 l2.24,0.91l2.3-1l2.3,0.1l3.4-3.5l2.1-1l1-2.3l-2.8-1.3l1.8-5.1l2.4-0.8l2.3,1l3.6-2.9l9.5-1.3l3.2,0.6l-0.5,2.7l4.2,4.1l-2.1,6.4 l-0.6,1.5l4.6,1.7l-0.1,4.8l2-1.4l4.6,6.6l-1.3,5l-2.5,1.7l-4.9,0.9l-0.6,3.7l0.02,0.04l-0.62,0.76l-2.2,1.5l-6.2-2.3l-2.3-1.8 l0.6-3l-2.4-1.6l-9.4,13.9l-4.6-3.6l-3.2-0.8l-2.6,1.6l-3-1.7l-1.3-2.5L547,340.1z", 1, "departement", 3, "ngClass"], ["data-name", "Provence-Alpes-Cote d'Azur", "data-code_insee", "93", 1, "region"], ["data-name", "Alpes-de-Haute-Provence", "data-department", "04", "d", "m596.5,409.9l0.57-0.5l-0.37,4.5l-2.2,1.5 l-0.6,2.9l3.5,4l-1.8,4.8l0.19,0.21L589,435.1l-2,5.3l4.3,8.5l7,7.7l-5.2-0.6l-5.2,3.8l1.2,2.6l-3,1.4l-9.8,0.4l-1.2,3.5l-5.9-3.6 l-10.1,8.5l-4-4.8l-2.7,1.8l-5.3-0.2l-6.1-6l-3.4-1.1l1.7-2.5l-3.7-5.2l1.2-3l-2.2-5.4l4.3-4.8l2.3-0.1l1-0.2l5.9-1.4l3.8,1 l-3.4-4.9l3.9,1.1l1.4-8.6l5.3-4l3.3-0.7l3.5,4.5l0.7-3.8l3.8-4.2l11.1,3.3l9-10.2L596.5,409.9z", 1, "departement", 3, "ngClass"], ["data-name", "Hautes-Alpes", "data-department", "05", "d", "m597.1,409l-0.03,0.4l-0.57,0.5l-6,3.3l-9,10.2 l-11.1-3.3l-3.8,4.2l-0.7,3.8l-3.5-4.5l-3.3,0.7l-5.3,4l-1.4,8.6l-3.9-1.1l3.4,4.9l-3.8-1l-5.9,1.4l0.2-4.7l-10-5.7l-1.5-2.6 l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3l2.5-6.7l5.8-0.3l0.3-3.4l1-3.1l5.5-1.2l0.9-2.8l12.6-3.9l3.1,1.1l0.4-5.8l-4.4-3.1l-1.8-4.2 l1.6-3.8l7.7,3.5l2-2l6.8-1.9l1.8,4.5l2.4,0.6l1.1,2l0.4,3l1.2,2.2l3,2.3l5.7,0.5l2.2,1.3l-0.7,2.1l3.2,4.7l-3,1.5L597.1,409z", 1, "departement", 3, "ngClass"], ["data-name", "Alpes-Maritimes", "data-department", "06", "d", "m605.3,477.1l-3.2-0.1l-1.3,1.8l-0.1,2.2 l-0.42,0.77l-2.18-3.97l0.8-2.9l-5.6-2.6l-1.7-5.6l-5.5-2.9l3-1.4l-1.2-2.6l5.2-3.8l5.2,0.6l-7-7.7l-4.3-8.5l2-5.3l6.79-7.79 l6.91,7.79l6.9,1.6l4.2,2.8l2.5-0.4l1.8,1.4l10.3-2.4l2.7-1.8l-0.3,2.6l1.5,2.2l0.3,3.2l-1.6,1.9l-0.2,2.3l-2.7,1.6l-3.3,5l-0.5,1.6 l1.1,2.7l-1.1,2.7l-3.5,2.9l-2.3,0.5l-0.9,2.4l-3-0.9l-1.5,2.1l-2.3,0.5L609,472l0.1,2.8l-2.4,0.6L605.3,477.1z", 1, "departement", 3, "ngClass"], ["data-name", "Bouches-du-Rhone", "data-department", "13", "d", "m545,500.2l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5 l-5.5-9.1l2-5.3l3.3-0.8l-1.9-3.8l-0.1-0.1l-6.6,4.3l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l-3.9,4.1l-1.9,10.6 l-3.3-0.9l-4.2,4.8l1,2.7l-5.8,1.8l-3.1,4.9l0.2,0.1h13.2l2.2,0.9l1,2.2l-1.6,1.5l2.2,1.4l7.4,0.1l3.2,1.3l1.8-1.7l-1.5-2.8l0.4-2.4 l4.9,1l3,5.3l10-0.8l2.6-1.1l1.8,2l-0.2,2.5l1,2l-1.2,2.2h9.2l1.3,2l2.2-0.8l1.7,0.2L545,500.2z", 1, "departement", 3, "ngClass"], ["data-name", "Var", "data-department", "83", "d", "m600.28,481.77l-1.38,2.53l-6.8,1.7l-0.7,2.5 l-5.5,5.7l5,0.7l-2,4.8l-4,0.2l-4.8,2.5l-3.5,1.1l0.1,2.7l-4.9-1.5l-2.7,0.5l-1.6,1.6l-0.4,2.3l-2.2,1.6l1.4-1.8l-2.4-1.7l-2.2,0.7 l-1.6-1.6l-3.1,0.1l0.9,2.2l-2.3-0.4l-1.5,1.7l-3-1.1l0.6-2.3l-6.4-4.1l-0.5-0.1l0.2-2.1l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5l-5.5-9.1 l2-5.3l3.3-0.8l-1.9-3.8l0.1-0.4l5.3,0.2l2.7-1.8l4,4.8l10.1-8.5l5.9,3.6l1.2-3.5l9.8-0.4l5.5,2.9l1.7,5.6l5.6,2.6l-0.8,2.9 L600.28,481.77z", 1, "departement", 3, "ngClass"], ["data-name", "Vaucluse", "data-department", "84", "d", "m541,463.4l6.1,6l-0.1,0.4l-0.1-0.1l-6.6,4.3 l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l4.5-5l-6.3-6.4l-0.2-5.5l-2.6-4.4l-0.1-3.7l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l2.2,5.4l-1.2,3l3.7,5.2l-1.7,2.5L541,463.4z", 1, "departement", 3, "ngClass"], ["data-name", "Corse", "data-department", "20", "data-code_insee", "94", 1, "region"], ["data-name", "Corse-du-Sud", "data-department", "2A", "d", "m640.5,554.2l3.2-1.7l0.7,8.4l-0.15,0.54 l-1.85,4.86l-2.7,1.9l3.3,0.4l-5.8,14.7l-3.1-1.2l-1.2-2.8l-11.2-3.4l-4.8-4.4l0.2-3l4.9-3.3l-9.5-1.9l2.7-7l-0.9-5.8l-7.3,2.6 l3-8.4l2.6-1.6l-7.9-4.4l-1.1-5.5l5.3-3.8l-3.8-4.2l-2.6,1l0.5-2.7l13.6,2.1l1.2,3.5l6,3.4l6,5.9l0.5,3.2l2.7,1.1l3.7,11 L640.5,554.2z", 1, "departement", 3, "ngClass"], ["data-name", "Haute-Corse", "data-department", "2B", "d", "m643.7,551.5v1l-3.2,1.7l-3.8-0.5l-3.7-11 l-2.7-1.1l-0.5-3.2l-6-5.9l-6-3.4l-1.2-3.5l-13.6-2.1v-0.2l3.9-5l-0.3-3.4l2.2-2.8l2.8-0.3l0.9-2.9l10.7-4.2l3.5-4.9l8.6,1.3 l-0.5-17.4l2.4-2l2.9,1.1l0.18,0.89l1.52,8.21l-0.5,10.6l4,5.6l3.8,26l-5.4,11.9V551.5L643.7,551.5z", 1, "departement", 3, "ngClass"], ["yPosition", "above"], ["aboveMenu", "matMenu"], ["mat-menu-item", ""]], template: function MapFrenchComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "g", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "path", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "g", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "path", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "g", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "path", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "g", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "path", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "g", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "path", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "g", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "path", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "path", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "path", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "path", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "path", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "path", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "path", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "g", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "path", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "path", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "path", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "path", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "g", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "path", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "path", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "path", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "path", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "path", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "path", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "path", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "g", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "path", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "path", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "path", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "path", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "path", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "g", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "path", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "path", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "path", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "path", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "g", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "path", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "path", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "path", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "path", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "path", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "path", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "path", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "path", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "path", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "path", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "g", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "path", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "path", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "path", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "path", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "path", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "g", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "path", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "path", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "path", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "path", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "g", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "path", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "path", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "path", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "path", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "path", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](78, "path", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "path", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "path", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "path", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "path", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "path", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "path", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "g", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "path", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "path", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "path", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](89, "path", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "path", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "path", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "path", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "path", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "path", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "path", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "path", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "path", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "path", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "g", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "path", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "path", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "path", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "path", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "path", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "path", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "path", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "path", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "path", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "path", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "path", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "path", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "g", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "path", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "path", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "path", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "path", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "path", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "path", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "g", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "path", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "path", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "mat-menu", 122, 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "button", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Item 1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "button", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Item 2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](101, _c0, "01" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](103, _c0, "02" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](105, _c0, "03" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](107, _c0, "04" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](109, _c0, "05" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](111, _c0, "75" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](113, _c0, "77" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](115, _c0, "78" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](117, _c0, "91" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](119, _c0, "92" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](121, _c0, "93" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](123, _c0, "94" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](125, _c0, "95" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](127, _c0, "18" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](129, _c0, "28" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](131, _c0, "36" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](133, _c0, "37" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](135, _c0, "41" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](137, _c0, "45" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](139, _c0, "21" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](141, _c0, "25" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](143, _c0, "39" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](145, _c0, "58" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](147, _c0, "70" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](149, _c0, "71" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](151, _c0, "89" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](153, _c0, "90" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](155, _c0, "14" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](157, _c0, "27" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](159, _c0, "50" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](161, _c0, "61" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](163, _c0, "76" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](165, _c0, "02" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](167, _c0, "59" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](169, _c0, "60" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](171, _c0, "62" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](173, _c0, "80" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](175, _c0, "08" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](177, _c0, "10" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](179, _c0, "51" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](181, _c0, "52" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](183, _c0, "54" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](185, _c0, "55" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](187, _c0, "57" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](189, _c0, "67" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](191, _c0, "68" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](193, _c0, "88" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](195, _c0, "44" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](197, _c0, "49" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](199, _c0, "53" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](201, _c0, "72" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](203, _c0, "85" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](205, _c0, "22" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](207, _c0, "29" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](209, _c0, "35" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](211, _c0, "56" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](213, _c0, "16" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](215, _c0, "17" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](217, _c0, "19" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](219, _c0, "23" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](221, _c0, "24" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](223, _c0, "33" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](225, _c0, "40" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](227, _c0, "47" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](229, _c0, "64" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](231, _c0, "97" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](233, _c0, "86" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](235, _c0, "87" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](237, _c0, "09" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](239, _c0, "11" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](241, _c0, "11" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](243, _c0, "30" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](245, _c0, "31" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](247, _c0, "32" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](249, _c0, "34" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](251, _c0, "46" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](253, _c0, "48" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](255, _c0, "65" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](257, _c0, "66" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](259, _c0, "81" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](261, _c0, "82" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](263, _c0, "01" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](265, _c0, "03" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](267, _c0, "07" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](269, _c0, "15" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](271, _c0, "26" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](273, _c0, "38" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](275, _c0, "42" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](277, _c0, "43" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](279, _c0, "63" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](281, _c0, "69" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](283, _c0, "73" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](285, _c0, "74" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](287, _c0, "04" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](289, _c0, "05" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](291, _c0, "06" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](293, _c0, "13" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](295, _c0, "83" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](297, _c0, "84" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](299, _c0, "2A" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](301, _c0, "2B" === ctx.dept));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["_MatMenu"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuItem"]], styles: [".mapfrench_container[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_title[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 550px;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\r\n  fill: #7f8076;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n  transition: 0.3s;\r\n  position: relative;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path.selected[_ngcontent-%COMP%] {\r\n  fill: red;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]:hover   path[_ngcontent-%COMP%] {\r\n  fill: blue;\r\n\r\n}\r\npolygon[_ngcontent-%COMP%]{\r\n  -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n          clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]{\r\n  outline: solid 1px rgb(182, 182, 182);\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]   text[_ngcontent-%COMP%]{\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\r\n  fill: red;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   .choix[_ngcontent-%COMP%] {\r\n  fill: red;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFNBQVM7QUFDWDtBQUNBO0VBQ0UsVUFBVTs7QUFFWjtBQUNBO0VBQ0Usd0ZBQWdGO1VBQWhGLGdGQUFnRjs7QUFFbEY7QUFDQTtFQUNFLHFDQUFxQzs7RUFFckMsYUFBYTtBQUNmO0FBQ0E7O0VBRUUsYUFBYTtBQUNmO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCIiwiZmlsZSI6InNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwZnJlbmNoX2NvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX3RpdGxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xyXG59XHJcblxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aCB7XHJcbiAgZmlsbDogIzdmODA3NjtcclxuICBzdHJva2U6IHdoaXRlO1xyXG4gIHN0cm9rZS13aWR0aDogMXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aC5zZWxlY3RlZCB7XHJcbiAgZmlsbDogcmVkO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZzpob3ZlciBwYXRoIHtcclxuICBmaWxsOiBibHVlO1xyXG5cclxufVxyXG5wb2x5Z29ue1xyXG4gIGNsaXAtcGF0aDogcG9seWdvbigwJSAwJSwgMTAwJSAwJSwgMTAwJSA3NSUsIDc1JSA3NSUsIDY0JSAxMDAlLCA1MCUgNzUlLCAwJSA3NSUpO1xyXG5cclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IC5wb3B1cHtcclxuICBvdXRsaW5lOiBzb2xpZCAxcHggcmdiKDE4MiwgMTgyLCAxODIpO1xyXG5cclxuICBmaWxsOiAjMDAwMGZmO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgLnBvcHVwIHRleHR7XHJcblxyXG4gIGZpbGw6ICMwMDAwZmY7XHJcbn1cclxuXHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyBwYXRoOmhvdmVyIHtcclxuICBmaWxsOiByZWQ7XHJcbiAgc3Ryb2tlOiB3aGl0ZTtcclxuICBzdHJva2Utd2lkdGg6IDFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyAgLmNob2l4IHtcclxuICBmaWxsOiByZWQ7XHJcbiAgc3Ryb2tlOiB3aGl0ZTtcclxuICBzdHJva2Utd2lkdGg6IDFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapFrenchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-map-french',
                templateUrl: './map-french.component.html',
                styleUrls: ['./map-french.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/material-module.ts":
/*!************************************!*\
  !*** ./src/app/material-module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js");
/* harmony import */ var _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/clipboard */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/clipboard.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/drag-drop.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/autocomplete.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/badge.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/bottom-sheet.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button-toggle.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/chips.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/expansion.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/grid-list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/paginator.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-spinner.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sidenav.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slider.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slide-toggle.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/snack-bar.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/toolbar.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/overlay.js");













































class MaterialModule {
}
MaterialModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: MaterialModule });
MaterialModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialModule, { exports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaterialModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                exports: [
                    _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
                    _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
                    _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
                    _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
                    _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
                    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
                    _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
                    _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
                    _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                    _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
                    _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
                    _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
                    _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
                    _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
                    _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
                    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
                    _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
                    _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
                    _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
                    _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
                    _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
                    _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
                    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
                    _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
                    _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
                    _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
                    _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
                    _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
                    _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
                    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
                    _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
                    _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
                    _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
                    _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"],
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
}
NotFoundComponent.ɵfac = function NotFoundComponent_Factory(t) { return new (t || NotFoundComponent)(); };
NotFoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NotFoundComponent, selectors: [["app-not-found"]], decls: 11, vars: 0, consts: [[1, "container"], [1, "boo-wrapper"], [1, "boo"], [1, "face"], [1, "shadow"]], template: function NotFoundComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Whoops!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " We couldn't find the page you ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " were looking for. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["@keyframes floating {\r\n    0% {\r\n        transform: translate3d(0, 0, 0);\r\n   }\r\n    45% {\r\n        transform: translate3d(0, -10%, 0);\r\n   }\r\n    55% {\r\n        transform: translate3d(0, -10%, 0);\r\n   }\r\n    100% {\r\n        transform: translate3d(0, 0, 0);\r\n   }\r\n}\r\n@keyframes floatingShadow {\r\n    0% {\r\n        transform: scale(1);\r\n   }\r\n    45% {\r\n        transform: scale(0.85);\r\n   }\r\n    55% {\r\n        transform: scale(0.85);\r\n   }\r\n    100% {\r\n        transform: scale(1);\r\n   }\r\n}\r\nbody[_ngcontent-%COMP%] {\r\n    background-color: #f7f7f7;\r\n}\r\n.container[_ngcontent-%COMP%] {\r\n    font-family: 'Varela Round', sans-serif;\r\n    color: #9b9b9b;\r\n    position: relative;\r\n    height: 100vh;\r\n    text-align: center;\r\n    font-size: 16px;\r\n}\r\n.container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\r\n    font-size: 32px;\r\n    margin-top: 32px;\r\n}\r\n.boo-wrapper[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform: translate(-50%, -50%);\r\n    paddig-top: 64px;\r\n    paddig-bottom: 64px;\r\n}\r\n.boo[_ngcontent-%COMP%] {\r\n    width: 160px;\r\n    height: 184px;\r\n    background-color: #f7f7f7;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    border: 3.3939393939px solid #9b9b9b;\r\n    border-bottom: 0;\r\n    overflow: hidden;\r\n    border-radius: 80px 80px 0 0;\r\n    box-shadow: -16px 0 0 2px rgba(234, 234, 234, .5) inset;\r\n    position: relative;\r\n    padding-bottom: 32px;\r\n    animation: floating 3s ease-in-out infinite;\r\n}\r\n.boo[_ngcontent-%COMP%]::after {\r\n    content: '';\r\n    display: block;\r\n    position: absolute;\r\n    left: -18.8235294118px;\r\n    bottom: -8.3116883117px;\r\n    width: calc(100% + 32px);\r\n    height: 32px;\r\n    background-repeat: repeat-x;\r\n    background-size: 32px 32px;\r\n    background-position: left bottom;\r\n    background-image: linear-gradient(-45deg, #f7f7f7 16px, transparent 0), linear-gradient(45deg, #f7f7f7 16px, transparent 0), linear-gradient(-45deg, #9b9b9b 18.8235294118px, transparent 0), linear-gradient(45deg, #9b9b9b 18.8235294118px, transparent 0);\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%] {\r\n    width: 24px;\r\n    height: 3.2px;\r\n    border-radius: 5px;\r\n    background-color: #9b9b9b;\r\n    position: absolute;\r\n    left: 50%;\r\n    bottom: 56px;\r\n    transform: translateX(-50%);\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before, .boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\r\n    content: '';\r\n    display: block;\r\n    width: 6px;\r\n    height: 6px;\r\n    background-color: #9b9b9b;\r\n    border-radius: 50%;\r\n    position: absolute;\r\n    bottom: 40px;\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before {\r\n    left: -24px;\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\r\n    right: -24px;\r\n}\r\n.shadow[_ngcontent-%COMP%] {\r\n    width: 128px;\r\n    height: 16px;\r\n    background-color: rgba(234, 234, 234, .75);\r\n    margin-top: 40px;\r\n    margin-right: auto;\r\n    margin-left: auto;\r\n    border-radius: 50%;\r\n    animation: floatingShadow 3s ease-in-out infinite;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7UUFDSSwrQkFBK0I7R0FDcEM7SUFDQztRQUNJLGtDQUFrQztHQUN2QztJQUNDO1FBQ0ksa0NBQWtDO0dBQ3ZDO0lBQ0M7UUFDSSwrQkFBK0I7R0FDcEM7QUFDSDtBQUNBO0lBQ0k7UUFDSSxtQkFBbUI7R0FDeEI7SUFDQztRQUNJLHNCQUFzQjtHQUMzQjtJQUNDO1FBQ0ksc0JBQXNCO0dBQzNCO0lBQ0M7UUFDSSxtQkFBbUI7R0FDeEI7QUFDSDtBQUNBO0lBQ0kseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSx1Q0FBdUM7SUFDdkMsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0M7SUFDaEMsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixvQ0FBb0M7SUFDcEMsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQiw0QkFBNEI7SUFDNUIsdURBQXVEO0lBQ3ZELGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsMkNBQTJDO0FBQy9DO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixZQUFZO0lBQ1osMkJBQTJCO0lBQzNCLDBCQUEwQjtJQUMxQixnQ0FBZ0M7SUFDaEMsNFBBQTRQO0FBQ2hRO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0lBQ1osMkJBQTJCO0FBQy9CO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLFVBQVU7SUFDVixXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLDBDQUEwQztJQUMxQyxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaURBQWlEO0FBQ3JEIiwiZmlsZSI6InNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGtleWZyYW1lcyBmbG9hdGluZyB7XHJcbiAgICAwJSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcclxuICAgfVxyXG4gICAgNDUlIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC0xMCUsIDApO1xyXG4gICB9XHJcbiAgICA1NSUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTEwJSwgMCk7XHJcbiAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XHJcbiAgIH1cclxufVxyXG5Aa2V5ZnJhbWVzIGZsb2F0aW5nU2hhZG93IHtcclxuICAgIDAlIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICB9XHJcbiAgICA0NSUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XHJcbiAgIH1cclxuICAgIDU1JSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwLjg1KTtcclxuICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgfVxyXG59XHJcbmJvZHkge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcclxufVxyXG4uY29udGFpbmVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnVmFyZWxhIFJvdW5kJywgc2Fucy1zZXJpZjtcclxuICAgIGNvbG9yOiAjOWI5YjliO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG4uY29udGFpbmVyIGgxIHtcclxuICAgIGZvbnQtc2l6ZTogMzJweDtcclxuICAgIG1hcmdpbi10b3A6IDMycHg7XHJcbn1cclxuLmJvby13cmFwcGVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIHBhZGRpZy10b3A6IDY0cHg7XHJcbiAgICBwYWRkaWctYm90dG9tOiA2NHB4O1xyXG59XHJcbi5ib28ge1xyXG4gICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgaGVpZ2h0OiAxODRweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIGJvcmRlcjogMy4zOTM5MzkzOTM5cHggc29saWQgIzliOWI5YjtcclxuICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogODBweCA4MHB4IDAgMDtcclxuICAgIGJveC1zaGFkb3c6IC0xNnB4IDAgMCAycHggcmdiYSgyMzQsIDIzNCwgMjM0LCAuNSkgaW5zZXQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMzJweDtcclxuICAgIGFuaW1hdGlvbjogZmxvYXRpbmcgM3MgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbn1cclxuLmJvbzo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IC0xOC44MjM1Mjk0MTE4cHg7XHJcbiAgICBib3R0b206IC04LjMxMTY4ODMxMTdweDtcclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgKyAzMnB4KTtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMzJweCAzMnB4O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCBib3R0b207XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZjdmN2Y3IDE2cHgsIHRyYW5zcGFyZW50IDApLCBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmN2Y3ZjcgMTZweCwgdHJhbnNwYXJlbnQgMCksIGxpbmVhci1ncmFkaWVudCgtNDVkZWcsICM5YjliOWIgMTguODIzNTI5NDExOHB4LCB0cmFuc3BhcmVudCAwKSwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOWI5YjliIDE4LjgyMzUyOTQxMThweCwgdHJhbnNwYXJlbnQgMCk7XHJcbn1cclxuLmJvbyAuZmFjZSB7XHJcbiAgICB3aWR0aDogMjRweDtcclxuICAgIGhlaWdodDogMy4ycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOWI5YjliO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgYm90dG9tOiA1NnB4O1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xyXG59XHJcbi5ib28gLmZhY2U6OmJlZm9yZSwgLmJvbyAuZmFjZTo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiA2cHg7XHJcbiAgICBoZWlnaHQ6IDZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5YjliOWI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDQwcHg7XHJcbn1cclxuLmJvbyAuZmFjZTo6YmVmb3JlIHtcclxuICAgIGxlZnQ6IC0yNHB4O1xyXG59XHJcbi5ib28gLmZhY2U6OmFmdGVyIHtcclxuICAgIHJpZ2h0OiAtMjRweDtcclxufVxyXG4uc2hhZG93IHtcclxuICAgIHdpZHRoOiAxMjhweDtcclxuICAgIGhlaWdodDogMTZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjM0LCAyMzQsIDIzNCwgLjc1KTtcclxuICAgIG1hcmdpbi10b3A6IDQwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGFuaW1hdGlvbjogZmxvYXRpbmdTaGFkb3cgM3MgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotFoundComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-not-found',
                templateUrl: './not-found.component.html',
                styleUrls: ['./not-found.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/security/token-interceptor.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/security/token-interceptor.service.ts ***!
  \*******************************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");



;
class TokenInterceptorService {
    constructor(token) {
        this.token = token;
    }
    intercept(request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.token.getToken()}`
            }
        });
        return next.handle(request);
    }
}
TokenInterceptorService.ɵfac = function TokenInterceptorService_Factory(t) { return new (t || TokenInterceptorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
TokenInterceptorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenInterceptorService, factory: TokenInterceptorService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenInterceptorService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/test-egibilite.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/cpn/test-egibilite.service.ts ***!
  \********************************************************/
/*! exports provided: TestEgibiliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestEgibiliteService", function() { return TestEgibiliteService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class TestEgibiliteService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    /*********************** test/activities/get ****************************/
    getActivites() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/activities/get");
    }
    /*********************** test/transitions/get ****************************/
    getTransitions() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/transitions/get");
    }
    /*********************** test/grants/region/get ****************************/
    regionalGrant(region, budget, naf) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/region/" + region + "/" + budget + "/" + naf);
    }
    /*********************** test/grants/cpn/get ****************************/
    cpnGrant(service, budget) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/cpn/" + service + "/" + budget);
    }
    /*********************** test/events/get ****************************/
    getEvents() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/get");
    }
    /*********************** test/events/add ****************************/
    addEvents(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/add", form);
    }
    /*********************** test/service/turnover ****************************/
    getServiceTurnover(range) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/service/turnover/" + range[0] + "/" + range[1], { withCredentials: false });
    }
    /*********************** test/company/siren ****************************/
    getCompanySiren(siret) {
        console.log('siret', siret);
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/company/siren/" + siret, { withCredentials: false });
    }
    /*********************** test/contact/save ****************************/
    addContact(form) {
        console.log('conatct', form);
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/save", form, { withCredentials: false });
    }
    /*********************** test/contact/confirm ****************************/
    contactConfirm(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/confirm", form, { withCredentials: false });
    }
    /*********************** test/zoom/generate ****************************/
    addZoom(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/zoom/generate", form, { withCredentials: false });
    }
    /*********************** test/timer/save ****************************/
    addTimer(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/timer/save", form, { withCredentials: false });
    }
}
TestEgibiliteService.ɵfac = function TestEgibiliteService_Factory(t) { return new (t || TestEgibiliteService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
TestEgibiliteService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: TestEgibiliteService, factory: TestEgibiliteService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](TestEgibiliteService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/token-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/token-storage.service.ts ***!
  \***************************************************/
/*! exports provided: TokenStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenStorageService", function() { return TokenStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const projet = 'projet';
class TokenStorageService {
    constructor() { }
    signOut() {
        window.sessionStorage.clear();
    }
    saveToken(token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }
    getToken() {
        return window.sessionStorage.getItem(TOKEN_KEY);
    }
    getUSERKEY() {
        return window.sessionStorage.getItem(USER_KEY);
    }
    saveUser(user) {
        window.sessionStorage.removeItem(USER_KEY);
        window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    }
    saveProjectId(id) {
        window.sessionStorage.setItem(projet, id);
    }
    getProjectId() {
        window.sessionStorage.getItem(projet);
    }
    getUser() {
        if (window.sessionStorage.getItem(USER_KEY)) {
            const user = window.sessionStorage.getItem(USER_KEY);
            if (user) {
                return user;
            }
        }
        return false;
    }
}
TokenStorageService.ɵfac = function TokenStorageService_Factory(t) { return new (t || TokenStorageService)(); };
TokenStorageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenStorageService, factory: TokenStorageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenStorageService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/test/test.component.ts":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/cpn/test-egibilite.service */ "./src/app/services/cpn/test-egibilite.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");











function TestComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Testez votre \u00E9ligibilit\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_3_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r22.checkForm(ctx_r22.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Commencer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-map-french");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choisissez votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre secteur d'activit\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { butttonREd: a0 }; };
function TestComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Status juridique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r24.getstatus("SARL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "SARL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r26.getstatus("SAS"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "SAS");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r27.getstatus("SASU"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "SASU");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r28.getstatus("EURL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "EURL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r29.getstatus("MICRO-ENT"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "MICRO-ENT");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SARL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SAS"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SASU"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](11, _c0, ctx_r3.testEgibFormGroup.get("status").value === "EURL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx_r3.testEgibFormGroup.get("status").value === "MICRO-ENT"));
} }
function TestComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nom de votre entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Baisse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 61, 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r31.incTurn(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r33.decTurn(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Augmentation");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "input", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "input", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "label", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Etat Stable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_9_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r35 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r35);
} }
function TestComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Avez vous d\u00E9ja obtenu des aides de l'\u00E9tat");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_9_option_7_Template, 2, 1, "option", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.test.data[ctx_r6.test.active.step].options);
} }
function TestComponent_div_10_option_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r37 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r37);
} }
function TestComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Dernier chiffre d'affaires r\u00E9alis\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "datalist", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_10_option_11_Template, 2, 1, "option", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r7.test.data[7].labels);
} }
function TestComponent_div_11_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r39 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r39);
} }
function TestComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nombre de salari\u00E9s ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_11_option_7_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r8.test.data[ctx_r8.test.active.step].options);
} }
function TestComponent_div_12_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous un site internet pour votre entreprise ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre type de site internet ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "E-commerce");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vitrine");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-radio-button", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Market-place");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Nombre de Vente");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Nombre de Visite");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Nombre d'utilisateurs");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 102);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Lien du site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Ch\u00E9que num\u00E9rique et aide num\u00E9rique de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "L'agence qui a d\u00E9velopp\u00E9 votre site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Internet/Freelance");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_div_1_Template, 11, 0, "div", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_1_div_2_Template, 44, 0, "div", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_1_div_3_Template, 7, 0, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_1_div_4_Template, 9, 0, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_1_div_5_Template, 9, 0, "div", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 5);
} }
function TestComponent_div_12_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un CRM)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Sage");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter votre logistique interne ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour votre logistique ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un ERP)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Sage");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_2_div_1_Template, 11, 0, "div", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_div_2_Template, 11, 0, "div", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_2_div_3_Template, 11, 0, "div", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_2_div_4_Template, 11, 0, "div", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r41.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r41.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r41.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r41.test.active.subStep == 4);
} }
function TestComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_Template, 5, 4, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 2);
} }
function TestComponent_div_13_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_13_div_7_Template_input_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r54); const items_r52 = ctx.$implicit; const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r53.selectService(items_r52 == null ? null : items_r52.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const items_r52 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](items_r52 == null ? null : items_r52.name);
} }
function TestComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Service");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "input", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_13_div_7_Template, 9, 1, "div", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r10.transition);
} }
function TestComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Budget d'investissement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "300$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "400$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "500$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 140);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "600$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Num\u00E9ro de Siret");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ville");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Fiche de rensignement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Pr\u00E9nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "mail");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Num\u00E9ro de portable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 154);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Num\u00E9ro d'entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-radio-group", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-radio-button", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "G\u00E9rant");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-radio-button", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Associ\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-radio-button", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Dir\u00E9cteur");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-radio-button", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "autre");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que Num\u00E9rique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_18_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r56); const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r55.showResult("hhhh"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r15.test.result.cpn.amount, " \u20AC");
} }
function TestComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r58 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que commerce connect\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_19_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r58); const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r57.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r16.test.result.regional.amount, " $");
} }
function TestComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    const _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Malheureusement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "vous n'\u00EAtes pas \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u00E0 l'aide de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_20_Template_div_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r60); const ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r59.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Vos disponibilit\u00E9s");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "full-calendar", 173, 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-group", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-radio-button", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Entretien vid\u00E9o direct");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-radio-button", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Visite de courtoisie");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx_r18.calendarOption);
} }
function TestComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Un conseiller entrera en contact avec");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " vous dans 30min");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Suivez-nous");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "img", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 185);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { checkIcon: a0 }; };
function TestComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 187);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r63); const ctx_r62 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r62.prevStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 187);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_36_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r63); const ctx_r64 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r64.checkForm(ctx_r64.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "i", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](16, _c1, ctx_r20.test.active.step == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](18, _c1, ctx_r20.test.active.step == 2));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](20, _c1, ctx_r20.test.active.step == 3));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](22, _c1, ctx_r20.test.active.step == 4));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](24, _c1, ctx_r20.test.active.step == 5));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](26, _c1, ctx_r20.test.active.step == 6));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](28, _c1, ctx_r20.test.active.step == 7));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](30, _c1, ctx_r20.test.active.step == 8));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](32, _c1, ctx_r20.test.active.step == 9));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](34, _c1, ctx_r20.test.active.step == 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](36, _c1, ctx_r20.test.active.step == 11));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](38, _c1, ctx_r20.test.active.step == 12));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](40, _c1, ctx_r20.test.active.step == 13));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](42, _c1, ctx_r20.test.active.step == 14));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](44, _c1, ctx_r20.test.active.step == 15));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](46, _c1, ctx_r20.test.active.step == 16));
} }
class TestComponent {
    /***********************************life cycle *******************/
    constructor(_formBuilder, testService) {
        this._formBuilder = _formBuilder;
        this.testService = testService;
        this.i = 9;
        /**********************************************turnover ******************************************/
        this.turn = 1;
        /****************************************************** resultat de test **************************************/
        this.eleg = null;
        /*****************************************test step **************************************/
        this.test = {
            active: {
                step: 0,
                subStep: 1,
                subStepCat: 0,
                stepType: "form",
                popup: false,
                confirmed: false,
            },
            result: {
                isOpen: false,
                isLoading: false,
                isCpn: true,
                regional: {
                    id: null,
                    region: null,
                    eligible: false,
                    voucher: null,
                    amount: null,
                },
                cpn: {
                    id: null,
                    amount: null,
                    originalPrice: null,
                    sellPrice: null,
                },
            },
            zoom: {
                generating: false,
                generated: false,
            },
            orientations: [],
            data: [
                {
                    step: 0,
                    title: "Bienvenue"
                },
                {
                    step: 1,
                    title: "Renseigner le code postal",
                },
                {
                    step: 2,
                    title: "Nom de l'entreprise"
                },
                {
                    step: 3,
                    title: "Statut juridique"
                },
                {
                    step: 4,
                    title: "Secteur d'activité",
                    options: [],
                },
                {
                    step: 5,
                    title: "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
                    labels: [
                        "Baisse",
                        "",
                        "",
                        "",
                        "",
                        "-50%",
                        "",
                        "",
                        "",
                        "",
                        "Stable",
                        "",
                        "",
                        "",
                        "",
                        "50%",
                        "",
                        "",
                        "",
                        "",
                        "Hausse"
                    ],
                },
                {
                    step: 6,
                    title: "Avez vous déja obtenu des aides de l'état",
                    options: [
                        "Chéque numérique et aide numérique de votre région",
                        "Crédit d'impôt",
                        "Fond de solidarité",
                        "Chaumage partiel",
                        "Aucune aide",
                    ],
                },
                {
                    step: 7,
                    title: "Dernier chiffre d'affaires réalisé",
                    labels: [
                        "5k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "700k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "3.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "7.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "30m €",
                    ],
                    selectedRange: [0, 1],
                    range: [
                        5000,
                        50000,
                        100000,
                        200000,
                        300000,
                        400000,
                        500000,
                        600000,
                        700000,
                        800000,
                        900000,
                        1000000,
                        1500000,
                        2000000,
                        2500000,
                        3000000,
                        3500000,
                        4000000,
                        4500000,
                        5000000,
                        5500000,
                        6000000,
                        6500000,
                        7000000,
                        7500000,
                        8000000,
                        8500000,
                        9000000,
                        9500000,
                        10000000,
                        15000000,
                        20000000,
                        25000000,
                        30000000,
                    ],
                },
                {
                    step: 8,
                    title: "Nombre de salariés",
                    options: [
                        "de 0 à 5 Personnes",
                        "de 5 à 10 Personnes",
                        "de 10 à 20 Personnes",
                        "de 20 à 30 Personnes",
                        "de 30 à 40 Personnes",
                        "de 40 à 50 Personnes",
                        "plus de 50 Personnes",
                    ],
                },
                {
                    step: 9,
                    title: "Type de site",
                    website: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un site internet pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de site"
                        },
                        {
                            subStep: 3,
                            title: "Lien de site"
                        },
                        {
                            subStep: 4,
                            title: "Date de développement"
                        },
                        {
                            subStep: 5,
                            title: "L'agence qui a développé votre site"
                        }
                    ],
                    crm: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un crm pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de crm"
                        },
                        {
                            subStep: 3,
                            title: "Le crm a été développé"
                        },
                        {
                            subStep: 4,
                            title: "Quel type de ERP vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "Quel type de CRM vous utilisez",
                            options: [
                                "Zoho",
                                "Habspot",
                                "Microsoft Dynamics",
                                "SalesForce",
                                "Pipedrive",
                                "Agile",
                                "Axonaut",
                                "FreshSales",
                                "Teamleader",
                                "Facture",
                                "Crème",
                                "Suite",
                            ]
                        },
                        {
                            subStep: 6,
                            title: "Date de développement"
                        }
                    ]
                },
                {
                    step: 10,
                    title: "Quel projet est à subventionner pour votre transition numérique",
                    tabServices: null,
                    loading: false,
                    services: ["Services éligible", "Services suplémentaire"],
                    tabCategories: null,
                    categories: ["Tous", "Graphique", "Développement", "Montage", "Marketing"],
                    options: [],
                },
                {
                    step: 11,
                    title: "Budget d'investissement",
                    budget: 5,
                    min: 400,
                    target: 500,
                    max: 100000,
                },
                {
                    step: 12,
                    title: "Numéros d'identification",
                    loading: false,
                },
                {
                    step: 13,
                    title: "Adresse",
                },
                {
                    step: 14,
                    title: "Fiche de renseignement",
                    options: [
                        "Gérant",
                        "Directeur",
                        "Associé",
                        "Autre"
                    ],
                },
                {
                    step: 15,
                    title: "Vos disponibilités",
                },
                {
                    step: 16,
                    title: "Type de client",
                    items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
                    labels: [
                        "agressif",
                        "indécis",
                        "anxieux",
                        "économe",
                        "compréhensif",
                        "roi",
                    ]
                },
                {
                    step: 17,
                    title: "Merci pour votre temps",
                },
            ],
        };
        /*************************form  data contact************************/
        this.testEgibFormGroup = this._formBuilder.group({
            codeP: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            nomSoc: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            activite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            status: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            help: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            personneSal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            turnover: [50, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastTurnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            liensite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            datesite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siteVal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            dateCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeCRM: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeERP: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            crmDev: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            agence: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            budget: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            service: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siret: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{14}")]],
            siren: [''],
            naf: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            adresse: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            zipcode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            region: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            prenom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            departement: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phoneEntrep: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            post: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            contactID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            meetingType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        /*************************form data event *******************************/
        this.addEventForm = this._formBuilder.group({
            title: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            dateDebut: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            cid: this.testEgibFormGroup.value.contactID
        });
        /*************************************calandreir ***************************/
        this.calendarOption = {
            customButtons: {
                myCustomButton: {
                    text: 'custom!',
                    click: function () {
                        alert('clicked the custom button!');
                    }
                }
            },
            locale: "fr",
            initialView: 'dayGridMonth',
            //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
            weekends: true,
            editable: true,
            selectable: true,
            selectMirror: true,
            droppable: false,
            displayEventTime: true,
            disableDragging: false,
            timeZone: 'UTC',
            refetchResourcesOnNavigate: true,
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,dayGridWeek,dayGridDay'
            },
            dayMaxEvents: true,
            events: [],
            dateClick: this.handleDateClick,
        };
    }
    get f() { return this.addEventForm.controls; }
    ngOnInit() {
        this.getTransition();
    }
    /***************************************select date rendez vous *************************/
    /*Show Modal with Forn on dayClick Event*/
    handleDateClick() {
        console.log("dateselect");
    }
    sendRendvous() {
        this.testService.addEvents(this.addEventForm.value).subscribe(res => {
            console.log('event', res);
        });
    }
    /***********************************generate lien zoom ****************/
    generateZoomLink() {
        this.test.zoom.generating = true;
        this.test.zoom.generated = true;
        this.testService.addZoom({ cid: this.testEgibFormGroup.value.contactID, type: this.testEgibFormGroup.value.meetingType })
            .subscribe(response => {
            console.log('zoom', response);
            if (!response.data.error) {
                this.test.zoom.generating = false;
                this.test.zoom.generated = true;
                alert({ title: 'Zoom', message: response.data.message, type: 'success' });
                this.nextStep();
            }
            else {
                this.test.zoom.generating = false;
                this.test.zoom.generated = false;
                alert({ title: 'Zoom', message: response.data.message, type: 'error' });
            }
        });
    }
    /*********************** selection turnover ******************/
    selectedRange(val) {
        this.testEgibFormGroup.value.turnover = val;
    }
    /***********************select transition ******************/
    getTransition() {
        this.testService.getTransitions().subscribe(res => {
            this.transition = res === null || res === void 0 ? void 0 : res.data;
            console.log("transition", this.transition);
            //  this.elegible=res?.data.filter(data=>data.category==='Services')
            //  this.graphic=res?.data.filter(data=>data.category==='Graphique')
            // this.montage=res?.data.filter(data=>data.category==='Montage')
            // this.marketing=res?.data.filter(data=>data.category==='Marketing')
            // this.development=res?.data.filter(data=>data.category==='Développement')
            // console.log("elegi",this.elegible)
            // console.log("suplimet",this.development)
        });
    }
    /*************************************services ***************************************/
    selectService(val) {
        console.log('service', val);
        this.testEgibFormGroup.get('service').setValue(val);
    }
    incTurn() {
        this.turn++;
        this.testEgibFormGroup.get('turnover').setValue(this.turn);
    }
    decTurn() {
        this.turn--;
        this.testEgibFormGroup.get('turnover').setValue(this.turn);
    }
    /**********************************************Last turnover ******************************************/
    setLastTurnover(val) {
        const step = this.test.active.step;
        let range = [
            val[0],
            val[1]
        ];
        console.log('val', val);
        console.log('range', range);
        this.testService.getServiceTurnover(range).subscribe(response => {
            console.log('data', response);
            this.testEgibFormGroup.get('service').setValue(response.data.transition_id);
            this.testEgibFormGroup.get('lastTurnover').setValue(response.data.id);
            this.testEgibFormGroup.get('budget').setValue(Math.ceil(response.data.budget / 100) * 100);
            this.test.data[11].budget = Math.ceil(response.data.budget / 100);
            this.test.data[11].min = Math.ceil(response.data.budget_min / 100) * 100;
            this.test.data[11].target = Math.ceil(response.data.budget / 100) * 100;
            this.test.data[11].max = Math.ceil(response.data.budget_max / 100) * 100;
        });
    }
    /******************************************************** get naf with siret**********************************/
    getNafCompany(siret) {
        let siren = siret.substring(0, 9);
        this.testEgibFormGroup.get('siren').setValue(siren);
        this.test.data[this.test.active.step].loading = true;
        if (siren.length == 9 && siret.length == 14) {
            this.testService.getCompanySiren(this.testEgibFormGroup.value.siret).subscribe(response => {
                console.log('siren', response);
                this.test.data[this.test.active.step].loading = false;
                this.testEgibFormGroup.get('naf').setValue(response.ape);
                this.testEgibFormGroup.get('naf').setValue(response.ape);
            });
        }
    }
    /******************************************************** save client to database**********************************/
    setContactForm(formDatas) {
        this.testService.addContact({
            address: {
                advisorName: this.testEgibFormGroup.value.nomSoc,
                line: this.testEgibFormGroup.value.adresse,
                zipcode: this.testEgibFormGroup.value.zipcode,
                region: this.testEgibFormGroup.value.region,
                departement: this.testEgibFormGroup.value.departement,
                city: this.testEgibFormGroup.value.city,
                country: this.testEgibFormGroup.value.country
            },
            companies: {
                name: this.testEgibFormGroup.value.nomSoc,
                status: this.testEgibFormGroup.value.status,
                activity: this.testEgibFormGroup.value.activite,
                help: this.testEgibFormGroup.value.help,
                salaries: this.testEgibFormGroup.value.personneSal,
                siret: this.testEgibFormGroup.value.siret,
                siren: this.testEgibFormGroup.value.siren,
                naf: this.testEgibFormGroup.value.naf,
                phone: this.testEgibFormGroup.value.phoneEntrep,
                turnover: this.testEgibFormGroup.value.turnover,
                lastTurnover: this.testEgibFormGroup.value.lastTurnover
            },
            contacts: {
                firstName: this.testEgibFormGroup.value.nom,
                lastName: this.testEgibFormGroup.value.prenom,
                email: this.testEgibFormGroup.value.email,
                phone: this.testEgibFormGroup.value.phone,
                position: this.testEgibFormGroup.value.post,
                type: 3,
                comment: ''
            },
            development: {
                haveWebsite: this.testEgibFormGroup.value.haveSite,
                websiteType: this.testEgibFormGroup.value.typeSite,
                websiteValue: this.testEgibFormGroup.value.siteVal,
                websiteLink: this.testEgibFormGroup.value.liensite,
                websiteDate: "12/03/1995",
                haveCrm: this.testEgibFormGroup.value.haveCrm,
                crmType: this.testEgibFormGroup.value.typeCRM,
                crmDev: this.testEgibFormGroup.value.crmDev,
                crmName: this.testEgibFormGroup.value.nom,
                erpName: this.testEgibFormGroup.value.nom,
                crmDate: this.testEgibFormGroup.value.dateCrm,
                agencyName: this.testEgibFormGroup.value.agence
            },
            investment: {
                service: this.testEgibFormGroup.value.service,
                budget: this.testEgibFormGroup.value.budget,
                digitalTransitions: []
            },
            contactID: '',
            meetingType: ''
        })
            .subscribe(response => {
            if (!response.error) {
                this.testEgibFormGroup.get('contactID').setValue(response.cid);
            }
        });
    }
    /********************************************************status**********************************/
    getstatus(data) {
        console.log('activi', data);
        this.testEgibFormGroup.get('status').setValue(data);
    }
    /******************************************nextmodule  *************************************************/
    nextStep() {
        this.test.active.step += 1;
    }
    nextSubStep() {
        this.test.active.subStep += 1;
    }
    /******************************************prevmodule  *************************************************/
    prevStep() {
        this.test.active.step -= 1;
    }
    /**************************test elgible */
    elgiblTest() {
        this.eleg = null;
        this.nextStep();
    }
    /**********************is cpn **************/
    isCpn() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        console.log("service", this.testEgibFormGroup.value.service);
        console.log("budget", this.testEgibFormGroup.value.budget);
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        this.nextStep();
    }
    showResult() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        this.nextStep();
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        switch (this.test.result.isOpen) {
            case true:
                console.log("is open true");
                if (this.test.result.isCpn) {
                    console.log("is cpn true");
                    this.test.result.isCpn = false;
                    this.test.result.isLoading = true;
                    this.testService.regionalGrant(region, budget, naf)
                        .subscribe(response => {
                        console.log("regionalGrant", response);
                        this.eleg = response.eligible;
                        if (response.eligible) {
                            this.test.result.regional.id = response.id;
                            this.test.result.regional.eligible = response.eligible;
                            this.test.result.regional.voucher = response.voucher;
                            this.test.result.regional.amount = response.amount;
                            this.test.result.regional.region = response.region;
                            console.log("is eligible", this.test.active.step);
                        }
                        else {
                            this.test.result.regional.eligible = response.eligible;
                            this.test.result.regional.voucher = null;
                            this.test.result.regional.amount = null;
                            console.log("is not eligi", this.test.active.step);
                        }
                        this.test.result.isLoading = false;
                    });
                    /*  .catch(error=>{
                        this.test.result.isLoading = false;
                        console.log(error)
                      });*/
                }
                else {
                    console.log("is cpn false");
                    this.setContactForm(this.test.formData);
                    this.test.result.isCpn = true;
                    this.test.result.isOpen = false;
                    this.nextStep();
                }
                break;
        }
        console.log("test", this.test.result);
    }
    /******************************************************chekform **************************************/
    checkForm(step) {
        console.log('step', step);
        let subStep = this.test.active.subStep;
        let subStepCat = this.test.active.subStepCat;
        switch (step) {
            case 0:
                this.nextStep();
                break;
            case 15:
                if (this.testEgibFormGroup.value.codeP == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 2:
                if (this.testEgibFormGroup.value.activite == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 3:
                if (this.testEgibFormGroup.value.status == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 4:
                if (this.testEgibFormGroup.value.nomSoc == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 5:
                if (this.testEgibFormGroup.value.turnover == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 6:
                if (this.testEgibFormGroup.value.help == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 7:
                this.setLastTurnover([5000, 50000]);
                this.nextStep();
                break;
            case 8:
                if (this.testEgibFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.testEgibFormGroup.value.personneSal == "de 5 à 10 Personnes") {
                    this.test.active.subStepCat = 1;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                else {
                    this.test.active.subStepCat = 2;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                break;
            case 9:
                switch (subStepCat) {
                    case 1:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveSite == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.nextStep();
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextStep();
                                break;
                        }
                        break;
                    case 2:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveCrm == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.test.active.subStepCat = 1;
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextSubStep();
                                break;
                            case 6:
                                this.test.active.subStepCat = 1;
                                this.test.active.subStep = 1;
                                break;
                        }
                        break;
                }
                break;
            case 10:
                this.nextStep();
                break;
            case 11:
                if (this.testEgibFormGroup.value.budget == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 12:
                if (this.testEgibFormGroup.value.siret == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.getNafCompany(this.testEgibFormGroup.value.siret);
                    this.nextStep();
                }
                break;
            case 13:
                if (this.testEgibFormGroup.value.adresse == '' && this.testEgibFormGroup.value.zipcode == '' && this.testEgibFormGroup.value.region == ''
                    && this.testEgibFormGroup.value.city == '' && this.testEgibFormGroup.value.country == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 14:
                if (this.testEgibFormGroup.value.nom == '' && this.testEgibFormGroup.value.prenom == '' && this.testEgibFormGroup.value.email == ''
                    && this.testEgibFormGroup.value.phone == '' && this.testEgibFormGroup.value.phoneEntrep == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.isCpn();
                }
                break;
            case 15:
                this.nextStep();
                break;
            case 16:
                this.nextStep();
                break;
            case 1:
                this.generateZoomLink();
                if (!this.test.zoom.generated) {
                    alert("genrate zoom est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
        }
    }
}
TestComponent.ɵfac = function TestComponent_Factory(t) { return new (t || TestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_2__["TestEgibiliteService"])); };
TestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TestComponent, selectors: [["app-test"]], decls: 50, vars: 24, consts: [[1, "container"], [1, "body"], [3, "formGroup"], ["class", "slide0", 4, "ngIf"], ["class", "slide1", 4, "ngIf"], ["class", "slide2", 4, "ngIf"], ["class", "slide3", 4, "ngIf"], ["class", "slide4", 4, "ngIf"], ["class", "slide5", 4, "ngIf"], ["class", "slide6", 4, "ngIf"], ["class", "slide7", 4, "ngIf"], ["class", "slide8", 4, "ngIf"], [4, "ngIf"], ["class", "slide27", 4, "ngIf"], ["class", "slide18", 4, "ngIf"], ["class", "slide19", 4, "ngIf"], ["class", "slide20", 4, "ngIf"], ["class", "slide26", 4, "ngIf"], ["class", "slide21", 4, "ngIf"], ["class", "slide24", 4, "ngIf"], ["class", "slide25", 4, "ngIf"], [1, "footer"], [1, "left"], [3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", ""], ["class", "center", 4, "ngIf"], ["id", "eventModal", "tabindex", "-1", "role", "dialog", 1, "modal", "fade", "text-left"], [1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title", "align-center"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "row"], [1, "col-sm-12"], [1, "form-group"], ["placeholder", "cr\u00E9e un \u00E9v\u00E9nement", "type", "text", "formControlName", "title", 1, "titleinp", "form-control"], ["type", "datetime-local", "id", "meeting-time", "name", "meeting-time", "formControlName", "dateDebut", 1, "titleinp", "form-control"], ["dateDebut", ""], ["type", "submit", 1, "btn", "btn-primary"], [1, "slide0"], [1, "image"], ["src", "assets/cpnimages/test-egibilite/1.png", "height", "30%", "alt", ""], [1, "test"], ["mat-stroked-button", "", "color", "primary", 3, "click"], [1, "slide1"], ["type", "text", "formControlName", "codeP", "placeholder", "entrer votre code postal"], [1, "slide2"], ["src", "assets/cpnimages/test-egibilite/4.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "activite", "placeholder", "Choisir un activit\u00E9"], [1, "slide3"], ["src", "assets/cpnimages/test-egibilite/5.png", "height", "30%", "alt", ""], [1, "blockBtn"], [1, "sousBlock"], ["mat-stroked-button", "", 3, "ngClass", "click"], [1, "slide4"], ["src", "assets/cpnimages/test-egibilite/3.png", "height", "30%", "alt", ""], ["type", "text", "placeholder", "nom societe", "formControlName", "nomSoc"], [1, "slide5"], ["src", "assets/cpnimages/test-egibilite/6.png", "height", "30%", "alt", ""], [1, "qty"], ["type", "text", "formControlName", "turnover", 3, "readonly"], ["turnover", ""], [1, "qtyBtn"], ["type", "text", "value", "+50%"], ["mat-stroked-button", "", "color", "primary"], ["type", "radio", "formControlName", "turnover", "name", "turnover", "id", "turnover", "value", "0", 1, "form-check-input"], ["for", "inlineRadio1", 1, "form-check-label"], [1, "slide6"], ["src", "assets/cpnimages/test-egibilite/7.png", "alt", ""], ["name", "", "formControlName", "help"], ["value", "item", 4, "ngFor", "ngForOf"], ["value", "item"], [1, "slide7"], [1, "colum1"], ["src", "assets/cpnimages/test-egibilite/8.png", "height", "30%", "alt", ""], [1, "colum2"], [1, "__range", "__range-step"], ["step", "1", "min", "0", "max", "test.data[test.active.step].range.length-1", "formControlName", "lastTurnover", "type", "range", "list", "tickmarks", 1, "slider"], ["id", "tickmarks"], [1, "slide8"], ["src", "assets/cpnimages/test-egibilite/9.png", "height", "30%", "alt", ""], ["placeholder", "choisir un Nombre de salari\u00E9s", "formControlName", "personneSal"], [4, "ngFor", "ngForOf"], ["class", "slide9", 4, "ngIf"], ["class", "slide10", 4, "ngIf"], ["class", "slide11", 4, "ngIf"], ["class", "slide12", 4, "ngIf"], ["class", "slide13", 4, "ngIf"], [1, "slide9"], ["src", "assets/cpnimages/test-egibilite/10.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveSite"], ["value", "oui"], ["value", "non"], [1, "slide10"], ["src", "assets/cpnimages/test-egibilite/11.png", "height", "30%", "alt", ""], ["aria-label", "Select an option"], ["value", "1"], ["value", "2"], ["value", "3"], [1, "block"], ["type", "text", "value", "+10%"], ["type", "text", "value", "+1000%"], [1, "slide11"], ["src", "assets/cpnimages/test-egibilite/12.png", "height", "30%", "alt", ""], ["type", "text"], [1, "slide12"], ["src", "assets/cpnimages/test-egibilite/13.png", "alt", ""], ["name", "", "id", ""], ["value", ""], [1, "slide13"], ["class", "slide14", 4, "ngIf"], ["class", "slide15", 4, "ngIf"], ["class", "slide16", 4, "ngIf"], ["class", "slide17", 4, "ngIf"], [1, "slide14"], ["src", "assets/cpnimages/test-egibilite/17.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveCrm"], [1, "slide15"], ["src", "assets/cpnimages/test-egibilite/logo-sage.png", "height", "20px", "alt", ""], [1, "slide16"], ["src", "assets/cpnimages/test-egibilite/14.png", "height", "30%", "alt", ""], [1, "slide17"], [1, "slide27"], ["for", ""], ["type", "text", 1, "search"], [1, "contentTab"], ["class", "list-item", 4, "ngFor", "ngForOf"], [1, "list-item"], [1, "item-content"], [1, "block1"], [1, "fas", "fa-chess-rook"], [1, "block2"], [1, "block3"], ["type", "radio", "name", "service", "id", "service", 3, "click"], [1, "slide18"], ["name", "", "formControlName", "budget"], ["value", "300"], ["value", "400"], ["value", "500"], ["value", "600"], [1, "slide19"], ["src", "assets/cpnimages/test-egibilite/18.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "siret", "placeholder", "EX: 13168813881"], [1, "slide20"], ["src", "assets/cpnimages/test-egibilite/19.png", "alt", ""], ["type", "text", "formControlName", "adresse"], ["type", "text", "formControlName", "region"], ["type", "text", "formControlName", "zipcode"], [1, "slide26"], ["src", "assets/cpnimages/test-egibilite/20.png", "alt", ""], ["type", "text", "formControlName", "nom"], ["type", "text", "formControlName", "prenom"], ["type", "text", "formControlName", "email"], ["type", "text", "formControlName", "phone"], ["type", "text", "formControlName", "phoneEntrep"], ["aria-label", "Select an option", "formControlName", "post"], ["value", "G\u00E9rant"], ["value", "Associ\u00E9"], ["value", "Dir\u00E9cteur"], ["value", "autre"], [1, "slide21"], ["src", "assets/cpnimages/test-egibilite/21.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/24.png", "alt", ""], [1, "prix"], ["src", "assets/cpnimages/test-egibilite/22.png", "alt", ""], [1, "nextIcon", 3, "click"], ["src", "assets/cpnimages/test-egibilite/23.png", "alt", ""], [1, "faild"], ["src", "assets/cpnimages/test-egibilite/25.png", "alt", ""], [1, "slide24"], ["src", "assets/cpnimages/test-egibilite/27.png", "height", "30%", "alt", ""], [1, "calend"], ["data-toggle", "modal", "data-target", "#eventModal", 2, "width", "100%", 3, "options"], ["fullcalendar", ""], ["aria-label", "Select an option", "formControlName", "meetingType"], ["value", "Entretien vid\u00E9o direct"], ["value", "Visite de courtoisie"], [1, "slide25"], ["src", "assets/cpnimages/test-egibilite/28.png", "alt", ""], [1, "nextIcon"], [1, "socialMedia"], ["src", "assets/cpnimages/test-egibilite/icone-Facebook.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Instagram.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Linkedin.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-youtube.png", "alt", ""], [1, "center"], [3, "click"], [1, "far", "fa-chevron-left", "iconNex"], [1, "listP"], ["aria-hidden", "true", 1, "fas", "fa-circle", "point", 3, "ngClass"], [1, "far", "fa-chevron-right", "iconNex"]], template: function TestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_3_Template, 8, 0, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_4_Template, 7, 0, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_5_Template, 7, 0, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TestComponent_div_6_Template, 19, 15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_7_Template, 7, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TestComponent_div_8_Template, 32, 1, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_9_Template, 8, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_10_Template, 12, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_11_Template, 8, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, TestComponent_div_12_Template, 3, 2, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_13_Template, 8, 1, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, TestComponent_div_14_Template, 15, 0, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TestComponent_div_15_Template, 7, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TestComponent_div_16_Template, 17, 0, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestComponent_div_17_Template, 36, 0, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, TestComponent_div_18_Template, 24, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, TestComponent_div_19_Template, 24, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestComponent_div_20_Template, 12, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, TestComponent_div_21_Template, 14, 1, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, TestComponent_div_22_Template, 20, 0, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TestComponent_div_27_Template, 38, 48, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " Cr\u00E9er un \u00E9v\u00E9nement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "form", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function TestComponent_Template_form_ngSubmit_35_listener() { return ctx.sendRendvous(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Titre \u00E9v\u00E9nement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "input", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "select date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "input", 37, 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.testEgibFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step != 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.addEventForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_button__WEBPACK_IMPORTED_MODULE_5__["MatButton"], _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_6__["MapFrenchComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RangeValueAccessor"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__["MatRadioButton"], _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_8__["FullCalendarComponent"]], styles: [".container[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    width: 100%;\r\n    height: 100%;\r\n    max-width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n.body[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction:column;\r\n    justify-content: center;\r\n}\r\n.footer[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    max-height: 150px;\r\n    min-height: 150px;\r\n    height: 100%;\r\n    background-color:  #111D5Eff;\r\n    display: flex;\r\n    flex-direction: row;\r\n}\r\n.left[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    height: 100%;\r\n    align-items: center;\r\n    width: 10%;\r\n    margin-left: 10px;\r\n}\r\n.left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 50%;\r\n}\r\n.point[_ngcontent-%COMP%]{\r\n    font-size: 10px;\r\n    color:rgb(153, 153, 153);\r\n    margin-left: 10px;\r\n    \r\n}\r\ni[_ngcontent-%COMP%]{\r\n    color:rgb(153, 153, 153);\r\n}\r\ni[_ngcontent-%COMP%]:hover{\r\n    color: white;\r\n    cursor: pointer;\r\n}\r\n.iconNex[_ngcontent-%COMP%]{\r\n    font-size: 25px;\r\n    margin-top: 8px;\r\n    margin-left: 10px;\r\n}\r\n.checkIcon[_ngcontent-%COMP%]{\r\n    color: white;\r\n}\r\n.center[_ngcontent-%COMP%]{\r\n width: 100%;\r\n height: 100%;\r\n display: flex;\r\n flex-direction: row;\r\n justify-content: center;\r\n align-items: center;\r\n}\r\n\r\n.butttonREd[_ngcontent-%COMP%]{\r\n    background-color: rgb(206, 0, 0);\r\n    color: white;\r\n}\r\nbutton[_ngcontent-%COMP%]:hover{\r\n    background-color: rgb(145, 136, 136);\r\n    color: white;\r\n}\r\n\r\n.slide1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 70%;\r\n  height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n order: 1;\r\n display: flex;\r\n flex-direction: column;\r\n   justify-content: center;\r\n   align-items:center;\r\nwidth: 40%;\r\nheight: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n       }\r\n\r\n.slide0[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 52%;\r\n  height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n   flex-direction: column;\r\n   justify-content: center;\r\n   align-items: center;\r\n   width:40%;\r\n   height: 100%;\r\n   }\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n   }\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n    width: 150px;\r\n    height: 50px;\r\n    border-radius: 30px;\r\n    border-color: rgb(192, 3, 3);\r\n       }\r\n\r\n.slide2[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n      order: 1;\r\n      width: 55%;\r\n      height: 100%;\r\n    }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n       flex-direction: column;\r\n       justify-content: center;\r\n       align-items: flex-start;\r\n       width:40%;\r\n       height: 100%;\r\n       }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n       }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n           }\r\n\r\n.slide3[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 54%;\r\n        height: 100%;\r\n    }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center;\r\n        margin-top: 50px;\r\n        width:40%;\r\n        height: 70%;\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 50px;\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n           display: flex;\r\n           flex-direction: column;\r\n           justify-content: center;\r\n\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;\r\n \r\n         }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n        width: 310px;\r\n        height: 50px;\r\n        margin: 5px;\r\n        border-color: rgb(192, 3, 3);\r\n            }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n                width: 150px;\r\n                margin: 5px;\r\n                    }\r\n\r\n.slide4[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        margin-left: 50px;\r\n        order: 1;\r\n        width: 33%;\r\n        height: 100%;\r\n        display: flex;\r\n        z-index: 1;\r\n        margin-top: 55px;\r\n    }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide5[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        width: 50%;\r\n        height: 100%;\r\n        z-index: 1;\r\n        display: flex;\r\n        margin-top: 64px;\r\n    }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: center;\r\n        width:50%;\r\n        height: 100%;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 50px;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n           display: flex;\r\n           flex-direction: row;\r\n           justify-content: space-around;\r\n            width: 100%;\r\n            height: 20%;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: flex-start;\r\n            align-items: center; \r\n         }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;            \r\n            }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 80px;\r\n        height: 50px;\r\n        border: 4px solid rgb(212, 5, 5);\r\n        text-align: center;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        margin: 0 5px 0 0;\r\n        padding: 0;\r\n        display: flex;\r\n        align-items: center;\r\n        flex-direction: row;\r\n            }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n        order: 3;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: flex-start;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n            border: none;\r\n            border-bottom: 1px solid white;\r\n            margin-bottom: 1px;\r\n            width: 5px;\r\n            border-radius: 0px;\r\n            height: 25px;\r\n            color: white;\r\n            background-color: rgb(212, 5, 5);\r\n            display: flex;\r\n            flex-direction: column;\r\n            font-size: 20px;\r\n            justify-content: center;\r\n            align-items: center;\r\n            }\r\n\r\n.slide6[_ngcontent-%COMP%]{\r\n                display: flex;\r\n                flex-direction: row;\r\n                justify-content: space-between;\r\n                width: 100%;\r\n                height: 100%;\r\n            }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                display: flex;\r\n                flex-direction: column;\r\n                justify-content: center;\r\n                align-items:center;\r\n                width:53%;\r\n                height: 100%;\r\n            }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                height: 25%;\r\n                width: 15%;\r\n                    }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n                order: 1;\r\n                font-size: 50px;\r\n                text-align: center;\r\n                margin-bottom: 20px;\r\n                    }\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                display: flex;\r\n                flex-direction: column;\r\n                justify-content: center;\r\n                align-items: flex-start;\r\n                width:40%;\r\n                height: 100%;\r\n                }\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n                width: 80%;\r\n                height: 40px;\r\n                border-color: rgb(212, 5, 5);\r\n                    }\r\n\r\n.slide7[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n    margin-bottom: 20px;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    width: 100%;\r\n    margin-top: 20px;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 1;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n.slider[_ngcontent-%COMP%] {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    height: 15px;\r\n    background: rgb(255, 255, 255);\r\n    outline: none;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 8px;\r\n  }\r\n\r\n.slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n    -webkit-appearance: none;\r\n    appearance: none;\r\n    width: 20px;\r\n    height: 60px;\r\n    background: rgb(248, 224, 5);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(248, 224, 5);\r\n    border-radius: 50px;\r\n  }\r\n\r\n.slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n    width: 20px;\r\n    height: 60px;\r\n    background: rgb(255, 255, 255);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 4px;\r\n  }\r\n.__range[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 100%;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-max[_ngcontent-%COMP%]{\r\n\tfloat: right;\r\n}\r\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n}\r\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n    background-color: #c657a0;\r\n  }\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n\tposition:relative;\r\n\tdisplay: flex;\r\n\tjustify-content: space-between;\r\n\theight: auto;\r\n\tbottom: 10px;\r\n\t\r\n\t-webkit-user-select: none;                   \r\n\tuser-select: none; \r\n\t\r\n\tpointer-events:none;  \r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n\twidth: 10px;\r\n\theight: 10px;\r\n\tmin-height: 10px;\r\n\tborder-radius: 100px;\r\n\t\r\n\twhite-space: nowrap;       \r\n  padding:0;\r\n  line-height: 40px;\r\n}\r\n\r\n.slide8[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n\r\n.slide9[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide10[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n        margin-top: 20px;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center; \r\n     }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: center;            \r\n        }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80px;\r\n    height: 50px;\r\n    border: 4px solid rgb(212, 5, 5);\r\n    text-align: center;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    margin: 0 5px 0 0;\r\n    padding: 0;\r\n    display: flex;\r\n    align-items: center;\r\n    flex-direction: row;\r\n        }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n        border: none;\r\n        border-bottom: 1px solid white;\r\n        margin-bottom: 1px;\r\n        width: 5px;\r\n        border-radius: 0px;\r\n        height: 25px;\r\n        color: white;\r\n        background-color: rgb(212, 5, 5);\r\n        display: flex;\r\n        flex-direction: column;\r\n        font-size: 20px;\r\n        justify-content: center;\r\n        align-items: center;\r\n        }\r\n\r\n.slide11[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        align-items: center;\r\n        display: flex;\r\n        order: 1;\r\n        width: 37%;\r\n        height: 100%;\r\n    }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide12[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        height: 15%;\r\n        width: 15%;\r\n            }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide13[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 1;\r\n    display: flex;\r\n    align-items:center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    margin: 50px 0 50px 0;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n\r\n.slide14[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide15[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 2;\r\n    \r\n            }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide16[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide17[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 2;\r\n    \r\n            }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide18[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: space-between;\r\n            width: 100%;\r\n            height: 100%;\r\n        }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items:center;\r\n            width:53%;\r\n            height: 100%;\r\n        }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            height: 30%;\r\n            width: 15%;\r\n                }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            font-size: 50px;\r\n            text-align: center;\r\n            margin-bottom: 20px;\r\n                }\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n            order: 1;\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items: center;\r\n            width:40%;\r\n            height: 100%;\r\n            }\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n            width: 80%;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n                }\r\n\r\n.slide19[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        margin-left: 50px;\r\n        display: flex;\r\n        align-items: center;\r\n        order: 1;\r\n        width: 37%;\r\n        height: 100%;\r\n    }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide20[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 1;\r\n    width: 60%;\r\n    height: 80%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 80%;\r\n  width: 40%;\r\n  filter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:60%;\r\n    height: 100%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 40%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n.slide21[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 70%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-end;\r\n    justify-content: flex-end;\r\n    order: 1;\r\n    width: 22%;\r\n    height: 100%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 70%;\r\n  width: 30%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-start;\r\n    align-items: center;\r\n    width:70%;\r\n    height: 80%;\r\n    }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n       color: green;\r\n       font-size: 50px;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n          \r\n            font-size: 100px;\r\n            margin: 10px;\r\n                }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n        color: rgb(0, 0, 133);\r\n        font-size: 30px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n        color: red;\r\n        line-height: normal;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n        color: red;\r\n        line-height: normal;\r\n        font-size: 60px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        width: 100px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    align-items: flex-end;\r\n    width: 22%;\r\n    height: 90%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n color: green;\r\n width: 80px;\r\n}\r\n\r\n.slide24[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 2;\r\n    display: flex;\r\n    align-items:flex-end;\r\n    width: 47%;\r\n    height: 100%;\r\n    margin-top: 10px;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    text-align: center;\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    margin: 50px 0 50px 0;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n       display: flex;\r\n       flex-direction: row;\r\n       justify-content: space-around;\r\n       align-items: flex-start;\r\n       width: 100%;\r\n\r\n            }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\r\n        width: 450px;\r\n        height: 450px;\r\n    }\r\n\r\n.slide25[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-end;\r\n    justify-content: flex-end;\r\n    order: 1;\r\n    width: 22%;\r\n    height: 20%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-start;\r\n    align-items: center;\r\n    width:70%;\r\n    height: 80%;\r\n    }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 50px;\r\n        }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    color: rgb(0, 0, 133);\r\n    font-size: 40px;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n   }\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n width: 60px;\r\n}\r\n\r\n.slide26[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 30%;\r\n    width: 15%;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 50px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:60%;\r\n    height: 100%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        align-items: flex-start;\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n.slide27[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    \r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%] {\r\n        margin-bottom: 15px;\r\n        width: 50%;\r\n        border-radius: 50px;\r\n        background-color: rgb(134, 134, 134);\r\n        color:white;\r\n      }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .list-item[_ngcontent-%COMP%] {  \r\n\r\nborder: 3px solid rgb(255, 0, 0);\r\nborder-radius: 4px;\r\ncolor: rgb(153, 153, 153);\r\nline-height: 90px;\r\nfont-weight: 400;\r\nbackground-color: rgb(255, 255, 255);\r\nwidth: 88%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%] {\r\n    height: 100%;\r\n    border: none;\r\n    color: rgb(153, 153, 153);\r\n    line-height: 45px;\r\n    background-color: rgb(255, 255, 255);\r\n    box-shadow: rgba(0,0,0,0.2) 0px 1px 2px 0px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width: 100%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 10%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    height: 100%;\r\n    width: -moz-available;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        margin-top: 0;\r\n        margin-bottom: 1rem;\r\n        height: 29px;\r\n        }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]{\r\n    display: inline;\r\n    flex-direction: column;\r\n    width: 10%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover{\r\n    background-color: rgb(218, 98, 98);\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%]{\r\n    color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   input[_ngcontent-%COMP%]{\r\n        color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   i[_ngcontent-%COMP%]{\r\n        color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .contentTab[_ngcontent-%COMP%]{\r\n          width: 100%;\r\n          height: 500px;\r\n          overflow-y: scroll;\r\n      }\r\n\r\n@media screen and (max-width: 768px) {\r\n   \r\n    .body[_ngcontent-%COMP%]{\r\n        width: 100%;\r\n        height: 100%;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n    }\r\n    .footer[_ngcontent-%COMP%]{\r\n        flex-direction: column;\r\n    }\r\n    .left[_ngcontent-%COMP%]{\r\n        justify-content: center;\r\n        align-items: center;\r\n        width: 100%;\r\n    }\r\n    .left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        width: 60px;\r\n    }\r\n    .point[_ngcontent-%COMP%]{\r\n        text-align: center;\r\n        font-size: 7px;\r\n    }\r\n     .center[_ngcontent-%COMP%]{\r\n      width: 90%;\r\n      margin-left: 20px;  \r\n     }\r\n    .center[_ngcontent-%COMP%]   .listP[_ngcontent-%COMP%]{\r\n   text-align: center;\r\n    }\r\n    \r\n    \r\n.slide0[_ngcontent-%COMP%]{\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center ;\r\n    height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n  }\r\n\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n   width:100%;\r\n   }\r\n\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n   }\r\n\r\n\r\n\r\n.slide1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 90%;\r\n  height: 100%;\r\n}\r\n\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\nwidth: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    }\r\n\r\n  \r\n  .slide2[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content:flex-end;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n   flex-direction: column;\r\n   justify-content: space-around;\r\n   align-items: center;\r\n   width:100%;\r\n   height: 100%;\r\n   }\r\n   .slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n   }\r\n \r\n  \r\n  .slide3[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n \r\n\r\n\r\n.slide4[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-end;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n    \r\n     \r\n     .slide5[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-between;\r\n        align-items: center;\r\n        width: 100%;\r\n        height: 100%;\r\n\r\n    }\r\n    .slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 100%;\r\n        height: 100%;\r\n        z-index: 1;\r\n     \r\n    }\r\n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-between;\r\n        align-items: center;\r\n        width:100%;\r\n        height: 100%;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 30px;\r\n        text-align: center;\r\n        margin-block: auto;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n\r\n           display: flex;\r\n           flex-direction: column;\r\n           justify-content: center;\r\n            width: 100%;\r\n            height: 20%;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items: flex-end;\r\n            width: 75%;\r\n         }\r\n    \r\n         .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;  \r\n            margin: 10px;          \r\n            }\r\n            \r\n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 80px;\r\n        height: 50.5px;\r\n        border: 4px solid rgb(212, 5, 5);\r\n        text-align: center;\r\n        }\r\n   \r\n \r\n .slide6[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 15%;\r\n    width: 15%;\r\n        }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n\r\n    \r\n\r\n.slide7[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    align-items: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    margin-top: 20px;\r\n    }\r\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 20px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n    .slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n\r\n.slider[_ngcontent-%COMP%] {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    height: 15px;\r\n    background: rgb(255, 255, 255);\r\n    outline: none;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 8px;\r\n  }\r\n  \r\n  \r\n  \r\n  .slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n    -webkit-appearance: none;\r\n    appearance: none;\r\n    width: 10px;\r\n    height: 40px;\r\n    background: rgb(248, 224, 5);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(248, 224, 5);\r\n    border-radius: 30px;\r\n  }\r\n  \r\n  \r\n  .slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n    width: 10px;\r\n    height: 30px;\r\n    background: rgb(255, 255, 255);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 4px;\r\n  }\r\n.__range[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 100%;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n\r\n.__range-max[_ngcontent-%COMP%]{\r\n\tfloat: right;\r\n}\r\n           \r\n\r\n\r\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n}\r\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n    background-color: #c657a0;\r\n  }\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n\tposition:relative;\r\n\tdisplay: flex;\r\n\tjustify-content: space-between;\r\n\theight: auto;\r\n\tbottom: 6px;\r\n\t\r\n\t-webkit-user-select: none;                   \r\n\tuser-select: none; \r\n\t\r\n\tpointer-events:none;  \r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n\twidth: 10px;\r\n\theight: 10px;\r\n\tmin-height: 10px;\r\n\tborder-radius: 100px;\r\n\t\r\n\twhite-space: nowrap;       \r\n  padding:0;\r\n  line-height: 40px;\r\n}\r\n\r\n\r\n\r\n.slide8[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n    \r\n.slide9[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n\r\n\r\n.slide10[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        width: 100%;\r\n        margin-top: 20px;\r\n    }\r\n\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center; \r\n        margin-top: 10px;\r\n     }\r\n  \r\n     \r\n        \r\n\r\n.slide11[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    align-items: center;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n  \r\n  .slide12[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:center;\r\n    width: 100%;\r\n    height: 700px;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n\r\n\r\n.slide13[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\nwidth: 80%;\r\nheight: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n\r\n.slide14[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: flex-end;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-around;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n.slide15[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    order: 2;\r\n\r\n        }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n\r\n\r\n\r\n.slide16[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: flex-end;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n\r\n.slide17[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n       \r\n       .slide18[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:100%;\r\n        height: 100%;\r\n    }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        height: 20%;\r\n        width: 15%;\r\n            }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        font-size: 30px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n\r\n    .slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: flex-start ;\r\n        align-items: center;\r\n        width:100%;\r\n        height: 100%;\r\n        }\r\n     \r\n    \r\n\r\n.slide19[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n        \r\n\r\n.slide20[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: center;\r\norder: 1;\r\nwidth: 100%;\r\nheight: 80%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nheight: 60%;\r\nwidth: 40%;\r\nfilter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n    }\r\n\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-start;\r\n    flex-direction: column;\r\n    margin-top: 10px;\r\n}\r\n\r\n\r\n.slide21[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay:none;\r\nflex-direction: column;\r\nalign-items: center;\r\njustify-content: center;\r\norder: 1;\r\nwidth: 100%;\r\nheight: 10%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content:center;\r\nalign-items: center;\r\nwidth:80%;\r\nheight: 80%;    \r\n}\r\n\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n   color: green;\r\n   font-size: 30px;\r\n   text-align: center;\r\n    }\r\n    .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n        font-size: 80px;\r\n        margin: 10px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    color: rgb(0, 0, 133);\r\n    font-size: 30px;\r\n    text-align: center;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n    color: red;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    width: 360px;\r\n    color: red;\r\n    font-size: 30px;\r\n    text-align: center;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 80px;\r\n        }\r\n\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-end;\r\n    align-items: flex-end;\r\n    width:97%;\r\n    height: 15%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    color: green;\r\n    width: 80px;\r\n}\r\n        \r\n\r\n\r\n\r\n.slide24[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\nmargin-left: 50px;\r\norder: 2;\r\ndisplay: flex;\r\nalign-items:flex-end;\r\nwidth: 88%;\r\nheight: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-end;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\nmargin-top: 20px;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\nwidth: 80%;\r\nheight: 40px;\r\nmargin: 50px 0 50px 0;\r\nborder-color: rgb(212, 5, 5);\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n   display: flex;\r\n   flex-direction: row;\r\n   justify-content: space-around;\r\n   align-items: flex-start;\r\n   width: 100%;\r\n   margin-top: 20px;\r\n\r\n        }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\r\n    width: 350px;\r\n    height: 350px;\r\n}\r\n\r\n\r\n\r\n.slide25[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nalign-items: flex-end;\r\njustify-content: flex-end;\r\norder: 1;\r\nwidth: 22%;\r\nheight: 20%;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content:flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 80%;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nwidth: 50px;\r\nmargin-bottom: 30px;\r\n    }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\ncolor: rgb(0, 0, 133);\r\nfont-size: 30px;\r\nline-height: normal;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\norder: 3;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\ntext-align: center;\r\nline-height: normal;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nwidth: 60px;\r\n}\r\n\r\n          \r\n\r\n.slide26[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 20%;\r\n    width: 15%;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n     font-size: 50px;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n    margin-left: 5px;\r\n        }\r\n\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin: 20px;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: flex-start;\r\n        width: 90%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 90%;\r\n    }\r\n\r\n    \r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n\r\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix1QkFBdUI7QUFDM0I7QUFDQTtJQUNJLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0IsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZix3QkFBd0I7SUFDeEIsaUJBQWlCOztBQUVyQjtBQUNBO0lBQ0ksd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osZUFBZTtBQUNuQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFFQTtDQUNDLFdBQVc7Q0FDWCxZQUFZO0NBQ1osYUFBYTtDQUNiLG1CQUFtQjtDQUNuQix1QkFBdUI7Q0FDdkIsbUJBQW1CO0FBQ3BCO0FBRUEsK0VBQStFO0FBQy9FO0lBQ0ksZ0NBQWdDO0lBQ2hDLFlBQVk7QUFDaEI7QUFDQztJQUNHLG9DQUFvQztJQUNwQyxZQUFZO0FBQ2hCO0FBQ0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBRUE7Q0FDQyxRQUFRO0NBQ1IsYUFBYTtDQUNiLHNCQUFzQjtHQUNwQix1QkFBdUI7R0FDdkIsa0JBQWtCO0FBQ3JCLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFFRztJQUNDLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO09BQ3pCO0FBRVAsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtHQUNkLHNCQUFzQjtHQUN0Qix1QkFBdUI7R0FDdkIsbUJBQW1CO0dBQ25CLFNBQVM7R0FDVCxZQUFZO0dBQ1o7QUFDQTtBQUNILGVBQWU7R0FDWjtBQUNBO0lBQ0MsWUFBWTtJQUNaLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsNEJBQTRCO09BQ3pCO0FBRUEsNkVBQTZFO0FBQzdFO1FBQ0MsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtNQUNFLFFBQVE7TUFDUixVQUFVO01BQ1YsWUFBWTtJQUNkO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtPQUNkLHNCQUFzQjtPQUN0Qix1QkFBdUI7T0FDdkIsdUJBQXVCO09BQ3ZCLFNBQVM7T0FDVCxZQUFZO09BQ1o7QUFDQTtJQUNILGVBQWU7T0FDWjtBQUNBO1FBQ0MsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7V0FDekI7QUFFTiw2RUFBNkU7QUFDMUU7UUFDQSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixTQUFTO1FBQ1QsV0FBVztRQUNYO0FBQ0E7UUFDQSxlQUFlO1FBQ2Y7QUFDQTtXQUNHLGFBQWE7V0FDYixzQkFBc0I7V0FDdEIsdUJBQXVCOztRQUUxQjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7O1NBRTFCO0FBQ0Q7UUFDQSxZQUFZO1FBQ1osWUFBWTtRQUNaLFdBQVc7UUFDWCw0QkFBNEI7WUFDeEI7QUFDQTtnQkFDSSxZQUFZO2dCQUNaLFdBQVc7b0JBQ1A7QUFFcEIsNkVBQTZFO0FBQ3pFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLGlCQUFpQjtRQUNqQixRQUFRO1FBQ1IsVUFBVTtRQUNWLFlBQVk7UUFDWixhQUFhO1FBQ2IsVUFBVTtRQUNWLGdCQUFnQjtJQUNwQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QixTQUFTO1FBQ1QsWUFBWTtRQUNaO0FBQ0E7SUFDSixlQUFlO1FBQ1g7QUFDQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1AsNkVBQTZFO0FBQzdFO1FBQ0csYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixVQUFVO1FBQ1YsWUFBWTtRQUNaLFVBQVU7UUFDVixhQUFhO1FBQ2IsZ0JBQWdCO0lBQ3BCO0FBR0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtRQUNBLGVBQWU7UUFDZjtBQUNBO1dBQ0csYUFBYTtXQUNiLG1CQUFtQjtXQUNuQiw2QkFBNkI7WUFDNUIsV0FBVztZQUNYLFdBQVc7UUFDZjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiwyQkFBMkI7WUFDM0IsbUJBQW1CO1NBQ3RCO0FBRUE7WUFDRyxhQUFhO1lBQ2IsbUJBQW1CO1lBQ25CLHVCQUF1QjtZQUN2QjtBQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxZQUFZO1FBQ1osZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjtBQUNKO1FBQ0ksUUFBUTtRQUNSLGlCQUFpQjtRQUNqQixVQUFVO1FBQ1YsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixtQkFBbUI7WUFDZjtBQUVSO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsMkJBQTJCO1FBQzNCO0FBQ0E7WUFDSSxZQUFZO1lBQ1osOEJBQThCO1lBQzlCLGtCQUFrQjtZQUNsQixVQUFVO1lBQ1Ysa0JBQWtCO1lBQ2xCLFlBQVk7WUFDWixZQUFZO1lBQ1osZ0NBQWdDO1lBQ2hDLGFBQWE7WUFDYixzQkFBc0I7WUFDdEIsZUFBZTtZQUNmLHVCQUF1QjtZQUN2QixtQkFBbUI7WUFDbkI7QUFFQSw2RUFBNkU7QUFDN0U7Z0JBQ0ksYUFBYTtnQkFDYixtQkFBbUI7Z0JBQ25CLDhCQUE4QjtnQkFDOUIsV0FBVztnQkFDWCxZQUFZO1lBQ2hCO0FBQ0E7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2QixrQkFBa0I7Z0JBQ2xCLFNBQVM7Z0JBQ1QsWUFBWTtZQUNoQjtBQUNBO2dCQUNJLFFBQVE7Z0JBQ1IsV0FBVztnQkFDWCxVQUFVO29CQUNOO0FBQ1I7Z0JBQ0ksUUFBUTtnQkFDUixlQUFlO2dCQUNmLGtCQUFrQjtnQkFDbEIsbUJBQW1CO29CQUNmO0FBR1I7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2Qix1QkFBdUI7Z0JBQ3ZCLFNBQVM7Z0JBQ1QsWUFBWTtnQkFDWjtBQUVBO2dCQUNBLFVBQVU7Z0JBQ1YsWUFBWTtnQkFDWiw0QkFBNEI7b0JBQ3hCO0FBR3BCLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0FBQ0osZUFBZTtJQUNYO0FBQ0E7SUFDQSxVQUFVO0lBQ1YsWUFBWTtJQUNaLDRCQUE0QjtRQUN4QjtBQUNSO0lBQ0ksUUFBUTtBQUNaO0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFHQSxzQkFBc0I7QUFDdEI7SUFDRSx3QkFBd0I7SUFDeEIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxZQUFZO0lBQ1osNEJBQTRCO0lBQzVCLGVBQWU7SUFDZixrQ0FBa0M7SUFDbEMsbUJBQW1CO0VBQ3JCO0FBRUEsZ0JBQWdCO0FBQ2hCO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFDRjtJQUNJLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7Q0FDQyxrQkFBa0I7QUFDbkI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUVBO0NBQ0MsWUFBWTtBQUNiO0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osWUFBWTtDQUNaLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25CO0FBR0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFUiw0RUFBNEU7QUFDNUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtJQUNBLGVBQWU7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUVBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjtBQUVBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO0tBQ3RCO0FBQ0Q7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQix1QkFBdUI7UUFDdkI7QUFFUjtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdDQUFnQztJQUNoQyxrQkFBa0I7SUFDbEI7QUFDSjtJQUNJLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsVUFBVTtJQUNWLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsbUJBQW1CO1FBQ2Y7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQjtBQUNBO1FBQ0ksWUFBWTtRQUNaLDhCQUE4QjtRQUM5QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osWUFBWTtRQUNaLGdDQUFnQztRQUNoQyxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLGVBQWU7UUFDZix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CO0FBR1IsOEVBQThFO0FBQzFFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLG1CQUFtQjtRQUNuQixhQUFhO1FBQ2IsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFFTiw4RUFBOEU7QUFDOUU7UUFDRSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxVQUFVO1lBQ047QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBR1I7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1osOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLDRCQUE0QjtRQUN4QjtBQUdSLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7SUFDQSxlQUFlO0lBQ2Y7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDZCQUE2QjtRQUM3QixXQUFXO0lBQ2Y7QUFFQSw4RUFBOEU7QUFDOUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTs7WUFFSjtBQUNSO1FBQ0ksUUFBUTtRQUNSLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsbUJBQW1CO1lBQ2Y7QUFJUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsU0FBUztRQUNULFlBQVk7UUFDWjtBQUVBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFHWiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBSUEsOEVBQThFO0FBQzlFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsU0FBUztRQUNULFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7O1lBRUo7QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBSVI7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR0QsOEVBQThFO0FBQzlFO1lBQ0MsYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiw4QkFBOEI7WUFDOUIsV0FBVztZQUNYLFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixrQkFBa0I7WUFDbEIsU0FBUztZQUNULFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixXQUFXO1lBQ1gsVUFBVTtnQkFDTjtBQUNSO1lBQ0ksUUFBUTtZQUNSLGVBQWU7WUFDZixrQkFBa0I7WUFDbEIsbUJBQW1CO2dCQUNmO0FBR1I7WUFDSSxRQUFRO1lBQ1IsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFNBQVM7WUFDVCxZQUFZO1lBQ1o7QUFFQTtZQUNBLFVBQVU7WUFDVixZQUFZO1lBQ1osNEJBQTRCO2dCQUN4QjtBQUloQiw4RUFBOEU7QUFDMUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFJWiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsUUFBUTtJQUNSLFVBQVU7SUFDVixXQUFXO0FBQ2Y7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsOERBQThEO0FBQ2hFO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFSjtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFFQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFDQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFFSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsUUFBUTtJQUNSLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFQTtRQUNJLG1CQUFtQjtPQUNwQixZQUFZO09BQ1osZUFBZTtRQUNkO0FBQ0E7O1lBRUksZ0JBQWdCO1lBQ2hCLFlBQVk7Z0JBQ1I7QUFDWjtRQUNJLG1CQUFtQjtRQUNuQixxQkFBcUI7UUFDckIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxVQUFVO1FBQ1YsbUJBQW1CO1lBQ2Y7QUFDUjtRQUNJLFVBQVU7UUFDVixtQkFBbUI7UUFDbkIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxZQUFZO1lBQ1I7QUFJWjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixxQkFBcUI7SUFDckIsVUFBVTtJQUNWLFdBQVc7QUFDZjtBQUNBO0NBQ0MsWUFBWTtDQUNaLFdBQVc7QUFDWjtBQUdBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsUUFBUTtJQUNSLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsVUFBVTtJQUNWLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsNEJBQTRCO1FBQ3hCO0FBQ0o7T0FDRyxhQUFhO09BQ2IsbUJBQW1CO09BQ25CLDZCQUE2QjtPQUM3Qix1QkFBdUI7T0FDdkIsV0FBVzs7WUFFTjtBQUNSO1FBQ0ksWUFBWTtRQUNaLGFBQWE7SUFDakI7QUFHSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGFBQWE7SUFDYixxQkFBcUI7SUFDckIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBRUE7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFSjtJQUNJLFdBQVc7UUFDUDtBQUNSO0lBQ0kscUJBQXFCO0lBQ3JCLGVBQWU7QUFDbkI7QUFFQTtJQUNJLFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtHQUN4QjtBQUNIO0NBQ0MsV0FBVztBQUNaO0FBR0EsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjtBQUVSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO1FBQ3hCO0FBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDtBQUdBO1FBQ0ksVUFBVTtJQUNkO0FBQ0E7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFNBQVM7SUFDYjtBQUVILDhFQUE4RTtBQUM5RTtJQUNHLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0FBQ2hCO0FBS0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULFlBQVk7O0lBRVo7QUFDQTtRQUNJLG1CQUFtQjtRQUNuQixVQUFVO1FBQ1YsbUJBQW1CO1FBQ25CLG9DQUFvQztRQUNwQyxXQUFXO01BQ2I7QUFHQTs7QUFFTixnQ0FBZ0M7QUFDaEMsa0JBQWtCO0FBQ2xCLHlCQUF5QjtBQUN6QixpQkFBaUI7QUFDakIsZ0JBQWdCO0FBQ2hCLG9DQUFvQztBQUNwQyxVQUFVO0FBQ1Y7QUFFRTtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGlCQUFpQjtJQUNqQixvQ0FBb0M7SUFDcEMsMkNBQTJDO0lBQzNDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixXQUFXO0VBQ2I7QUFHQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixVQUFVO0VBQ1o7QUFFQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCO0FBRUE7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWjtBQUVOO0lBQ0UsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixVQUFVO0VBQ1o7QUFDQTtJQUNFLFdBQVc7RUFDYjtBQUNBO0lBQ0Usa0NBQWtDO0VBQ3BDO0FBQ0E7SUFDRSxZQUFZO0lBQ1o7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFHRTtVQUNJLFdBQVc7VUFDWCxhQUFhO1VBQ2Isa0JBQWtCO01BQ3RCO0FBTU4sMEdBQTBHO0FBQzFHOztJQUVJO1FBQ0ksV0FBVztRQUNYLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtJQUNsQztJQUNBO1FBQ0ksc0JBQXNCO0lBQzFCO0lBQ0E7UUFDSSx1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFdBQVc7SUFDZjtJQUNBO1FBQ0ksV0FBVztJQUNmO0lBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsY0FBYztJQUNsQjtLQUNDO01BQ0MsVUFBVTtNQUNWLGlCQUFpQjtLQUNsQjtJQUNEO0dBQ0Qsa0JBQWtCO0lBQ2pCOztJQUVBLDZFQUE2RTtBQUNqRjtJQUNJLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0Isb0JBQW9CO0lBQ3BCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtFQUNkOztBQUVGO0dBQ0csVUFBVTtHQUNWOztBQUVIO0lBQ0ksZUFBZTtHQUNoQjs7O0FBR0gsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0lBQ0ksZUFBZTtJQUNmOztFQUVGLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHdCQUF3QjtBQUMxQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7R0FDZCxzQkFBc0I7R0FDdEIsNkJBQTZCO0dBQzdCLG1CQUFtQjtHQUNuQixVQUFVO0dBQ1YsWUFBWTtHQUNaO0dBQ0E7SUFDQyxlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVELDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7SUFDQTtJQUNBLGVBQWU7SUFDZixrQkFBa0I7SUFDbEI7OztLQUdDLDZFQUE2RTtLQUM3RTtRQUNHLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsOEJBQThCO1FBQzlCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsWUFBWTs7SUFFaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsWUFBWTtRQUNaLFVBQVU7O0lBRWQ7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDhCQUE4QjtRQUM5QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjtRQUNBO1FBQ0EsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEI7UUFDQTs7V0FFRyxhQUFhO1dBQ2Isc0JBQXNCO1dBQ3RCLHVCQUF1QjtZQUN0QixXQUFXO1lBQ1gsV0FBVztRQUNmO1FBQ0E7WUFDSSxhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixxQkFBcUI7WUFDckIsVUFBVTtTQUNiOztTQUVBO1lBQ0csYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7WUFDdkIsWUFBWTtZQUNaOztJQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxjQUFjO1FBQ2QsZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjs7Q0FFUCw2RUFBNkU7Q0FDN0U7SUFDRyxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO1FBQ047QUFDUjtJQUNJLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtRQUNmOzs7QUFHUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjs7O0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7SUFDSjtJQUNBLFFBQVE7QUFDWjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4QixXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixhQUFhO0lBQ2IsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjs7O0VBR0Esc0JBQXNCO0VBQ3RCO0lBQ0Usd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsWUFBWTtJQUNaLDRCQUE0QjtJQUM1QixlQUFlO0lBQ2Ysa0NBQWtDO0lBQ2xDLG1CQUFtQjtFQUNyQjs7RUFFQSxnQkFBZ0I7RUFDaEI7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjtBQUNGO0lBQ0ksVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUNBO0NBQ0Msa0JBQWtCO0FBQ25COztBQUVBO0NBQ0MsWUFBWTtBQUNiOzs7O0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osV0FBVztDQUNYLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25COzs7QUFHQSw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7SUFFQSw0RUFBNEU7QUFDaEY7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCO0lBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjs7SUFFQTtRQUNJLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsNkJBQTZCO1FBQzdCLG1CQUFtQjtRQUNuQixnQkFBZ0I7S0FDbkI7Ozs7QUFJTCw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOztFQUVGLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7O0FBR1I7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7OztBQUdKLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLFdBQVc7QUFDWCxhQUFhO0FBQ2I7QUFDQTtBQUNBLFFBQVE7QUFDUixVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7O0FBRUEsNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDZCQUE2QjtBQUM3QixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFROztRQUVKO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7OztBQUlSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaOzs7O0FBSUosNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDJCQUEyQjtBQUMzQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjs7QUFFQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO1FBQ2Y7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7O09BRUcsOEVBQThFO09BQzlFO1FBQ0MsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsVUFBVTtZQUNOO0lBQ1I7UUFDSSxRQUFRO1FBQ1IsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixtQkFBbUI7WUFDZjs7SUFFUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDRCQUE0QjtRQUM1QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjs7O0FBR1IsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7O0FBR0osOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLFlBQVk7QUFDWjtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsUUFBUTtBQUNSLFdBQVc7QUFDWCxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWCxVQUFVO0FBQ1YsOERBQThEO0FBQzlEO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0QiwyQkFBMkI7QUFDM0IsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1Y7O0FBRUo7SUFDSSxhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixnQkFBZ0I7QUFDcEI7O0FBRUEsOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixRQUFRO0FBQ1IsV0FBVztBQUNYLFdBQVc7QUFDWDtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsc0JBQXNCO0FBQ3RCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsV0FBVztBQUNYOztBQUVBO0lBQ0ksbUJBQW1CO0dBQ3BCLFlBQVk7R0FDWixlQUFlO0dBQ2Ysa0JBQWtCO0lBQ2pCO0lBQ0E7UUFDSSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7WUFDUjtBQUNaO0lBQ0ksbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2Ysa0JBQWtCO1FBQ2Q7QUFDUjtJQUNJLFVBQVU7UUFDTjtBQUNSO0lBQ0ksbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQjtRQUNkO0FBQ1I7SUFDSSxXQUFXO1FBQ1A7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIscUJBQXFCO0lBQ3JCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2Y7Ozs7QUFJQSw4RUFBOEU7QUFDOUU7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsUUFBUTtBQUNSLGFBQWE7QUFDYixvQkFBb0I7QUFDcEIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIseUJBQXlCO0FBQ3pCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWixxQkFBcUI7QUFDckIsNEJBQTRCO0lBQ3hCO0FBQ0o7R0FDRyxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDZCQUE2QjtHQUM3Qix1QkFBdUI7R0FDdkIsV0FBVztHQUNYLGdCQUFnQjs7UUFFWDtBQUNSO0lBQ0ksWUFBWTtJQUNaLGFBQWE7QUFDakI7OztBQUdBLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHlCQUF5QjtBQUN6QixRQUFRO0FBQ1IsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDBCQUEwQjtBQUMxQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFdBQVc7QUFDWCxtQkFBbUI7SUFDZjtBQUNKO0lBQ0ksa0JBQWtCO0FBQ3RCLHFCQUFxQjtBQUNyQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0EsV0FBVztBQUNYOzs7QUFHQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0tBQ0MsZUFBZTtJQUNoQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsZ0JBQWdCO1FBQ1o7O0lBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDs7O0lBR0E7UUFDSSxVQUFVO0lBQ2Q7SUFDQTtRQUNJLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsU0FBUztJQUNiOzs7SUFHQSIsImZpbGUiOiJzcmMvYXBwL3Rlc3QvdGVzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcbi5ib2R5e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246Y29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLmZvb3RlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LWhlaWdodDogMTUwcHg7XHJcbiAgICBtaW4taGVpZ2h0OiAxNTBweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMTExRDVFZmY7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG4ubGVmdHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG4ubGVmdCBpbWd7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59XHJcbi5wb2ludHtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgXHJcbn1cclxuaXtcclxuICAgIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcclxufVxyXG5pOmhvdmVye1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5pY29uTmV4e1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuLmNoZWNrSWNvbntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNlbnRlcntcclxuIHdpZHRoOiAxMDAlO1xyXG4gaGVpZ2h0OiAxMDAlO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJ0bioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5idXR0dG9uUkVke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIwNiwgMCwgMCk7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuIGJ1dHRvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxNDUsIDEzNiwgMTM2KTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMSAuaW1hZ2V7XHJcbiAgb3JkZXI6IDI7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5zbGlkZTEgLnRlc3R7XHJcbiBvcmRlcjogMTtcclxuIGRpc3BsYXk6IGZsZXg7XHJcbiBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG53aWR0aDogNDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbiAgIC5zbGlkZTEgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgIH1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICB3aWR0aDogNTIlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgd2lkdGg6NDAlO1xyXG4gICBoZWlnaHQ6IDEwMCU7XHJcbiAgIH1cclxuICAgLnNsaWRlMCAudGVzdCBoMXtcclxuZm9udC1zaXplOiA1MHB4O1xyXG4gICB9XHJcbiAgIC5zbGlkZTAgLnRlc3QgYnV0dG9ue1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE5MiwgMywgMyk7XHJcbiAgICAgICB9XHJcbiAgIFxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAuc2xpZGUye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMiAuaW1hZ2V7XHJcbiAgICAgIG9yZGVyOiAxO1xyXG4gICAgICB3aWR0aDogNTUlO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyIC50ZXN0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgIH1cclxuICAgICAgIC5zbGlkZTIgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICB9XHJcbiAgICAgICAuc2xpZGUyIC50ZXN0IGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgXHJcbiAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDMgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAgLnNsaWRlM3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTMgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIHdpZHRoOiA1NCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMyAudGVzdHtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDcwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMyAudGVzdCBoMXtcclxuICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRue1xyXG4gICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gXHJcbiAgICAgICAgIH1cclxuICAgICAgICAuc2xpZGUzIC50ZXN0IC5ibG9ja0J0biBidXR0b257XHJcbiAgICAgICAgd2lkdGg6IDMxMHB4O1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBtYXJnaW46IDVweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHJnYigxOTIsIDMsIDMpO1xyXG4gICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgLnNsaWRlMyAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayBidXR0b257XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDVweDtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgIC5zbGlkZTR7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU0IC5pbWFnZXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICB3aWR0aDogMzMlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTVweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTQgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTQgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTQgLnRlc3QgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgXHJcbiAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgLnNsaWRlNXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTUgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA2NHB4O1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG4gICAgLnNsaWRlNSAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGU1IC50ZXN0IGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG57XHJcbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXHJcbiAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHl7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IGlucHV0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBib3JkZXI6IDRweCBzb2xpZCByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IHB7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgbWFyZ2luOiAwIDVweCAwIDA7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRue1xyXG4gICAgICAgIG9yZGVyOiAzO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRuIGJ1dHRvbntcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDFweDtcclxuICAgICAgICAgICAgd2lkdGg6IDVweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA2ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgICAgICAgICAuc2xpZGU2e1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuc2xpZGU2IC5pbWFnZXtcclxuICAgICAgICAgICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOjUzJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuc2xpZGU2IC5pbWFnZSBpbWd7XHJcbiAgICAgICAgICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMjUlO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1JTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5zbGlkZTYgLmltYWdlIGgxe1xyXG4gICAgICAgICAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICAuc2xpZGU2IC50ZXN0e1xyXG4gICAgICAgICAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDo0MCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIC5zbGlkZTYgLnRlc3Qgc2VsZWN0e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlN3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU3IC5jb2x1bTF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuLnNsaWRlNyAuY29sdW0ye1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLmltYWdle1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIHdpZHRoOiAzNyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlNyAudGVzdCBoMXtcclxuZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlNyAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgIH1cclxuLnNsaWRlNyAucHJvZ3Jlc3N7XHJcbiAgICBvcmRlcjogMztcclxufVxyXG5cclxuLnNsaWRlciB7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgLyogZm9yIGNocm9tZS9zYWZhcmkgKi9cclxuICAuc2xpZGVyOjotd2Via2l0LXNsaWRlci10aHVtYiB7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICBhcHBlYXJhbmNlOiBub25lO1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjQ4LCAyMjQsIDUpO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDI0OCwgMjI0LCA1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIGZvciBmaXJlZm94ICovXHJcbiAgLnNsaWRlcjo6LW1vei1yYW5nZS10aHVtYiB7XHJcbiAgICB3aWR0aDogMjBweDtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIH1cclxuLl9fcmFuZ2V7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5fX3JhbmdlLXN0ZXB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlOyAgICAgICAgICAgICAgICBcclxufVxyXG4uX19yYW5nZS1zdGVwe1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTsgICAgICAgICAgICAgICAgXHJcbn1cclxuXHJcbi5fX3JhbmdlLW1heHtcclxuXHRmbG9hdDogcmlnaHQ7XHJcbn1cclxuICAgICAgICAgICBcclxuXHJcblxyXG4uX19yYW5nZSBpbnB1dDo6cmFuZ2UtcHJvZ3Jlc3Mge1x0YmFja2dyb3VuZDogcmdiKDE4OSwgOCwgOCk7XHJcbn1cclxuLnNsaWRlciBpbnB1dFt0eXBlPXJhbmdlXTo6LW1vei1yYW5nZS1wcm9ncmVzcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY1N2EwO1xyXG4gIH1cclxuLl9fcmFuZ2Utc3RlcCBkYXRhbGlzdCB7XHJcblx0cG9zaXRpb246cmVsYXRpdmU7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0aGVpZ2h0OiBhdXRvO1xyXG5cdGJvdHRvbTogMTBweDtcclxuXHQvKiBkaXNhYmxlIHRleHQgc2VsZWN0aW9uICovXHJcblx0LXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTsgLyogU2FmYXJpICovICAgICAgICBcclxuXHQtbW96LXVzZXItc2VsZWN0OiBub25lOyAvKiBGaXJlZm94ICovXHJcblx0LW1zLXVzZXItc2VsZWN0OiBub25lOyAvKiBJRTEwKy9FZGdlICovICAgICAgICAgICAgICAgIFxyXG5cdHVzZXItc2VsZWN0OiBub25lOyAvKiBTdGFuZGFyZCAqL1xyXG5cdC8qIGRpc2FibGUgY2xpY2sgZXZlbnRzICovXHJcblx0cG9pbnRlci1ldmVudHM6bm9uZTsgIFxyXG59XHJcbi5fX3JhbmdlLXN0ZXAgZGF0YWxpc3Qgb3B0aW9uIHtcclxuXHR3aWR0aDogMTBweDtcclxuXHRoZWlnaHQ6IDEwcHg7XHJcblx0bWluLWhlaWdodDogMTBweDtcclxuXHRib3JkZXItcmFkaXVzOiAxMDBweDtcclxuXHQvKiBoaWRlIHRleHQgKi9cclxuXHR3aGl0ZS1zcGFjZTogbm93cmFwOyAgICAgICBcclxuICBwYWRkaW5nOjA7XHJcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgOCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlOHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU4IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTggLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTggLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTggLnRlc3Qgc2VsZWN0e1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDkqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlOXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU5IC5pbWFnZXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTkgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTkgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU5IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEwKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTEwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEwIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEwIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo0MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMCAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTEwIC50ZXN0IG1hdC1yYWRpby1ncm91cHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2t7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxyXG4gICAgIH1cclxuICAgIC5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHl7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eSBpbnB1dHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXI6IDRweCBzb2xpZCByZ2IoMjEyLCA1LCA1KTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgIHB7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIG1hcmdpbjogMCA1cHggMCAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICB9XHJcblxyXG4uc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9jayAucXR5IC5xdHlCdG57XHJcbiAgICBvcmRlcjogMztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRuIGJ1dHRvbntcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFweDtcclxuICAgICAgICB3aWR0aDogNXB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDExICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgLnNsaWRlMTF7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTExIC5pbWFnZXtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgd2lkdGg6IDM3JTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMSAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMTEgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTExIC50ZXN0IGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgICAgLnNsaWRlMTJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMiAuaW1hZ2V7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgICAgICB3aWR0aDo1MyU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTIgLmltYWdlIGltZ3tcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBoZWlnaHQ6IDE1JTtcclxuICAgICAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAuc2xpZGUxMiAuaW1hZ2UgaDF7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuICAgIC5zbGlkZTEyIC50ZXN0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB3aWR0aDo0MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICBcclxuICAgICAgICAuc2xpZGUxMiAudGVzdCBzZWxlY3R7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTMgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTEze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMyAuaW1hZ2V7XHJcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOiAzNyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTMgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTEzIC50ZXN0IGlucHV0e1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1hcmdpbjogNTBweCAwIDUwcHggMDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNCoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxNHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNCAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTQgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgLnNsaWRlMTV7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNSAuaW1hZ2V7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgICAgICB3aWR0aDo1MyU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTUgLmltYWdlIHB7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMTUgLmltYWdlIGgxe1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgLnNsaWRlMTUgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgIFxyXG4gICAgICAgIC5zbGlkZTE1IC50ZXN0IHNlbGVjdHtcclxuICAgICAgICB3aWR0aDogODAlO1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE2KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE2e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE2IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE2IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo0MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNiAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTE2IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgXHJcbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAuc2xpZGUxN3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE3IC5pbWFnZXtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjUzJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNyAuaW1hZ2UgcHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAuc2xpZGUxNyAuaW1hZ2UgaDF7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuXHJcbiAgICAuc2xpZGUxNyAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgXHJcbiAgICAgICAgLnNsaWRlMTcgLnRlc3Qgc2VsZWN0e1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgICAgICAgICAuc2xpZGUxOHtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGUxOCAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgICAgICAgICB3aWR0aDo1MyU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMTggLmltYWdlIGltZ3tcclxuICAgICAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzAlO1xyXG4gICAgICAgICAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTE4IC5pbWFnZSBoMXtcclxuICAgICAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgLnNsaWRlMTggLnRlc3R7XHJcbiAgICAgICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgXHJcbiAgICAgICAgICAgIC5zbGlkZTE4IC50ZXN0IHNlbGVjdHtcclxuICAgICAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgIC5zbGlkZTE5e1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTkgLmltYWdle1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICB3aWR0aDogMzclO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE5IC50ZXN0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB3aWR0aDo0MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGUxOSAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMTkgLnRlc3QgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjAgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTIwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyMCAuaW1hZ2V7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICB3aWR0aDogNjAlO1xyXG4gICAgaGVpZ2h0OiA4MCU7XHJcbn1cclxuLnNsaWRlMjAgLmltYWdlIGltZ3tcclxuICBoZWlnaHQ6IDgwJTtcclxuICB3aWR0aDogNDAlO1xyXG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMC40cmVtIDAuNHJlbSAwLjQ1cmVtIHJnYmEoMCwgMCwgMzAsIDAuNSkpO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo2MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMCAudGVzdCBoMXtcclxuZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjAgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgLnNsaWRlMjAgLnRlc3QgLmJsb2NrMXtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDQwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIgbGFiZWx7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazEgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazEgbGFiZWx7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjF7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3MCU7XHJcbn1cclxuLnNsaWRlMjEgLmltYWdle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIHdpZHRoOiAyMiU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjEgLmltYWdlIGltZ3tcclxuICBoZWlnaHQ6IDcwJTtcclxuICB3aWR0aDogMzAlO1xyXG59XHJcbi5zbGlkZTIxIC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjcwJTtcclxuICAgIGhlaWdodDogODAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5zbGlkZTIxIC50ZXN0IGgxe1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICBjb2xvcjogZ3JlZW47XHJcbiAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTIxIC50ZXN0IGltZ3tcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAuc2xpZGUyMSAudGVzdCBwe1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgY29sb3I6IHJnYigwLCAwLCAxMzMpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMjEgLnRlc3QgLnByaXgge1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMjEgLnRlc3QgLmZhaWxke1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBmb250LXNpemU6IDYwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTIxIC50ZXN0IGltZ3tcclxuICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcblxyXG5cclxuLnNsaWRlMjEgLm5leHRJY29ue1xyXG4gICAgb3JkZXI6IDM7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHdpZHRoOiAyMiU7XHJcbiAgICBoZWlnaHQ6IDkwJTtcclxufVxyXG4uc2xpZGUyMSAubmV4dEljb24gaW1ne1xyXG4gY29sb3I6IGdyZWVuO1xyXG4gd2lkdGg6IDgwcHg7XHJcbn1cclxuXHJcbiAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNCAuaW1hZ2V7XHJcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOmZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDQ3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuLnNsaWRlMjQgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjQgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNCAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgIH1cclxuICAgIC5zbGlkZTI0IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcclxuICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMjQgLnRlc3QgLmNhbGVuZHtcclxuICAgICAgICB3aWR0aDogNDUwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA0NTBweDtcclxuICAgIH1cclxuXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyNXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjUgLmltYWdle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIHdpZHRoOiAyMiU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxufVxyXG5cclxuLnNsaWRlMjUgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6NzAlO1xyXG4gICAgaGVpZ2h0OiA4MCU7XHJcbiAgICB9XHJcblxyXG4uc2xpZGUyNSAudGVzdCBpbWd7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgICAgICB9XHJcbi5zbGlkZTI1IC50ZXN0IHB7XHJcbiAgICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbiAgICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC5uZXh0SWNvbntcclxuICAgIG9yZGVyOiAzO1xyXG59XHJcbi5zbGlkZTI1IC5uZXh0SWNvbiBwe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5zbGlkZTI1IC5uZXh0SWNvbiAuc29jaWFsTWVkaWEge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgfVxyXG4uc2xpZGUyNSAubmV4dEljb24gLnNvY2lhbE1lZGlhIGltZ3tcclxuIHdpZHRoOiA2MHB4O1xyXG59XHJcblxyXG4gICAgICAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjYgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI2e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDo1MyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjYgLmltYWdlIGltZ3tcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgaGVpZ2h0OiAzMCU7XHJcbiAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgIH1cclxuLnNsaWRlMjYgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIH1cclxuXHJcbi5zbGlkZTI2IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo2MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCBoMXtcclxuZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMXtcclxuICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIG1hdC1yYWRpby1ncm91cCAge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGxhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcblxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI3ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gLnNsaWRlMjd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI3IC5pbWFnZXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjUzJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLnNsaWRlMjcgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgXHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNyAudGVzdCAuc2VhcmNoIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTM0LCAxMzQsIDEzNCk7XHJcbiAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgIH1cclxuIFxyXG4gICAgICBcclxuICAgICAgLnNsaWRlMjcgLnRlc3QgLmxpc3QtaXRlbSB7ICBcclxuXHJcbmJvcmRlcjogM3B4IHNvbGlkIHJnYigyNTUsIDAsIDApO1xyXG5ib3JkZXItcmFkaXVzOiA0cHg7XHJcbmNvbG9yOiByZ2IoMTUzLCAxNTMsIDE1Myk7XHJcbmxpbmUtaGVpZ2h0OiA5MHB4O1xyXG5mb250LXdlaWdodDogNDAwO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbndpZHRoOiA4OCU7XHJcbn1cclxuICBcclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiByZ2IoMTUzLCAxNTMsIDE1Myk7XHJcbiAgICBsaW5lLWhlaWdodDogNDVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGJveC1zaGFkb3c6IHJnYmEoMCwwLDAsMC4yKSAwcHggMXB4IDJweCAwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICBcclxuICAgXHJcbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudCAuYmxvY2sxe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAlO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudCAuYmxvY2sye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazIgcHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgaGVpZ2h0OiAyOXB4O1xyXG4gICAgICAgIH1cclxuICBcclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazN7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDEwJTtcclxuICB9XHJcbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudCAuYmxvY2szIGlucHV0e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQ6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjE4LCA5OCwgOTgpO1xyXG4gIH1cclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50OmhvdmVyIHB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50OmhvdmVyIGlucHV0e1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQ6aG92ZXIgaXtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiBcclxuICAgICAgXHJcbiAgICAgIC5zbGlkZTI3IC50ZXN0IC5jb250ZW50VGFie1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDUwMHB4O1xyXG4gICAgICAgICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgICB9XHJcbiAgXHJcblxyXG5cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gICBcclxuICAgIC5ib2R5e1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgfVxyXG4gICAgLmZvb3RlcntcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgfVxyXG4gICAgLmxlZnR7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIC5sZWZ0IGltZ3tcclxuICAgICAgICB3aWR0aDogNjBweDtcclxuICAgIH1cclxuICAgIC5wb2ludHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiA3cHg7XHJcbiAgICB9XHJcbiAgICAgLmNlbnRlcntcclxuICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7ICBcclxuICAgICB9XHJcbiAgICAuY2VudGVyIC5saXN0UHtcclxuICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMHtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIDtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcblxyXG4uc2xpZGUwIC50ZXN0e1xyXG4gICB3aWR0aDoxMDAlO1xyXG4gICB9XHJcblxyXG4uc2xpZGUwIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTF7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxIC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLnNsaWRlMSAudGVzdHtcclxud2lkdGg6IDEwMCU7XHJcbn1cclxuLnNsaWRlMSAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyIC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDpmbGV4LWVuZDtcclxufVxyXG4uc2xpZGUyIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgd2lkdGg6MTAwJTtcclxuICAgaGVpZ2h0OiAxMDAlO1xyXG4gICB9XHJcbiAgIC5zbGlkZTIgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIH1cclxuIFxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAzICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTMgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufVxyXG4uc2xpZGUzIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUzIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlNHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNCAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDpmbGV4LWVuZDtcclxufVxyXG4uc2xpZGU0IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG4gICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgIC5zbGlkZTV7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICB9XHJcbiAgICAuc2xpZGU1IC5pbWFnZXtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICBcclxuICAgIH1cclxuICAgIC5zbGlkZTUgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGU1IC50ZXN0IGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJsb2NrOiBhdXRvO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0bntcclxuXHJcbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiA3NSU7XHJcbiAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHl7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgXHJcbiAgICAgICAgICAgIG1hcmdpbjogMTBweDsgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MC41cHg7XHJcbiAgICAgICAgYm9yZGVyOiA0cHggc29saWQgcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgXHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5zbGlkZTZ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTYgLmltYWdlIGltZ3tcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgaGVpZ2h0OiAxNSU7XHJcbiAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgIH1cclxuLnNsaWRlNiAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbi5zbGlkZTYgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA3ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGU3e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAuY29sdW0xe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLmNvbHVtMntcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTcgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU3IC50ZXN0IGlucHV0e1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG4gICAgLnNsaWRlNyAucHJvZ3Jlc3N7XHJcbiAgICBvcmRlcjogMztcclxufVxyXG5cclxuLnNsaWRlciB7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgLyogZm9yIGNocm9tZS9zYWZhcmkgKi9cclxuICAuc2xpZGVyOjotd2Via2l0LXNsaWRlci10aHVtYiB7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICBhcHBlYXJhbmNlOiBub25lO1xyXG4gICAgd2lkdGg6IDEwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjQ4LCAyMjQsIDUpO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDI0OCwgMjI0LCA1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIGZvciBmaXJlZm94ICovXHJcbiAgLnNsaWRlcjo6LW1vei1yYW5nZS10aHVtYiB7XHJcbiAgICB3aWR0aDogMTBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIH1cclxuLl9fcmFuZ2V7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5fX3JhbmdlLXN0ZXB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlOyAgICAgICAgICAgICAgICBcclxufVxyXG4uX19yYW5nZS1zdGVwe1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTsgICAgICAgICAgICAgICAgXHJcbn1cclxuXHJcbi5fX3JhbmdlLW1heHtcclxuXHRmbG9hdDogcmlnaHQ7XHJcbn1cclxuICAgICAgICAgICBcclxuXHJcblxyXG4uX19yYW5nZSBpbnB1dDo6cmFuZ2UtcHJvZ3Jlc3Mge1x0YmFja2dyb3VuZDogcmdiKDE4OSwgOCwgOCk7XHJcbn1cclxuLnNsaWRlciBpbnB1dFt0eXBlPXJhbmdlXTo6LW1vei1yYW5nZS1wcm9ncmVzcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY1N2EwO1xyXG4gIH1cclxuLl9fcmFuZ2Utc3RlcCBkYXRhbGlzdCB7XHJcblx0cG9zaXRpb246cmVsYXRpdmU7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0aGVpZ2h0OiBhdXRvO1xyXG5cdGJvdHRvbTogNnB4O1xyXG5cdC8qIGRpc2FibGUgdGV4dCBzZWxlY3Rpb24gKi9cclxuXHQtd2Via2l0LXVzZXItc2VsZWN0OiBub25lOyAvKiBTYWZhcmkgKi8gICAgICAgIFxyXG5cdC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEZpcmVmb3ggKi9cclxuXHQtbXMtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIElFMTArL0VkZ2UgKi8gICAgICAgICAgICAgICAgXHJcblx0dXNlci1zZWxlY3Q6IG5vbmU7IC8qIFN0YW5kYXJkICovXHJcblx0LyogZGlzYWJsZSBjbGljayBldmVudHMgKi9cclxuXHRwb2ludGVyLWV2ZW50czpub25lOyAgXHJcbn1cclxuLl9fcmFuZ2Utc3RlcCBkYXRhbGlzdCBvcHRpb24ge1xyXG5cdHdpZHRoOiAxMHB4O1xyXG5cdGhlaWdodDogMTBweDtcclxuXHRtaW4taGVpZ2h0OiAxMHB4O1xyXG5cdGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG5cdC8qIGhpZGUgdGV4dCAqL1xyXG5cdHdoaXRlLXNwYWNlOiBub3dyYXA7ICAgICAgIFxyXG4gIHBhZGRpbmc6MDtcclxuICBsaW5lLWhlaWdodDogNDBweDtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA4ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGU4e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU4IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTggLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTggLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDkqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlOXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDc1MHB4O1xyXG59XHJcbi5zbGlkZTkgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOSAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlOSAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5zbGlkZTkgLnRlc3QgbWF0LXJhZGlvLWdyb3Vwe1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMCoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxMHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDc1MHB4O1xyXG59XHJcbi5zbGlkZTEwIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTAgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2t7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgfVxyXG4gIFxyXG4gICAgIFxyXG4gICAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTEgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTExe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbn1cclxuLnNsaWRlMTEgLmltYWdle1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTEgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTExIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMTJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzAwcHg7XHJcbn1cclxuLnNsaWRlMTIgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMiAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbi5zbGlkZTEyIC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiBcclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTN7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiA3NTBweDtcclxufVxyXG4uc2xpZGUxMyAuaW1hZ2V7XHJcbm9yZGVyOiAyO1xyXG53aWR0aDogODAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLnRlc3R7XHJcbm9yZGVyOiAxO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6MTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEzIC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDMwcHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE0KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE0e1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDc1MHB4O1xyXG59XHJcbi5zbGlkZTE0IC5pbWFnZXtcclxub3JkZXI6IDI7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbmp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTQgLnRlc3R7XHJcbm9yZGVyOiAxO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6MTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE0IC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDMwcHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE1e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDo3NTBweDtcclxufVxyXG4uc2xpZGUxNSAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE1IC5pbWFnZSBwe1xyXG4gICAgb3JkZXI6IDI7XHJcblxyXG4gICAgICAgIH1cclxuLnNsaWRlMTUgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIH1cclxuXHJcblxyXG5cclxuLnNsaWRlMTUgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuIFxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxNntcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiA3NTBweDtcclxufVxyXG4uc2xpZGUxNiAuaW1hZ2V7XHJcbm9yZGVyOiAyO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE2IC50ZXN0e1xyXG5vcmRlcjogMTtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDoxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTYgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE3e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDo3NTBweDtcclxufVxyXG4uc2xpZGUxNyAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE3IC5pbWFnZSBoMXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICB9XHJcblxyXG4uc2xpZGUxNyAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuIFxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE4ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgICAgLnNsaWRlMTh7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE4IC5pbWFnZXtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTggLmltYWdlIGltZ3tcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBoZWlnaHQ6IDIwJTtcclxuICAgICAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAuc2xpZGUxOCAuaW1hZ2UgaDF7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgLnNsaWRlMTggLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydCA7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE5ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxOXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxOSAuaW1hZ2V7XHJcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTkgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE5IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyMHtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjAgLmltYWdle1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxub3JkZXI6IDE7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDgwJTtcclxufVxyXG4uc2xpZGUyMCAuaW1hZ2UgaW1ne1xyXG5oZWlnaHQ6IDYwJTtcclxud2lkdGg6IDQwJTtcclxuZmlsdGVyOiBkcm9wLXNoYWRvdygwLjRyZW0gMC40cmVtIDAuNDVyZW0gcmdiYSgwLCAwLCAzMCwgMC41KSk7XHJcbn1cclxuLnNsaWRlMjAgLnRlc3R7XHJcbm9yZGVyOiAyO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyMCAudGVzdCBoMXtcclxuZm9udC1zaXplOiAzMHB4O1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMjAgLnRlc3QgLmJsb2NrMXtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogOTAlO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0IC5ibG9jazIge1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcblxyXG4uc2xpZGUyMCAudGVzdCAuYmxvY2syIGxhYmVse1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjF7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIxIC5pbWFnZXtcclxuZGlzcGxheTpub25lO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxub3JkZXI6IDE7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwJTtcclxufVxyXG4uc2xpZGUyMSAudGVzdHtcclxub3JkZXI6IDE7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDpjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOjgwJTtcclxuaGVpZ2h0OiA4MCU7ICAgIFxyXG59XHJcblxyXG4uc2xpZGUyMSAudGVzdCBoMXtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgIGNvbG9yOiBncmVlbjtcclxuICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMSAudGVzdCBpbWd7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBmb250LXNpemU6IDgwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgICAgICAgICB9XHJcbi5zbGlkZTIxIC50ZXN0IHB7XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgY29sb3I6IHJnYigwLCAwLCAxMzMpO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuLnNsaWRlMjEgLnRlc3QgLnByaXgge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgICAgICB9XHJcbi5zbGlkZTIxIC50ZXN0IC5mYWlsZHtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICB3aWR0aDogMzYwcHg7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuLnNsaWRlMjEgLnRlc3QgaW1ne1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuLnNsaWRlMjEgLm5leHRJY29ue1xyXG4gICAgb3JkZXI6IDM7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICB3aWR0aDo5NyU7XHJcbiAgICBoZWlnaHQ6IDE1JTtcclxufVxyXG4uc2xpZGUyMSAubmV4dEljb24gaW1ne1xyXG4gICAgY29sb3I6IGdyZWVuO1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbn1cclxuICAgICAgICBcclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI0e1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNCAuaW1hZ2V7XHJcbm1hcmdpbi1sZWZ0OiA1MHB4O1xyXG5vcmRlcjogMjtcclxuZGlzcGxheTogZmxleDtcclxuYWxpZ24taXRlbXM6ZmxleC1lbmQ7XHJcbndpZHRoOiA4OCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNCAudGVzdHtcclxub3JkZXI6IDE7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxubWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4uc2xpZGUyNCAudGVzdCBoMXtcclxuZm9udC1zaXplOiAzMHB4O1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMjQgLnRlc3QgaW5wdXR7XHJcbndpZHRoOiA4MCU7XHJcbmhlaWdodDogNDBweDtcclxubWFyZ2luOiA1MHB4IDAgNTBweCAwO1xyXG5ib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgfVxyXG4uc2xpZGUyNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgd2lkdGg6IDEwMCU7XHJcbiAgIG1hcmdpbi10b3A6IDIwcHg7XHJcblxyXG4gICAgICAgIH1cclxuLnNsaWRlMjQgLnRlc3QgLmNhbGVuZHtcclxuICAgIHdpZHRoOiAzNTBweDtcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI1e1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNSAuaW1hZ2V7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxub3JkZXI6IDE7XHJcbndpZHRoOiAyMiU7XHJcbmhlaWdodDogMjAlO1xyXG59XHJcblxyXG4uc2xpZGUyNSAudGVzdHtcclxub3JkZXI6IDI7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0O1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDoxMDAlO1xyXG5oZWlnaHQ6IDgwJTtcclxufVxyXG5cclxuLnNsaWRlMjUgLnRlc3QgaW1ne1xyXG53aWR0aDogNTBweDtcclxubWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIH1cclxuLnNsaWRlMjUgLnRlc3QgcHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuY29sb3I6IHJnYigwLCAwLCAxMzMpO1xyXG5mb250LXNpemU6IDMwcHg7XHJcbmxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC5uZXh0SWNvbntcclxub3JkZXI6IDM7XHJcbn1cclxuLnNsaWRlMjUgLm5leHRJY29uIHB7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxubGluZS1oZWlnaHQ6IG5vcm1hbDtcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gLnNvY2lhbE1lZGlhIHtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IHJvdztcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSBpbWd7XHJcbndpZHRoOiA2MHB4O1xyXG59XHJcblxyXG4gICAgICAgICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjZ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDo1MyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjYgLmltYWdlIGltZ3tcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICB3aWR0aDogMTUlO1xyXG4gICAgICAgIH1cclxuLnNsaWRlMjYgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIH1cclxuXHJcbi5zbGlkZTI2IC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgaDF7XHJcbiAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazF7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIG1hdC1yYWRpby1ncm91cCAge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGxhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIH0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-test',
                templateUrl: './test.component.html',
                styleUrls: ['./test.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_2__["TestEgibiliteService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/baseUrl */ "./src/app/baseUrl.ts");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    apiUrl: src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__["baseUrl"],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\utilisateur\Videos\e5er front\last\frontCrm\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map